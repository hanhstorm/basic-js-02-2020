// function Person(name, age) {
//     this.name = name;
//     this.age = age;
// }

// function School(name) {
//     this.name = name;
// }

// function Student(studentName, studentAge, studentSchoolName) {
//     this.person = new Person(studentName, studentAge);
//     this.school = new School(studentSchoolName);

//     this.sayHello = function () {
//         console.log(this.person.name + " xin chào các bạn");
//     }

//     this.introduce = function () {
//         console.log("Mình tên là " + this.person.name + ", năm nay tôi " + this.person.age + " tuổi. Tôi đang học trường " + this.school.name);
//     }
// }

// function Person(first, last, age, gender, interests) {
//     this.name = {
//         first: first,
//         last: last
//     };
//     this.age = age;
//     this.gender = gender;
//     this.interests = interests;
//     this.bio = function () {
//         alert(this.name.first + ' ' + this.name.last + ' is ' + this.age + ' years old. He likes ' + this.interests[0] + ' and ' + this.interests[1] + '.');
//     };
//     this.greeting = function () {
//         alert('Hi! I\'m ' + this.name.first + '.');
//     };
// }

// var person1 = new Person('Bob', 'Smith', 32, 'male', ['music', 'skiing']);

// let person1 = new Object();

// person1.name = 'Chris';
// person1['age'] = 38;
// person1.greeting = function () {
//     alert('Hi! I\'m ' + this.name + '.');
// };

// let person1 = new Object({
//     name: 'Chris',
//     age: 38,
//     greeting: function () {
//         alert('Hi! I\'m ' + this.name + '.');
//     }
// });

// var car = {
//     type: "Fiat",
//     model: "500",
//     color: "white",
//     fullInformation: function() {
//         return this.type + " " + this.model + " " + this.color;
//     }
// }

// console.log(car.type);
// console.log(car.model);
// console.log(car.color);

// console.log(car.fullInformation());

