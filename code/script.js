// var a = 9;

// switch (a) {
//     case 1:
//     case 3:
//     case 5:
//     case 7:
//     case 9:
//         console.log("Số lẻ");
//         break;
//     case 2:
//     case 4:
//     case 6:
//     case 8:
//     case 10:
//         console.log("Số chẵn");
//         break;
//     default:
//         console.log("Số chưa học");
// }

// var dem = 0;

// for (var i = 1; i <= 10; i++) {
//     if (i % 2 == 0)
//         dem++;
// }

// console.log(dem);

// var n = 17;
// var i = 2;

// for (; i < n; i++) {
//     if (n % i == 0)
//         break;
// }

// if (i < n) {
//     console.log("Không phải số nguyên tố");
// } else {
//     console.log("Số nguyên tố");
// }

// function kiemTraSoChanLe(a) {
//     if (a % 2 == 0)
//         console.log("Đây là số chẵn");
//     else
//         console.log("Đây là số lẻ");
// }

// function kiemTraSoChanLeReturn(a) {
//     if (a % 2 == 0)
//         return true;
//     else
//         return false;
// }

// function sayHello(message = "Xin chào các bạn") {
//     console.log(message);
// }

// sayHello("Hello world !");
// sayHello();

// var globalVariable = "Global";
// console.log(globalVariable);

// function checkVariableScope() {
//     var localVariable = "Local";

//     console.log(globalVariable);

//     globalVariable = "Global inside function";
//     console.log(globalVariable);

//     console.log(localVariable);
// }

// checkVariableScope();

// console.log(globalVariable);
// console.log(localVariable);

// var arr = new Array(1,2,3,4,5,6,7,8,9);
// console.log(arr[0]);

// function demo(a, b = 10, c = 20) {
//     console.log(a);
//     console.log(b);
//     console.log(c);
//     return "OK";
// }

// function timUCLN(a, b) {
//     var min = b;
//     if (min > a) min = a;

//     for (var i = min; i >= 0; i--) {
//         if (a % i == 0 && b % i == 0)
//             return i;
//     }
// }

// function timSoLonNhatTrongMang(arr) {
//     var max = 0;
//     for (var i = 0; i < arr.length; i++) 
//         if (max < arr[i])
//             max = arr[i];

//     return max;
// }

// function timSoLonHon(a, b) {
//     if (a > b)
//         return a;
//     return b;
// }

// var max = timSoLonHon(7,30);

// var a = 20, b = 50;
// var max = a > b ? a : b;

// var arr = [
//     [1, 3, 5, 7, 9],
//     ["Xin", "chao", "tat", "ca", "cac", "ban"],
//     ["Mang", "nhieu", "chieu", "giong", "nhu", "mot", "ma", "tran"],
//     ["Day", "la", "vi", "du"]
// ];

// var matran = [
//     [1, 2, 3, 4, 5, 6, 7],
//     [2, 4, 6, 8, 10, 12, 14],
//     [4, 6, 8, 9, 12, 24, 30]
// ];

// for (var i = 0; i < arr.length; i++) {
//     for (var j = 0; j < arr[i].length; j++) {
//         console.log(arr[i][j]);
//     }
// }

// var chuoi = "Học lập trình vui nhưng là dễ khùng lắm";

// console.log(chuoi.indexOf("trình"));
// console.log(chuoi.lastIndexOf("nhưng"));

// var xinchao = "Hello world !";
// console.log(xinchao);

// xinchao = xinchao.replace("world", "Hanh");
// console.log(xinchao);

// var chuoi = "Học lập trình vui nhưng là dễ khùng lắm";

// console.log(chuoi.slice(3, 10));
// console.log(chuoi);

// console.log(chuoi.substr(4, 10));
// console.log(chuoi);

// console.log(chuoi.substring(4, 10));
// console.log(chuoi);

// var chuoi = "Học lập trình vui nhưng là dễ khùng lắm";

// console.log(chuoi.split("i"));

// var myDate = new Date("2020/12/20 12:00:00Z");

// var today = new Date();
// console.log(today.toTimeString());

// var obj = {
//     name: "Hanh",
//     age: 26,
//     company: "Janeto",
//     "family-address": "17/12",
//     students: [
//         { name: "Tuyền" },
//         { name: "Kim" },
//         { name: "Hằng" },
//         { name: "Trang" },
//         { name: "Nhung" }
//     ]
// }

// console.log(obj.name);
// console.log(obj["family-address"]);

// console.log(JSON.stringify(obj));

// function timSoTrongDayFibonaci(n) {
//     if (n == 1 || n == 2) {
//         return 1;
//     } else {
//         var so1 = 1;
//         var so2 = 1;
//         for (var i = 3; i <= n ; i++) {
//             var somoi = so1 + so2;
//             so1 = so2;
//             so2 = somoi;
//         }

//         return so2;
//     }
// }

// function xoaPhanTuChiaHetCho3(arr) {
//     var result = [];
//     for (var i = 0; i < arr.length; i++) {
//         if (arr[i] % 3 != 0) {
//             result.push(arr[i]);
//         }
//     }

//     return result;
// }

// function bubbleSort(arr) {
//     var data = arr;
//     for (var i = 0; i < data.length; i++) {
//         for(var j = i + 1; j < data.length; j++) {
//             if (data[i] < data[j]) {
//                 var tam = data[i];
//                 data[i] = data[j];
//                 data[j] = tam;
//             }
//         }
//     }

//     return data;
// }

// function kiemTraSoCoTrongMangHayKhong(arr, n) {
//     for (var i = 0; i < arr.length; i++)
//         if (arr[i] == n)
//             return true;

//     return false;
// }

// function xoaKyTuTrungTrongMang(arr) {
//     var result = [];

//     for (var i = 0; i < arr.length; i++) {
//         if (!kiemTraSoCoTrongMangHayKhong(result, arr[i])) {
//             result.push(arr[i]);
//         }
//     }

//     return result;
// }

// function chiLayKyTuDuyNhat(arr) {
//     var result = [];

//     for (var i = 0; i < arr.length; i++) {
//         var flag = false;

//         for (var j = 0; j < arr.length; j++) {
//             if (arr[i] == arr[j] && i != j) {
//                 flag = true;
//                 break;
//             }
//         }

//         if (flag == false) {
//             result.push(arr[i]);
//         }
//     } 

//     return result;
// }

//alert("Hello world !!!");

// var result = confirm("Do you like programming ?");

// console.log(result)

// var chuoiNhap = prompt("Vui lòng nhập vào 1 chuỗi");

// console.log(chuoiNhap.trim());

// var numberNhap = prompt("Vui lòng nhập vào 1 số");

// console.log(numberNhap.trim());
// console.log(parseInt(numberNhap));
// console.log(parseFloat(numberNhap));

// let elm = document.getElementById("mContent");
// elm.style.color = "#0174DF";

// let todo = {
//     id: 169,
//     name: "Play football",
//     status: false
// };

// elm.innerHTML = `<div id="todo-id-${todo.id}">
//                     <i class="${ todo.status == true ? "hidden" : ""} glyphicon glyphicon-ok"></i>
//                     <span class="name">${todo.name}</span>
//                 </div>`;

// function func01(name, age) {
// 	return `func01 My name is ${name}, ${age} years old`;
// }

// var func02 = function(name, age) {
// 	return `func02 My name is ${name}, ${age} years old`;
// }

// var func03 = (name, age) => {
// 	return `func03 My name is ${name}, ${age} years old`;
// }

// var func04 = (name, age) => 
// 	`func04 My name is ${name}, ${age} years old`;
// 	//return `func04 My name is ${name}, ${age} years old`;

// var func05 = name => {
// 	return `func05 My name is ${name}`;
// }

// var func06 = () => {
// 	return `func06 My name is`;
// }

// function course(name, price, free){
// 	return {
// 		name,
// 		price,
// 		free
// 	}
// }

// console.log(course("ES6", 20, false));

// function course(name, price, free){
// 	return {
// 		name,
// 		price,
// 		free,

// 		showInfo1: function(){
// 			console.log(`${ name + " - " + price + " - " + free}`);
// 		},

// 		showInfo2(){
// 			console.log(`${ name + " - " + price + " - " + free}`);
// 		},

// 		showInfo3(delimiter = "-"){
// 			console.log(`${ name + delimiter + price + delimiter + free}`);
// 		}
// 	}
// }

class Student {
	constructor(code, name, age){
		this.code = code;
		this.name = name;
		this.age  = age;
	}

	showInfo(){
		return this.code + " - " + this.name + " - " + this.getAge();
	}

	getAge(){
		let today = new Date();
		let year  = today.getFullYear();
		return year - this.age;
	}
}

let studentObj = new Student("SV001", "john", 1992);
console.log(studentObj);
console.log(studentObj.showInfo());