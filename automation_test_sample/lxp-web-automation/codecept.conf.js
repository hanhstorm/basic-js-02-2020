exports.config = {
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'https://org-admin.qa.leapxpert.app',
      windowSize: "1536x864",
      show: true
    }
  },
  include: {
    I: './steps_file.js',
    loginPage: './org_admin/pages/login.js',
    companyPage: './org_admin/pages/company.js',
    menuPage: './org_admin/pages/menu.js',
    searchPage: './org_admin/pages/search.js',
    // timePage: '../utils/time.js',
    managerPage: './org_admin/pages/manager.js',
    managerDetailPage: './org_admin/pages/managers_detail.js'
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './org_admin/features/*.feature',
    steps: [
      './org_admin/step_definitions/login.js',
      './org_admin/step_definitions/managers.js',
      './org_admin/step_definitions/managers_edit.js'
  ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    },
    retryFailedStep: {
      enabled: true
    }
  },
  tests: './*_test.js',
  name: 'lxp-web-automation'
}