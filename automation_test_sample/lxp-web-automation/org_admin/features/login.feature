Feature: Business rules
  In order to achieve my goals
  As a persona
  I want to be able to check all cases on login process
  Scenario: Org amdin check all cases for login 
    Then Org admin have login with invalid company
    Then Org admin have login with invalid username
    Then Org admin have login with invalid password
    Then Org admin have login with missing user
    Then Org admin have login with correct usename and password 