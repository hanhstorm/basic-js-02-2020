Feature: Business rules
  In order to achieve my goals
  As a persona
  I want to be able to check all cases on login process
  Scenario: Org admin check all cases for account managers
    Then Org admin have login with correct usename and password
    Then Org admin have create manager with invalid password
    Then Org admin have create manager with invalid email
    Then Org admin have create manager with invalid phone
    Then Org admin have create manager with valid value
    Then Org admin have create manager with Email Existed
    Then Org admin have create manager with username Existed