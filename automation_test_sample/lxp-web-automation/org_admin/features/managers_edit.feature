Feature: Business rules
  In order to achieve my goals
  As a persona
  I want to be able to check all cases on login process
  Scenario: Org admin check all cases for account managers
    Then Org admin have login with correct usename and password
    Given Org admin setup data for manager
    Then Org admin edit manager missing data
    Then Org admin edit manager with email invalid data
    Then Org admin edit manager with valid value
