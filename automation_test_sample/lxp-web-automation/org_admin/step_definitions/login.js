const { I, loginPage } = inject();

const {
  users
} = require('../data/users_data');

Then('Org admin have login with correct usename and password',() => {
    loginPage.sendUser(users[0]);
});

Then('Org admin have login with invalid company',() => {
  loginPage.invalidCompany(users[1]);
});

Then('Org admin have login with invalid username',() => {
  loginPage.invalidUser(users[1]);
});

Then('Org admin have login with invalid password',() => {
  loginPage.invalidPassword(users[1]);
});

Then('Org admin have login with missing user',() => {
  loginPage.missingUser(users[1]);
});