const { I, managerPage, searchPage, managerDetailPage } = inject();

let manager, managerUpdate, emailIncorrect, secondManager;
let validPassword = 'Testing@123';
let faker = require('faker');
const {
  users
} = require('../data/users_data');

const {
  buildManager,
  buildManagerUpdate,
  buildSecondManager
} = require('../data/managers_data');


Given('Org admin setup data for manager',() => {
  emailIncorrect = faker.internet.userName();
  manager = buildManager();
  managerPage.create(manager);
  managerUpdate = buildManagerUpdate();
});

Then('Org admin edit manager missing data',() => {
  searchPage.searchManager(manager);
  managerDetailPage.editMissing(manager);
});


Then('Org admin edit manager with email invalid data',() => {
  searchPage.searchManager(manager);
  managerDetailPage.editEmailIncorrect(manager, emailIncorrect);
});

Then('Org admin edit manager with valid value',() => {
  searchPage.searchManager(manager);
  managerDetailPage.editValidValue(manager, managerUpdate);
});