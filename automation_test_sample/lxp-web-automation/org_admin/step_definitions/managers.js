const { I, managerPage, searchPage, managerDetailPage } = inject();

let manager, managerUsernameExist,managerInvalidEmail, managerInvalidPhone, managerEmailExist;
let validPassword = 'Testing@123';
let validPhone = '2015550123';

const {
  users
} = require('../data/users_data');

const {
  buildManager,
  buildManagerInvalidEmail,
  buildManagerInvalidPhone,
  buildManagerPassword,
  buildManagerEmailExist,
  buildManagerUsernameExist
} = require('../data/managers_data');

Then('Org admin have create manager with invalid password',() => {
  managerPage.invalidPassword(buildManagerPassword(), validPassword);
});

Then('Org admin have create manager with invalid email',() => {
  
  managerInvalidEmail =  buildManagerInvalidEmail();
  managerPage.invalidEmail(managerInvalidEmail);
});

Then('Org admin have create manager with invalid phone',() => {
  I.refreshPage();
  managerInvalidPhone = buildManagerInvalidPhone();
  managerPage.invalidPhone(managerInvalidPhone, validPhone);
});


Then('Org admin have create manager with valid value',() => {
  I.refreshPage();
  manager = buildManager();
  managerPage.create(manager);
});

Then('Org admin have create manager with Email Existed',() => {
  I.refreshPage();
  managerEmailExist = buildManagerEmailExist();
  managerPage.existedValue(managerEmailExist);
});

Then('Org admin have create manager with username Existed',() => {
  I.refreshPage();
  managerUsernameExist = buildManagerUsernameExist();
  managerUsernameExist.username = manager.username;
  managerPage.existedValue(managerUsernameExist);
});
