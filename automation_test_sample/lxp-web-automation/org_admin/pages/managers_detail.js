// enable I and another page object
const { I } = inject();
const login = require('./login');
const timePage = require('../../utils/time');
module.exports =
  {
    header: {
      profile: '(//span[@class=\'MuiTab-wrapper\'])[1]',
      clients: '(//span[@class=\'MuiTab-wrapper\'])[2]',
      editButtton: '//div[@class=\'edit-profile-text-wrapper\']',
      cancelButton: '//span[@class=\'MuiButton-label\']',
      saveChangeButton: '//div[@class=\'edit-profile-button\']',
      saveChangeButtonActive: '//div[@class=\'edit-profile-button active\']'
    },

    changePasswordPopup: {
      newPassword: '(//input[@type=\'password\'])[1]',
      RetypeNewPassword: '(//input[@type=\'password\'])[2]',
      changePasswordDisable: '//div[@class=\'new-manager--submit-btn disabled\']',
      changePasswordActive: '//div[@class=\'new-manager--submit-btn enabled\']',
      closePopup: '//div[@class=\'icon-close\']'
    },

    form: {
      profileName: '//div[@class=\'profile-name\']',
      firstName: '//div[@id=\'profile_manager_firstname\']/div/div/input',
      lastName: '//div[@id=\'profile_manager_lastname\']/div/div/input',
      email: '//div[@id=\'profile_manager_email\']/div/div/input',
      phone: '//div[@id=\'profile_manager_phone\']/div/div/input',
      title: '//div[@id=\'profile_manager_title\']/div/div/input',
    },

    formMissing: {
      firtMissing: '(//span[@class=\'MuiIconButton-label\'])[1]',
      secondMissing: '(//span[@class=\'MuiIconButton-label\'])[2]',
      thirdMissing: '(//span[@class=\'MuiIconButton-label\'])[3]',
      fourthMissing: '(//span[@class=\'MuiIconButton-label\'])[4]',
      fifthMissing: '(//span[@class=\'MuiIconButton-label\'])[5]',
    },

    messageEdit: {
      success: 'Profile has been updated successfully'
    },

    /**
     * 
     * @param {*} user 
     */
    editMissing(user) {
      this.openForm();
      I.say('Try to check missing first name');
      I.say('Check missing first name');
      I.click(this.form.firstName);
      for (i = 0; i < user.firstName.length; i++) {
        I.pressKey('Backspace');
      }
      I.waitForVisible(this.header.saveChangeButtonActive, timePage.time.normal);
      I.click(this.header.saveChangeButtonActive);
      I.dontSeeElement(this.header.editButtton, timePage.time.normal);
      I.fillField(this.form.firstName, user.firstName);

      I.say('Check missing last name');
      I.waitForElement(this.form.lastName, timePage.time.normal);
      I.click(this.form.lastName);
      for (i = 0; i < user.lastName.length; i++) {
        I.pressKey('Backspace');
      }
      I.waitForVisible(this.header.saveChangeButtonActive, timePage.time.normal);
      I.click(this.header.saveChangeButtonActive);
      I.dontSeeElement(this.header.editButtton, timePage.time.normal);
      I.fillField(this.form.lastName, user.lastName);

      I.say('Check missing phone number');
      I.click(this.form.phone);
      for (i = 0; i < user.email.length; i++) {
        I.pressKey('Backspace');
      }
      I.waitForVisible(this.header.saveChangeButtonActive, timePage.time.normal);
      I.click(this.header.saveChangeButtonActive);
      I.dontSeeElement(this.header.editButtton, timePage.time.normal);
      I.fillField(this.form.phone, user.mobilePhoneNumber);
    },

    /**
     * 
     * @param {*} user 
     */
    editEmailIncorrect(email) {
      this.openForm();
      I.say('Edit email incorrect');
      I.clearField(this.form.email);
      I.fillField(this.form.email, email);
      I.waitForVisible(this.header.saveChangeButtonActive, timePage.time.normal);
      I.click(this.header.saveChangeButtonActive);
      I.dontSeeElement(this.header.editButtton, timePage.time.normal);
    },

    /**
     * 
     * @param {*} updateUser 
     */
    editValidValue(updateUser) {
      this.openForm();
      I.say('Edit first name');
      I.clearField(this.form.firstName);
      I.fillField(this.form.firstName, updateUser.firstName);

      I.say('Edit last name');
      I.clearField(this.form.lastName);
      I.fillField(this.form.lastName, updateUser.lastName);

      I.say('Edit email ');
      I.clearField(this.form.email);
      I.fillField(this.form.email, updateUser.email);

      I.say('Edit phone ');
      I.clearField(this.form.phone);
      I.fillField(this.form.phone, updateUser.mobilePhoneNumber);

      I.say('Edit job title ');
      I.clearField(this.form.title);
      I.fillField(this.form.title, updateUser.title);

      I.waitForVisible(this.header.saveChangeButtonActive, timePage.time.normal);
      I.click(this.header.saveChangeButtonActive);

      I.waitForText(this.messageEdit.success, timePage.time.normal);
      I.waitForText(updateUser.email, timePage.time.normal);
    },

    openForm() {
      I.waitForElement(this.header.editButtton, timePage.time.normal);
      I.waitForVisible(this.header.editButtton, timePage.time.normal);
      I.wait(1);
      I.click(this.header.editButtton);
      I.waitForVisible(this.header.cancelButton, timePage.time.normal);
      I.waitForElement(this.form.firstName, timePage.time.normal);
    }
  };