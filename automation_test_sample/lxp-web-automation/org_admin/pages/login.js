// enable I and another page object
const { I } = inject();
const timePage = require('../../utils/time');
module.exports =
  {
    url: {
      login: '/login'
    },

    company: {
      name: '//div/div/input'
    },

    homePage: {
      accountManager: '//div[@class=\'list--new-button\']'
    },

    nextButton: {
      xpath: '//div[2]/span[3]',
      inactive: '//span[@class=\'button inactive\']'
    },

    messageLogin: {
      invalidCompany: 'Invalid company. Please try again.',
      invalidUsernamePassword: 'Wrong username or password. Please try again.'
    },
    user: {
      name: '//div[1]/div/input',
      password: '//div[2]/div/input'
    },

    logoutActionp: {
      button: '//div[@class=\'sidebar--tab icon-logout\']/span',
      confirmLogout: '//div[2]/button/span[1]'
    },

    /**
     * 
     * @param {*} user 
     */
    sendCompany(user) {
      I.say('Input the the company');
      I.amOnPage(this.url.login);
      I.waitForElement(this.company.name, timePage.time.normal);
      I.fillField(this.company.name, user.company);
      I.pressKey('Enter');
    },

    /**
     * 
     * @param {*} user 
     */
    sendUser(user) {
      this.sendCompany(user);
      I.wait(1);
      I.say('Input the valid username and password');
      I.waitForVisible(this.user.name, timePage.time.normal);
      I.waitForVisible(this.user.password, timePage.time.normal);
      I.fillField(this.user.name, user.username);
      I.fillField(this.user.password, user.password);
      I.click(this.nextButton);
      I.waitForVisible(this.homePage.accountManager, timePage.time.normal);
    },

    /**
     * 
     * @param {*} user 
     */
    invalidCompany(user) {
      this.sendCompany(user);
      this.sendCompany(user);
      I.say('Check invalid the company');
      I.amOnPage(this.url.login);
      I.waitForElement(this.company.name, timePage.time.normal);
      I.fillField(this.company.name, user.company + user.company);
      I.pressKey('Enter');
      I.waitForText(this.messageLogin.invalidCompany, timePage.time.normal);
    },

    /**
     * 
     * @param {*} user 
     */
    invalidUser(user) {
      I.wait(1);
      this.sendCompany(user);
      I.say('Check invalid the user');
      I.waitForVisible(this.user.name, timePage.time.normal);
      I.fillField(this.user.name, user.username + user.password);
      I.fillField(this.user.password, user.password);
      I.click(this.nextButton);
      I.dontSeeElement(this.homePage.accountManager, timePage.time.normal);
    },

    /**
     * 
     * @param {*} user 
     */
    invalidPassword(user) {
      I.wait(1);
      this.sendCompany(user);
      I.fillField(this.company.name, user.company);
      I.pressKey('Enter');
      I.say('Check invalid the password');
      I.waitForElement(this.nextButton.inactive, timePage.time.normal);
      I.waitForVisible(this.user.name, timePage.time.normal);
      I.fillField(this.user.name, user.username);
      I.fillField(this.user.password, user.password + user.password);
      I.click(this.nextButton);
      I.dontSeeElement(this.homePage.accountManager, timePage.time.normal);
    },

    /**
     * 
     * @param {*} user 
     */
    missingUser(user) {
      I.wait(1);
      this.sendCompany(user);
      I.say('Check missing the username');
      I.waitForVisible(this.user.name, 30);
      I.fillField(this.user.password, user.password);
      I.waitForElement(this.nextButton.inactive, timePage.time.normal);

      I.fillField(this.user.name, user.username);
      I.dontSeeElement(this.nextButton.inactive, timePage.time.normal);
    },

    /**
     * 
     * @param {*} user 
     */
    missingPassword(user) {
      I.wait(1);
      this.sendCompany(user);
      I.fillField(this.company.name, user.company);
      I.pressKey('Enter');
      I.say('Check missing the password');
      I.waitForVisible(this.user.name, 20);
      I.fillField(this.user.name, user.username);
      I.waitForElement(this.nextButton.inactive, timePage.time.normal);
      I.fillField(this.user.password, user.password);
      I.dontSeeElement(this.nextButton.inactive, timePage.time.normal);
    },

    logout() {
      I.waitForVisible(this.logoutActionp.button, timePage.time.normal);
      I.click(this.logoutActionp.button);
      I.waitForElement(this.logoutActionp.confirmLogout, timePage.time.normal);

      I.click(this.logoutActionp.confirmLogout);
      I.dontSeeElement(this.homePage.accountManager, timePage.time.normal);
    }
  };