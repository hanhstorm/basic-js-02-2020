// enable I and another page object
const { I } = inject();
const menuPage = require('./menu');
const timePage = require('../../utils/time');
module.exports =
  {
    url: {
      search: '/search'
    },

    searchFile: {
      field: '//div[@class=\'search-input-wrapper\']/input',
      iconSearch: '//div[@class=\'icon-search-wrapper\']',
      clear: '//div[@class=\'clear-button-wrapper\']',
    },

    resultTab: {
      generalTab: '//div[@class=\'search-results-tabs\']',
      clientTab: '//div[@class=\'search-results-tabs\']/div[1]',
      managerTab: '//div[@class=\'search-results-tabs\']/div[2]'
    },

    filterTab: {
      resetButton: '//div[@class=\'reset-filter\']',
      activeCheckbox: '//div[@class=\'property-values\']/div[1]/span',
      suspendedCheckbox: '//div[@class=\'property-values\']/div[2]/span'
    },

    resulsValue: {
      firstValue: '//div[@class=\'user-card\']'
    },

    message: {
      noResults: '0 results'
    },

    /**
     * 
     * @param {*} company 
     */
    searchManager(user) {
      I.refreshPage();
      I.say('Search user after create success');
      I.waitForElement(menuPage.button.search, timePage.time.normal);
      I.amOnPage(this.url.search);
      I.waitForVisible(this.searchFile.field, timePage.time.normal);
      I.click(this.searchFile.field);
      I.wait(1);
      I.fillField(this.searchFile.field, user.email);
      I.wait(1);
      I.pressKey('Enter');
      I.waitForElement(this.resultTab.generalTab, timePage.time.normal);
      I.waitForElement(this.resultTab.managerTab, timePage.time.normal);
      I.waitForText(user.email, timePage.time.normal);
      I.waitForVisible(this.resulsValue.firstValue, timePage.time.normal);
      I.click(this.resulsValue.firstValue);
      I.waitForText(user.email, timePage.time.normal);
    }
  };