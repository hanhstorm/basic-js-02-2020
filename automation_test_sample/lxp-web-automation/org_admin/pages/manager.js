// enable I and another page object
const { I } = inject();
const login = require('./login');
const timePage = require('../../utils/time');
module.exports =
  {
    url: {
      manager: '/managers'
    },
    managerPopup: {
      username: '//div[@class=\'MuiFormControl-root MuiTextField-root username\']/div/input',
      firstname: '//div[@class=\'MuiFormControl-root MuiTextField-root first-name\']/div/input',
      lastname: '//div[@class=\'MuiFormControl-root MuiTextField-root last-name\']/div/input',
      email: '//div[@class=\'MuiFormControl-root MuiTextField-root email\']/div/input',
      phone: '//input[@type=\'tel\']',
      password: '//input[@type=\'password\']'
    },

    buttonPopup: {
      generatePassword: '//span[contains(@class, \'MuiSwitch-track\')]',
      nextButton: '//div[@class=\'actions-wrapper\']',
      nextButtonEnable: '//button[@class=\'new-manager--submit-btn enabled\']'
    },

    letterPopup: {
      contentField: '//textarea[@placeholder=\'Customize your content\']',
      backButton: '(//span[@class=\'MuiTouchRipple-root\'])[1]',
      inviteButton: '//button[@class=\'MuiButtonBase-root MuiButton-root button-invite MuiButton-contained MuiButton-colorInherit\']',
      skeepEmail: '//div[@class=\'skip-sending-email\']'
    },

    messageLogin: {
      success: 'Successfully Created Account Manager',
      invalidCompany: 'Invalid company. Please try again.',
      invalidUsernamePassword: 'Wrong username or password. Please try again.'
    },
    user: {
      name: '//div[1]/div/input',
      password: '//div[2]/div/input'
    },

    /**
     * 
     * @param {*} company 
     */
    create(user) {
      I.say('Create manager by valid value');
      I.refreshPage();
      I.wait(1);
      I.waitForElement(login.homePage.accountManager, timePage.time.normal);
      I.click(login.homePage.accountManager);
      I.waitForElement(this.managerPopup.firstname, timePage.time.normal);
      I.fillField(this.managerPopup.firstname, user.firstName);

      I.waitForElement(this.managerPopup.lastname, timePage.time.normal);
      I.fillField(this.managerPopup.lastname, user.lastName);

      I.waitForElement(this.managerPopup.email, timePage.time.normal);
      I.fillField(this.managerPopup.email, user.email);

      I.waitForElement(this.managerPopup.phone, timePage.time.normal);
      I.wait(timePage.time.waitForInputOnFiled);
      I.click(this.managerPopup.phone);
      I.wait(timePage.time.waitForInputOnFiled);
      I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);

      I.wait(0.5);
      I.waitForElement(this.managerPopup.username, timePage.time.normal);
      I.fillField(this.managerPopup.username, user.username);

      I.waitForElement(this.managerPopup.password, timePage.time.normal);
      I.fillField(this.managerPopup.password, user.password);
      I.waitForElement(this.buttonPopup.nextButtonEnable, timePage.time.normal);
      I.wait(1);
      I.click(this.buttonPopup.nextButtonEnable);
      I.waitForElement(this.letterPopup.contentField, timePage.time.normal);
      I.waitForElement(this.letterPopup.inviteButton, timePage.time.normal);
      I.click(this.letterPopup.inviteButton);
      I.wait(timePage.time.waitOpenForm);
      I.waitForVisible(login.homePage.accountManager, timePage.time.wait60);
    },

    /**
     * 
     * @param {*} user 
     */
    invalidPhone(user, validPhone) {
      I.refreshPage();
      I.waitForElement(login.homePage.accountManager, timePage.time.normal);
      I.wait(1);
      I.click(login.homePage.accountManager);
      I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
      I.fillField(this.managerPopup.firstname, user.firstName);

      I.waitForElement(this.managerPopup.lastname, timePage.time.normal);
      I.fillField(this.managerPopup.lastname, user.lastName);

      I.waitForElement(this.managerPopup.email, timePage.time.normal);
      I.fillField(this.managerPopup.email, user.email);

      I.waitForElement(this.managerPopup.phone, timePage.time.normal);
      I.click(this.managerPopup.phone);
      I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);

      I.waitForElement(this.managerPopup.username, timePage.time.normal);
      I.fillField(this.managerPopup.username, user.username);

      I.waitForElement(this.managerPopup.password, timePage.time.normal);
      I.fillField(this.managerPopup.password, user.password);
      I.dontSeeElement(this.buttonPopup.nextButtonEnable, timePage.time.normal);

      I.click(this.managerPopup.phone);

      for (i = 0; i < user.mobilePhoneNumber.length; i++) {
        I.pressKey('Backspace');
      }

      I.fillField(this.managerPopup.phone, validPhone);
      I.waitForVisible(this.buttonPopup.nextButtonEnable, timePage.time.normal);
      I.refreshPage();
    },

    /**
     * 
     * @param {*} user 
     */
    invalidEmail(user) {
      I.refreshPage();
      I.waitForElement(login.homePage.accountManager, timePage.time.normal);
      I.wait(1);
      I.click(login.homePage.accountManager);
      I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);

      I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
      I.fillField(this.managerPopup.firstname, user.firstName);

      I.waitForVisible(this.managerPopup.lastname, timePage.time.normal);
      I.fillField(this.managerPopup.lastname, user.lastName);

      I.waitForVisible(this.managerPopup.email, timePage.time.normal);
      I.fillField(this.managerPopup.email, user.email);

      I.waitForVisible(this.managerPopup.phone, timePage.time.normal);
      I.click(this.managerPopup.phone);
      I.wait(timePage.time.waitForInputOnFiled);
      I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);

      I.waitForVisible(this.managerPopup.username, timePage.time.normal);
      I.fillField(this.managerPopup.username, user.username);

      I.waitForVisible(this.managerPopup.password, timePage.time.normal);
      I.fillField(this.managerPopup.password, user.password);
      I.dontSeeElement(this.buttonPopup.nextButtonEnable, timePage.time.normal);
      I.fillField(this.managerPopup.email, '@gmail.com');
      I.waitForVisible(this.buttonPopup.nextButtonEnable, timePage.time.normal);
      I.refreshPage();
    },

    /**
     * 
     * @param {*} user 
     */
    existedValue(user) {
      I.refreshPage();
      I.waitForElement(login.homePage.accountManager, timePage.time.normal);
      I.wait(1);
      I.click(login.homePage.accountManager);
      I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
      I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
      I.fillField(this.managerPopup.firstname, user.firstName);

      I.waitForVisible(this.managerPopup.lastname, timePage.time.normal);
      I.fillField(this.managerPopup.lastname, user.lastName);

      I.waitForVisible(this.managerPopup.email, timePage.time.normal);
      I.fillField(this.managerPopup.email, user.email);

      I.waitForVisible(this.managerPopup.phone, timePage.time.normal);
      I.click(this.managerPopup.phone);
      I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
      I.waitForVisible(this.managerPopup.username, timePage.time.normal);
      I.fillField(this.managerPopup.username, user.username);

      I.waitForVisible(this.managerPopup.password, timePage.time.normal);
      I.fillField(this.managerPopup.password, user.password);
      I.dontSeeElement(this.buttonPopup.nextButtonEnable, timePage.time.normal);
    },
    /**
  * 
  * @param {*} user 
  */
    invalidPhone(user) {
      I.refreshPage();
      I.waitForElement(login.homePage.accountManager, timePage.time.normal);
      I.wait(1);
      I.click(login.homePage.accountManager);
      I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
      I.fillField(this.managerPopup.firstname, user.firstName);

      I.waitForElement(this.managerPopup.lastname, timePage.time.normal);
      I.fillField(this.managerPopup.lastname, user.lastName);

      I.waitForElement(this.managerPopup.email, timePage.time.normal);
      I.fillField(this.managerPopup.email, user.email);

      I.waitForElement(this.managerPopup.phone, timePage.time.normal);
      I.click(this.managerPopup.phone);
      I.wait(timePage.time.waitForInputOnFiled);
      I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);

      I.waitForElement(this.managerPopup.username, timePage.time.normal);
      I.fillField(this.managerPopup.username, user.username);

      I.waitForElement(this.managerPopup.password, timePage.time.normal);
      I.fillField(this.managerPopup.password, user.password);
      I.dontSeeElement(this.buttonPopup.nextButtonEnable, timePage.time.normal);

      I.fillField(this.managerPopup.phone, '5551123');
      I.waitForVisible(this.buttonPopup.nextButtonEnable, 30);
      I.refreshPage();
    },

    /**
     * 
     * @param {*} user 
     */
    invalidPassword(user, password) {
      I.refreshPage();
      I.waitForElement(login.homePage.accountManager, timePage.time.normal);
      I.wait(1);
      I.click(login.homePage.accountManager);
      I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);

      I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
      I.fillField(this.managerPopup.firstname, user.firstName);

      I.waitForVisible(this.managerPopup.lastname, timePage.time.normal);
      I.fillField(this.managerPopup.lastname, user.lastName);

      I.waitForVisible(this.managerPopup.email, timePage.time.normal);
      I.fillField(this.managerPopup.email, user.email);

      I.waitForVisible(this.managerPopup.phone, timePage.time.normal);
      I.click(this.managerPopup.phone);
      I.wait(timePage.time.waitForInputOnFiled);
      I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);

      I.waitForVisible(this.managerPopup.username, timePage.time.normal);
      I.fillField(this.managerPopup.username, user.username);

      I.waitForVisible(this.managerPopup.password, timePage.time.normal);
      I.fillField(this.managerPopup.password, user.password);
      I.dontSeeElement(this.buttonPopup.nextButtonEnable, timePage.time.normal);
      I.clearField(this.managerPopup.password);
      I.fillField(this.managerPopup.password, password);
      I.waitForVisible(this.buttonPopup.nextButtonEnable, timePage.time.normal);
    },
  };