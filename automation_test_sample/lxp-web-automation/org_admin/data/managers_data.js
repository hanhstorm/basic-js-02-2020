let faker = require('faker');
const buildManager = () => {
    let manager = {
        username: 'automation' + faker.internet.userName().toLowerCase().trim(),
        firstName: 'automation' + faker.name.findName().trim().toLowerCase(),
        lastName: 'automation' + faker.name.findName().trim().toLowerCase(),
        email: faker.internet.email().toLowerCase(),
        title: faker.name.jobTitle(),
        mobilePhoneNumber: '2015550123',
        password: 'Testing@123'
    }
    return manager;
}

const buildManagerUpdate = () => {
    let manager = {
        username: 'automation' + faker.internet.userName().toLowerCase().trim(),
        firstName: 'automation' + faker.name.findName().trim().toLowerCase(),
        lastName: 'automation' + faker.name.findName().trim().toLowerCase(),
        email: faker.internet.email().toLowerCase(),
        title: faker.name.jobTitle(),
        mobilePhoneNumber: '2015550123',
        password: 'Testing@123'
    }
    return manager;
}

const buildSecondManager = () => {
    let manager = {
        username: faker.internet.userName().toLowerCase().trim(),
        firstName: faker.name.findName().trim().toLowerCase(),
        lastName: faker.name.findName().trim().toLowerCase(),
        email: faker.internet.email().toLowerCase(),
        title: faker.name.jobTitle(),
        mobilePhoneNumber: '2015550123',
        password: 'Testing@123'
    }
    return manager;
}

const buildManagerInvalidEmail = () => {
    let manager = {
        username: faker.internet.userName().toLowerCase(),
        firstName: faker.name.findName().toLowerCase(),
        lastName: faker.name.findName().toLowerCase(),
        email: faker.internet.userName().toLowerCase(),
        mobilePhoneNumber: '2015550123',
        password: 'Testing@123'
    }
    return manager;
}

const buildManagerInvalidPhone = () => {
    let manager = {
        username: faker.internet.userName().toLowerCase(),
        firstName: faker.name.findName(),
        lastName: faker.name.findName(),
        email: faker.internet.email().toLowerCase(),
        mobilePhoneNumber: '201',
        password: 'Testing@123'
    }
    return manager;
}

const buildManagerPassword = () => {
    let manager = {
        username: faker.internet.userName().toLowerCase(),
        firstName: faker.name.findName(),
        lastName: faker.name.findName(),
        email: faker.internet.email().toLowerCase(),
        mobilePhoneNumber: '2015550123',
        password: '12'
    }
    return manager;
}

const buildManagerEmailExist = () => {
    let manager = {
        username: faker.internet.userName().toLowerCase(),
        firstName: faker.name.findName(),
        lastName: faker.name.findName(),
        email: buildManager().email,
        mobilePhoneNumber: '201',
        password: 'Testing@123'
    }
    return manager;
}

const buildManagerUsernameExist = () => {
    let manager = {
        username: buildManager().username,
        firstName: faker.name.findName(),
        lastName: faker.name.findName(),
        email: faker.internet.email(),
        mobilePhoneNumber: '2015550123',
        password: 'Testing@123'
    }
    return manager;
}

module.exports = {
    buildManager,
    buildManagerInvalidEmail,
    buildManagerInvalidPhone,
    buildManagerPassword,
    buildManagerEmailExist,
    buildManagerUsernameExist,
    buildManagerUpdate,
    buildSecondManager
}