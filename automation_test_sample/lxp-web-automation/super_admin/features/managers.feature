Feature: Business rules
  In order to achieve my goals
  As a persona
  I want to be able to check all cases on managers
  Scenario: Check all cases for account managers
    Then Super admin have login with correct usename and password for super admin
    Given Super admin have create manger valid data
    Then Super admin have check create manager
    Then Super admin have check create manager with missing username
    Then Super admin have create quick client with missing first name
    Then Super admin have create quick client with missing last name
    Then Super admin have create quick client with missing email
    Given Super admin have change data to test email already exists
    Then Super admin have check create manager with email already exists
    Given Super admin have change data to test username already exists
    Then Super admin have check create manager with username already exists
    Given Super admin have change email for yopmail to checking
    Then Super admin have check create manager then get the new email on jopmail