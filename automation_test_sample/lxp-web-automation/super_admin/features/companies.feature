Feature: Business rules
  In order to achieve my goals
  As a persona
  I want to be able to check all cases on companies

  Scenario: Check all cases for companies
    Then Super admin have login with correct usename and password for super admin
    Then Super admin create new company with missing location
    Then Super admin create new company with missing name
    Then Super admin create new company then clicks on cancel button