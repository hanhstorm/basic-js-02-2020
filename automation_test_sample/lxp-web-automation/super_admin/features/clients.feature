Feature: Business rules
  In order to achieve my goals
  As a persona
  I want to be able to check all cases on clients
  Scenario: Check all cases for account clients
    Then Super admin have login with correct usename and password for super admin
    Given Super admin have create manager for client
    Then Super admin have create client with valid value
    Then Super admin have create client with missing username
    Then Super admin have create client with missing first name
    Then Super admin have create client with missing last name
    Then Super admin have create client with missing phone
    Then Super admin have create client with missing account manager
    Then Super admin have create client with missing password
