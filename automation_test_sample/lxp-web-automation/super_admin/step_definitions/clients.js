const { I, clientPage, managerPage} = inject();
let usernameValid, company, manager, client;
let faker = require('faker');

const {
    buildCompany
} = require('../data/companies_data');

const {
    buildManager
} = require('../data/managers_data');

const {
    buildClient,
    buildClientYopmail
} = require('../data/clients_data');

Given('Super admin have create manager for client',() => {
  company = buildCompany();
  manager = buildManager(company.name);
  I.refreshPage();
  managerPage.create(manager);
  client = buildClient(company.name, manager.username);
});

Then('Super admin have create client with valid value',() => {
  clientPage.create(client);
});

Given('Super admin update username for checking email already existed',() => {
  usernameValid = client.username;
  client.username = client.username + 'client';
});

Given('Super admin update email for checking username already existed',() => {
  client.username = usernameValid;
  client.email = faker.internet.email();
});

Then('Super admin have create client with email existed',() => {
  I.refreshPage();
  clientPage.nameAlreadyExists(client);
});

Then('Super admin have create client with missing username',() => {
  I.refreshPage();
  clientPage.misingUser(client);
});


Then('Super admin have create client with missing first name',() => {
  I.refreshPage();
  clientPage.missingFirstName(client);
});

Then('Super admin have create client with missing last name',() => {
  I.refreshPage();
  clientPage.missingLastName(client);
});

Then('Super admin have create client with missing phone',() => {
  I.refreshPage();
  clientPage.missingPhone(client);
});

Then('Super admin have create client with missing account manager',() => {
  I.refreshPage();
  clientPage.missingCompanyManager(client);
});

Then('Super admin have create client with missing password',() => {
  I.refreshPage();
  clientPage.missingPassword(client);
});