const { I, managerPage, yobmailPage } = inject();
let usernameValid, company, manager,managerYopmail;

let faker = require('faker');
const {
    buildCompany
} = require('../data/companies_data');
 
const {
    buildManager,
    buildManagerYopmail
} = require('../data/managers_data');

Given('Super admin have create manger valid data',() => {
  company = buildCompany();
  manager = buildManager(company.name);
});


Then('Super admin have check create manager',() => {
  I.refreshPage();
  managerPage.create(manager);
});

Then('Super admin have check create manager with missing username',() => {
  I.refreshPage();
  managerPage.misingUser(manager);
});

Then('Super admin have create quick client with missing first name',() => {
  managerPage.missingFirstName(manager);
});

Then('Super admin have create quick client with missing last name',() => {
  managerPage.missingLastName(manager);
});

Then('Super admin have create quick client with missing email',() => {
  managerPage.missingEmail(manager);
});

Given('Super admin have change data to test email already exists',() => {
  usernameValid = manager.username;
  manager.username = faker.random.word();
});
 
Then('Super admin have check create manager with email already exists',() => {
  managerPage.missingEmail(manager);
});

Given('Super admin have change data to test username already exists',() => {
  manager.username = usernameValid;
  manager.email = faker.internet.email();
});
 
Then('Super admin have check create manager with username already exists',() => {
  managerPage.nameAlreadyExists(manager);
});


Given('Super admin have change email for yopmail to checking',() => {
  managerYopmail = buildManagerYopmail(company.name);
});
 
Then('Super admin have check create manager then get the new email on jopmail', async () => {
  managerPage.create(managerYopmail);
});