const { I, loginPage } = inject();

const {
  users
} = require('../data/login_data');

Then('Super admin have login with correct usename and password for super admin',() => {
    loginPage.login(users[0]);
});

Then('Super admin have login with incorrect usename and password for super admin',() => {
  loginPage.invalid(users[1]);
});