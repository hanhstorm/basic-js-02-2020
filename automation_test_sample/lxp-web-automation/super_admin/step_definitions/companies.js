const { I, companyPage } = inject();

const {
  companies
} = require('../data/companies_data');

Then('Super admin create new company with valide value',() => {
  companyPage.create(companies[0]);
});

Then('Super admin create new company with missing location', async () => {
  I.refreshPage();
  companyPage.missingLocation(companies[1]);
});

Then('Super admin create new company with missing name', async () => {
  I.refreshPage();
  companyPage.missingName(companies[1]);
});

Then('Super admin create new company then clicks on cancel button', async () => {
  I.refreshPage();
  companyPage.cancel(companies[1]);
});


