// enable I and another page object
const { I } = inject();
const timePage = require('../../utils/time');
module.exports =
    {
        clientTab: { xpath: '//a[contains(@class, \'NavSide__sub-header\')][3]' },

        createButton: { xpath: '//div[contains(@class, \'ClientList__button\')]' },

        clientPopup: {
            username: '//input[@placeholder=\'User name\']',
            firstname: '//input[@placeholder=\'First Name\']',
            lastname: '//input[@placeholder=\'Last Name\']',
            email: '//input[@placeholder=\'Email\']',
            phone: '//input[@placeholder=\'Phone Number\']',
            company: '//div[contains(@class, \'AddClientModal__input\')][6]/div',
            manager: '//div[contains(@class, \'AddClientModal__input\')][7]/div',
            password: '//input[@placeholder=\'Temporary Password\']',
        },

        popupButton: {
            save: '//div[contains(@class, \'AddClientModal__buttons\')]/div[2]',
            cancel: '//div[contains(@class, \'AddClientModal__buttons\')]/div[1]',
            saveInactive: '//div[contains(@class, \'AddClientModal__inactive\')]',
        },


        popupSuccess: {
            message: 'INVITE SENT!',
            doneButton: '//div[contains(@class, \'AddClientModal__success\')]'
        },

        errorMessage: {
            email: 'email already exists',
            name: 'name already exists'
        },

        /**
         * 
         * @param {*} user 
         */
        create(client) {
            I.say('');
            I.refreshPage();
            I.waitForElement(this.clientTab.xpath, timePage.time.normal);
            I.click(this.clientTab.xpath);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.clientPopup.username, timePage.time.normal);
            I.fillField(this.clientPopup.username, client.username);
            I.waitForVisible(this.clientPopup.firstname, timePage.time.normal);
            I.fillField(this.clientPopup.firstname, client.firstName);
            I.fillField(this.clientPopup.lastname, client.lastName);
            I.fillField(this.clientPopup.email, client.email);
            I.fillField(this.clientPopup.phone, client.mobilePhoneNumber);
            I.waitForVisible(this.clientPopup.company, timePage.time.normal);
            I.click(this.clientPopup.company);
            I.fillField(this.clientPopup.company, client.company);
            I.wait(1);
            I.pressKey('Enter');

            I.click(this.clientPopup.manager);
            I.fillField(this.clientPopup.manager, client.manager);
            I.wait(1);
            I.pressKey('Enter');

            I.fillField(this.clientPopup.password, client.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
            I.click(this.popupButton.save);
            I.waitForText(this.popupSuccess.message, timePage.time.normal);
            I.waitForVisible(this.popupSuccess.doneButton, timePage.time.normal);
            I.click(this.popupSuccess.doneButton);
        },

        /**
         * 
         * @param {*} name 
         * @param {*} location 
         */
        cancel(client) {
            I.say('Check clicks on cancel after fill valid value');
            I.refreshPage();
            I.waitForElement(this.clientTab.xpath, timePage.time.normal);
            I.click(this.clientTab.xpath);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.clientPopup.username, timePage.time.normal);
            I.fillField(this.clientPopup.username, client.username);
            I.waitForVisible(this.clientPopup.firstname, timePage.time.normal);
            I.fillField(this.clientPopup.firstname, client.firstName);
            I.fillField(this.clientPopup.lastname, client.lastName);
            I.fillField(this.clientPopup.email, client.email);
            I.fillField(this.clientPopup.phone, client.mobilePhoneNumber);
            I.waitForVisible(this.clientPopup.company, timePage.time.normal);
            I.click(this.clientPopup.company);
            I.fillField(this.clientPopup.company, client.company);
            I.wait(1);
            I.pressKey('Enter');

            I.click(this.clientPopup.manager);
            I.fillField(this.clientPopup.manager, client.manager);
            I.wait(1);
            I.pressKey('Enter');

            I.fillField(this.clientPopup.password, client.password);
            I.waitForVisible(this.popupButton.cancel, timePage.time.normal);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
            I.click(this.popupButton.cancel);
            I.dontSeeElement(this.popupSuccess.doneButton, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        misingUser(client) {
            I.say('Create new client manager missing user');
            I.refreshPage();
            I.waitForElement(this.clientTab.xpath, timePage.time.normal);
            I.click(this.clientTab.xpath);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.clientPopup.username, timePage.time.normal);

            I.waitForVisible(this.clientPopup.firstname, timePage.time.normal);
            I.fillField(this.clientPopup.firstname, client.firstName);
            I.fillField(this.clientPopup.lastname, client.lastName);
            I.fillField(this.clientPopup.email, client.email);
            I.fillField(this.clientPopup.phone, client.mobilePhoneNumber);
            I.waitForVisible(this.clientPopup.company, timePage.time.normal);
            I.click(this.clientPopup.company);
            I.fillField(this.clientPopup.company, client.company);
            I.wait(1);
            I.pressKey('Enter');

            I.click(this.clientPopup.manager);
            I.fillField(this.clientPopup.manager, client.manager);
            I.wait(1);
            I.pressKey('Enter');

            I.fillField(this.clientPopup.password, client.password);
            I.waitForVisible(this.popupButton.cancel, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);
            I.fillField(this.clientPopup.username, client.username);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        missingFirstName(client) {
            I.say('Create new client manager missing firstname');
            I.refreshPage();
            I.waitForElement(this.clientTab.xpath, timePage.time.normal);
            I.click(this.clientTab.xpath);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.clientPopup.username, timePage.time.normal);
            I.fillField(this.clientPopup.username, client.username);
            I.waitForVisible(this.clientPopup.firstname, timePage.time.normal);

            I.fillField(this.clientPopup.lastname, client.lastName);
            I.fillField(this.clientPopup.email, client.email);
            I.fillField(this.clientPopup.phone, client.mobilePhoneNumber);
            I.waitForVisible(this.clientPopup.company, timePage.time.normal);
            I.click(this.clientPopup.company);
            I.fillField(this.clientPopup.company, client.company);
            I.wait(1);
            I.pressKey('Enter');

            I.click(this.clientPopup.manager);
            I.fillField(this.clientPopup.manager, client.manager);
            I.wait(1);
            I.pressKey('Enter');

            I.fillField(this.clientPopup.password, client.password);
            I.waitForVisible(this.popupButton.cancel, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);
            I.fillField(this.clientPopup.firstname, client.firstName);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        missingLastName(client) {
            I.say('Create new client manager missing lastname');
            I.refreshPage();
            I.waitForElement(this.clientTab.xpath, timePage.time.normal);
            I.click(this.clientTab.xpath);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.clientPopup.username, timePage.time.normal);
            I.fillField(this.clientPopup.username, client.username);
            I.waitForVisible(this.clientPopup.firstname, timePage.time.normal);

            I.fillField(this.clientPopup.lastname, client.lastName);
            I.fillField(this.clientPopup.email, client.email);
            I.fillField(this.clientPopup.phone, client.mobilePhoneNumber);
            I.waitForVisible(this.clientPopup.company, timePage.time.normal);
            I.click(this.clientPopup.company);
            I.fillField(this.clientPopup.company, client.company);
            I.wait(1);
            I.pressKey('Enter');

            I.click(this.clientPopup.manager);
            I.fillField(this.clientPopup.manager, client.manager);
            I.wait(1);
            I.pressKey('Enter');

            I.fillField(this.clientPopup.password, client.password);
            I.waitForVisible(this.popupButton.cancel, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);
            I.fillField(this.clientPopup.firstname, client.firstName);

            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        missingEmail(client) {
            I.say('Create new client manager missing email');
            I.refreshPage();
            I.waitForElement(this.clientTab.xpath, timePage.time.normal);
            I.click(this.clientTab.xpath);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.clientPopup.username, timePage.time.normal);
            I.fillField(this.clientPopup.username, client.username);
            I.waitForVisible(this.clientPopup.firstname, timePage.time.normal);

            I.fillField(this.clientPopup.lastname, client.lastName);
            I.fillField(this.clientPopup.firstname, client.firstName);
            I.fillField(this.clientPopup.phone, client.mobilePhoneNumber);
            I.waitForVisible(this.clientPopup.company, timePage.time.normal);
            I.click(this.clientPopup.company);
            I.fillField(this.clientPopup.company, client.company);
            I.wait(1);
            I.pressKey('Enter');
            I.waitForText(client.company, timePage.time.normal);

            I.click(this.clientPopup.manager);
            I.fillField(this.clientPopup.manager, client.manager);
            I.wait(1);
            I.pressKey('Enter');

            I.fillField(this.clientPopup.password, client.password);
            I.waitForVisible(this.popupButton.cancel, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);

            I.fillField(this.clientPopup.email, client.email);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        missingPhone(client) {
            I.say('Create new client manager missing phone');
            I.refreshPage();
            I.waitForElement(this.clientTab.xpath, timePage.time.normal);
            I.click(this.clientTab.xpath);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.clientPopup.username, timePage.time.normal);
            I.fillField(this.clientPopup.username, client.username);
            I.waitForVisible(this.clientPopup.firstname, timePage.time.normal);
            I.fillField(this.clientPopup.email, client.email);
            I.fillField(this.clientPopup.lastname, client.lastName);
            I.fillField(this.clientPopup.firstname, client.firstName);

            I.waitForVisible(this.clientPopup.company, timePage.time.normal);
            I.click(this.clientPopup.company);
            I.fillField(this.clientPopup.company, client.company);
            I.wait(1);
            I.pressKey('Enter');

            I.click(this.clientPopup.manager);
            I.fillField(this.clientPopup.manager, client.manager);
            I.wait(1);
            I.pressKey('Enter');

            I.fillField(this.clientPopup.password, client.password);
            I.waitForVisible(this.popupButton.cancel, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);
            I.fillField(this.clientPopup.phone, client.mobilePhoneNumber);

            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        missingCompanyManager(client) {
            I.say('Create new client manager missing company and manager');
            I.refreshPage();
            I.waitForElement(this.clientTab.xpath, timePage.time.normal);
            I.click(this.clientTab.xpath);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.clientPopup.username, timePage.time.normal);
            I.fillField(this.clientPopup.username, client.username);
            I.waitForVisible(this.clientPopup.firstname, timePage.time.normal);
            I.fillField(this.clientPopup.email, client.email);
            I.fillField(this.clientPopup.lastname, client.lastName);
            I.fillField(this.clientPopup.firstname, client.firstName);


            I.fillField(this.clientPopup.phone, client.mobilePhoneNumber);


            I.fillField(this.clientPopup.password, client.password);
            I.waitForVisible(this.popupButton.cancel, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);

            I.waitForVisible(this.clientPopup.company, timePage.time.normal);
            I.click(this.clientPopup.company);
            I.fillField(this.clientPopup.company, client.company);
            I.wait(1);
            I.pressKey('Enter');

            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);
            I.click(this.clientPopup.manager);
            I.fillField(this.clientPopup.manager, client.manager);
            I.wait(1);
            I.pressKey('Enter');
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);

        },
        /**
         * 
         * @param {*} user 
         */
        missingPassword(client) {
            I.say('Create new client manager missing password');
            I.refreshPage();
            I.waitForElement(this.clientTab.xpath, timePage.time.normal);
            I.click(this.clientTab.xpath);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.clientPopup.username, timePage.time.normal);
            I.fillField(this.clientPopup.username, client.username);
            I.waitForVisible(this.clientPopup.firstname, timePage.time.normal);
            I.fillField(this.clientPopup.email, client.email);
            I.fillField(this.clientPopup.lastname, client.lastName);
            I.fillField(this.clientPopup.firstname, client.firstName);
            I.fillField(this.clientPopup.phone, client.mobilePhoneNumber);

            I.waitForVisible(this.clientPopup.company, timePage.time.normal);
            I.click(this.clientPopup.company);
            I.fillField(this.clientPopup.company, client.company);
            I.wait(1);
            I.pressKey('Enter');

            I.click(this.clientPopup.manager);
            I.fillField(this.clientPopup.manager, client.manager);
            I.wait(1);
            I.pressKey('Enter');


            I.waitForVisible(this.popupButton.cancel, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);

            I.fillField(this.clientPopup.password, client.password);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        emailAlreadyExists(client) {
            I.say('');
            I.refreshPage();
            I.waitForElement(this.clientTab.xpath, timePage.time.normal);
            I.click(this.clientTab.xpath);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.clientPopup.username, timePage.time.normal);
            I.fillField(this.clientPopup.username, client.username);
            I.waitForVisible(this.clientPopup.firstname, timePage.time.normal);
            I.fillField(this.clientPopup.firstname, client.firstName);
            I.fillField(this.clientPopup.lastname, client.lastName);
            I.fillField(this.clientPopup.email, client.email);
            I.fillField(this.clientPopup.phone, client.mobilePhoneNumber);
            I.waitForVisible(this.clientPopup.company, timePage.time.normal);
            I.click(this.clientPopup.company);
            I.fillField(this.clientPopup.company, client.company);
            I.wait(1);
            I.pressKey('Enter');

            I.click(this.clientPopup.manager);
            I.fillField(this.clientPopup.manager, client.manager);
            I.wait(1);
            I.pressKey('Enter');

            I.fillField(this.clientPopup.password, client.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
            I.click(this.popupButton.save);
            I.waitForText(this.errorMessage.email, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        nameAlreadyExists(client) {
            I.say('');
            I.refreshPage();
            I.waitForElement(this.clientTab.xpath, timePage.time.normal);
            I.click(this.clientTab.xpath);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.clientPopup.username, timePage.time.normal);
            I.fillField(this.clientPopup.username, client.username);
            I.waitForVisible(this.clientPopup.firstname, timePage.time.normal);
            I.fillField(this.clientPopup.firstname, client.firstName);
            I.fillField(this.clientPopup.lastname, client.lastName);
            I.fillField(this.clientPopup.email, client.email);
            I.fillField(this.clientPopup.phone, client.mobilePhoneNumber);
            I.waitForVisible(this.clientPopup.company, timePage.time.normal);
            I.click(this.clientPopup.company);
            I.fillField(this.clientPopup.company, client.company);
            I.wait(1);
            I.pressKey('Enter');

            I.click(this.clientPopup.manager);
            I.fillField(this.clientPopup.manager, client.manager);
            I.wait(1);
            I.pressKey('Enter');

            I.fillField(this.clientPopup.password, client.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
            I.click(this.popupButton.save);
            I.waitForText(this.errorMessage.name, timePage.time.normal);
        }
    };