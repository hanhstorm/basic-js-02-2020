// enable I and another page object
const { I } = inject();
const login = require('./login');
const timePage = require('../../utils/time');
module.exports =
    {
        popupField: {
            name: '//div[1]/div[1]/input',
            location: '//div[1]/div[2]/input',
        },

        popupButton: {
            save: '//div[contains(@class, \'AddCompanyModal__button\')]/div[2]',
            cancel: '//div[contains(@class, \'AddCompanyModal__button\')]/div[1]',
            saveInactive: '//div[contains(@class, \'AddCompanyModal__inactive\')]',
        },

        popupSuccess: {
            message: 'SUCCESS!',
            doneButton: '//div[contains(@class, \'AddCompanyModal__button\')]'

        },

        table: {
            name: '(//a[contains(@class,\'AdminCompanyList__link\')])[1]'
        },

        /**
         * 
         * @param {*} name 
         * @param {*} location 
         */
        create(company) {
            I.refreshPage();
            I.waitForElement(login.homePage.addCompanies, timePage.time.normal);
            I.waitForVisible(login.homePage.addCompanies, timePage.time.normal);
            I.click(login.homePage.addCompanies)

            I.say('Add new comany with location');
            I.waitForElement(this.popupField.name, timePage.time.normal);
            I.waitForElement(this.popupField.name, timePage.time.normal);
            I.fillField(this.popupField.name, company.name);
            I.fillField(this.popupField.location, company.location);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.wait(1);
            I.click(this.popupButton.save);
            I.waitForText(this.popupSuccess.message, timePage.time.normal);
            I.waitForVisible(this.popupSuccess.doneButton, timePage.time.normal);
            I.click(this.popupSuccess.doneButton);
        },

        /**
         * 
         * @param {*} name 
         * @param {*} location 
         */
        cancel(company) {
            I.refreshPage();
            I.say('Clicks  on cancel after input valid value');
            I.waitForElement(login.homePage.addCompanies, timePage.time.normal);
            I.waitForVisible(login.homePage.addCompanies, timePage.time.normal);
            I.click(login.homePage.addCompanies)
            I.waitForElement(this.popupField.name, timePage.time.normal);
            I.waitForElement(this.popupField.name, timePage.time.normal);
            I.fillField(this.popupField.name, company.name);
            I.fillField(this.popupField.location, company.location);
            I.waitForVisible(this.popupButton.cancel, timePage.time.normal);
            I.click(this.popupButton.cancel);
            I.dontSeeElement(this.popupSuccess.doneButton, timePage.time.normal);
            I.dontSeeElement(this.popupSuccess.doneButton);
        },

        /**
         * 
         * @param {*} name 
         * @param {*} location 
         */
        missingLocation(company) {
            I.refreshPage();
            I.say('Clicks on save when missing Location');
            I.waitForElement(login.homePage.addCompanies, timePage.time.normal);
            I.waitForVisible(login.homePage.addCompanies, timePage.time.normal);
            I.click(login.homePage.addCompanies)

            I.waitForElement(this.popupField.name, timePage.time.normal);
            I.fillField(this.popupField.name, company.name);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.fillField(this.popupField.location, company.location);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);

        },

        /**
         * 
         * @param {*} name 
         * @param {*} location 
         */
        missingName(company) {
            I.refreshPage();
            I.say('Clicks on save when missing Name');
            I.waitForElement(login.homePage.addCompanies, timePage.time.normal);
            I.waitForVisible(login.homePage.addCompanies, timePage.time.normal);
            I.click(login.homePage.addCompanies)
            I.waitForElement(this.popupField.name, timePage.time.normal);
            I.fillField(this.popupField.location, company.location);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);
            I.fillField(this.popupField.name, company.name);
            I.fillField(this.popupField.location, company.location);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

    };