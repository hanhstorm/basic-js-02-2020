// enable I and another page object
const { I } = inject();
const timePage = require('../../utils/time');
module.exports =
    {
        user: {
            name: '//input[@type=\'text\']',
            password: '//input[@type=\'password\']'
        },

        loginButton: { xpath: '//span[contains(@class, \'Login__button\')]' },

        homePage: {
            content: '//div[contains(@class, \'AdminCompanyList__header\')]',
            addCompanies: '//div[contains(@class, \'AdminCompanyList__button\')]',
            companyName: '//span[contains(@class, \'AddCompanyModal__input\')]'
        },

        message: {
            error: "//div[@class='message']"
        },

        /**
         * Check valid username and password
         * @param {*} user array 
         */
        login(user) {
            I.refreshPage();
            I.amOnPage('/login');
            I.waitForElement(this.user.name, timePage.time.normal);
            I.fillField(this.user.name, user.name);
            I.fillField(this.user.password, user.password);
            I.pressKey('Enter');
            I.waitForVisible(this.homePage.content, timePage.time.normal);
            I.waitForVisible(this.homePage.addCompanies, timePage.time.normal);
        },

        /**
         * Check invalid username or password
         * 
         * @param {*} user array 
         */
        invalid(user) {
            I.refreshPage();
            I.say('Login with invalid username and password');
            I.amOnPage('/login');
            I.waitForElement(this.user.name, timePage.time.normal);
            I.fillField(this.user.name, user.name);
            I.fillField(this.user.password, user.password);
            I.pressKey('Enter');
            I.waitForElement(this.message.error, timePage.time.normal);

            I.refreshPage();
            I.say('Login with mising password');
            I.amOnPage('/login');
            I.waitForElement(this.user.name, timePage.time.normal);
            I.fillField(this.user.name, user.name);
            I.pressKey('Enter');
            I.waitForElement(this.message.error, timePage.time.normal);

            I.refreshPage();
            I.amOnPage('/login');
            I.say('Login with mising username');
            I.waitForElement(this.user.name, timePage.time.normal);
            I.fillField(this.user.name, user.password);
            I.pressKey('Enter');
            I.waitForElement(this.message.error, timePage.time.normal);

            I.refreshPage();
            I.say('Login with mising username and username');
            I.amOnPage('/login');
            I.waitForElement(this.user.name, timePage.time.normal);
            I.pressKey('Enter');
            I.waitForElement(this.message.error, timePage.time.normal);
        }
    };