// enable I and another page object
const { I } = inject();

module.exports =
{
  menu: {
    chat: '//a[@id=\'chat\']',
    contact: '//a[@id=\'contact\']'
  },

  header: {
    addButton: '//div[contains(@class, \'NavSideHeader__icon-wrapper\')]/span[1]'
  }
};