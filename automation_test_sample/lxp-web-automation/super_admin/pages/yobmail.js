// enable I and another page object
const { I } = inject();
const timePage = require('../../utils/time');
module.exports =
    {
        url: { yobmail: 'http://www.yopmail.com/en/' },

        input: {
            email: '//td[@class=\'nw\']/input[2]',
            checkButton: '//input[@type=\'submit\']'
        },

        email: {
            header: 'Invitation from LeapXpert',
            emailValue: 'Username: '
        },

        /**
         * 
         * @param {*} user 
         */
        checkEmail(email) {
            I.say('Check email confirm ');
            I.amOnPage(this.url.yobmail);
            I.waitForElement(this.input.email, timePage.time.normal);
            I.fillField(this.input.email, email);
            I.waitForElement(this.input.checkButton, timePage.time.normal);
            I.click(this.input.checkButton);
            I.wait(timePage.time.waitOpenForm);
            I.waitForText(this.email.emailValue, timePage.time.normal);
        }
    };