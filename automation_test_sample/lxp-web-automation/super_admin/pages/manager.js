// enable I and another page object
const { I } = inject();
const timePage = require('../../utils/time');
module.exports =
    {
        accountManagerTab: { xpath: '(//a[contains(@class, \'NavSide__sub-header\')])[2]' },

        createButton: { xpath: '//div[contains(@class, \'AdminAMList__button\')]' },

        managerPopup: {
            username: '//input[@placeholder=\'User name\']',
            firstname: '//input[@placeholder=\'First Name\']',
            lastname: '//input[@placeholder=\'Last Name\']',
            email: '//input[@placeholder=\'Email\']',
            phone: '//input[@placeholder=\'Phone Number\']',
            company: '//*[@id="react-select-2--value"]/div[1]',
            password: '//input[@placeholder=\'Temporary Password\']',
        },

        popupButton: {
            save: '//div[contains(@class, \'AddAMModal__button\')]/div[2]',
            cancel: '//div[contains(@class, \'AddAMModal__button\')]/div[1]',
            saveInactive: '//div[contains(@class, \'AddAMModal__inactive\')]',
        },

        popupSuccess: {
            message: 'INVITE SENT!',
            doneButton: '//div[contains(@class, \'AddAMModal__button\')]'
        },

        errorMessage: {
            email: 'email already exists',
            name: 'name already exists'
        },

        /**
         * 
         * @param {*} user 
         */
        create(user) {
            I.refreshPage();
            I.waitForElement(this.accountManagerTab, timePage.time.normal);
            I.click(this.accountManagerTab);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.managerPopup.username, timePage.time.normal);
            I.fillField(this.managerPopup.username, user.username);
            I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
            I.fillField(this.managerPopup.firstname, user.firstName);
            I.fillField(this.managerPopup.lastname, user.lastName);
            I.fillField(this.managerPopup.email, user.email);
            I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
            I.waitForVisible(this.managerPopup.company, timePage.time.normal);
            I.click(this.managerPopup.company);
            I.fillField(this.managerPopup.company, user.company);
            I.wait(1);
            I.pressKey('Enter');
            I.fillField(this.managerPopup.password, user.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
            I.wait(1);
            I.click(this.popupButton.save);
            I.waitForText(this.popupSuccess.message, timePage.time.normal);
            I.wait(1);
            I.waitForVisible(this.popupSuccess.doneButton, timePage.time.normal);
            I.click(this.popupSuccess.doneButton);
        },

        //43665managerde_ch@yobmail.com
        /**
         * 
         * @param {*} name 
         * @param {*} location 
         */
        cancel(user) {
            I.refreshPage();
            I.waitForElement(this.accountManagerTab, timePage.time.normal);
            I.click(this.accountManagerTab);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.managerPopup.username, timePage.time.normal);
            I.fillField(this.managerPopup.username, user.username);
            I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
            I.fillField(this.managerPopup.firstname, user.firstName);
            I.fillField(this.managerPopup.lastname, user.lastName);
            I.fillField(this.managerPopup.email, user.email);
            I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
            I.click(this.managerPopup.company);
            I.fillField(this.managerPopup.company, user.company);
            I.wait(1);
            I.pressKey('Enter');
            I.fillField(this.managerPopup.password, user.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
            I.waitForVisible(this.popupButton.cancel, timePage.time.normal);
            I.click(this.popupButton.cancel);
            I.dontSeeElement(this.popupSuccess.doneButton, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        misingUser(user) {
            I.refreshPage();
            I.waitForElement(this.accountManagerTab, timePage.time.normal);
            I.click(this.accountManagerTab);

            I.say('Create new account manager missing username');
            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.managerPopup.username, timePage.time.normal);
            I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
            I.fillField(this.managerPopup.firstname, user.firstName);
            I.fillField(this.managerPopup.lastname, user.lastName);
            I.fillField(this.managerPopup.email, user.email);
            I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
            I.click(this.managerPopup.company);
            I.fillField(this.managerPopup.company, user.company);
            I.wait(1);
            I.pressKey('Enter');
            I.fillField(this.managerPopup.password, user.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);

            I.fillField(this.managerPopup.username, user.username);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        missingFirstName(user) {
            I.say('Create new account manager missing firstname');
            I.refreshPage();
            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.managerPopup.username, timePage.time.normal);
            I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
            I.fillField(this.managerPopup.username, user.username);

            I.fillField(this.managerPopup.lastname, user.lastName);
            I.fillField(this.managerPopup.email, user.email);
            I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
            I.click(this.managerPopup.company);
            I.fillField(this.managerPopup.company, user.company);
            I.wait(1);
            I.pressKey('Enter');
            I.fillField(this.managerPopup.password, user.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);

            I.fillField(this.managerPopup.firstname, user.firstName);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        missingLastName(user) {
            I.say('Create new account manager missing last name');
            I.refreshPage();
            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.managerPopup.username, timePage.time.normal);
            I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
            I.fillField(this.managerPopup.username, user.username);

            I.fillField(this.managerPopup.lastname, user.lastName);
            I.fillField(this.managerPopup.email, user.email);
            I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
            I.click(this.managerPopup.company);
            I.fillField(this.managerPopup.company, user.company);
            I.wait(1);
            I.pressKey('Enter');
            I.fillField(this.managerPopup.password, user.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);

            I.fillField(this.managerPopup.firstname, user.firstName);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        missingEmail(user) {
            I.say('Create new account manager missing Email');
            I.refreshPage();
            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.managerPopup.username, timePage.time.normal);
            I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
            I.fillField(this.managerPopup.username, user.username);
            I.fillField(this.managerPopup.firstname, user.firstName);
            I.fillField(this.managerPopup.lastname, user.lastName);

            I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
            I.click(this.managerPopup.company);
            I.fillField(this.managerPopup.company, user.company);
            I.wait(1);
            I.pressKey('Enter');
            I.fillField(this.managerPopup.password, user.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);
            I.fillField(this.managerPopup.email, user.email);

            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        missingPhone(user) {
            I.say('Create new account manager missing phone');
            I.refreshPage();
            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.managerPopup.username, timePage.time.normal);
            I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
            I.fillField(this.managerPopup.username, user.username);
            I.fillField(this.managerPopup.firstname, user.firstName);
            I.fillField(this.managerPopup.lastname, user.lastName);
            I.fillField(this.managerPopup.email, user.email);

            I.click(this.managerPopup.company);
            I.fillField(this.managerPopup.company, user.company);
            I.wait(1);
            I.pressKey('Enter');
            I.fillField(this.managerPopup.password, user.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);

            I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        missingCompany(user) {
            I.say('Create new account manager missing company');
            I.refreshPage();
            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.managerPopup.username, timePage.time.normal);
            I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
            I.fillField(this.managerPopup.username, user.username);
            I.fillField(this.managerPopup.firstname, user.firstName);
            I.fillField(this.managerPopup.lastname, user.lastName);
            I.fillField(this.managerPopup.email, user.email);
            I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
            I.fillField(this.managerPopup.password, user.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);

            I.click(this.managerPopup.company);
            I.fillField(this.managerPopup.company, user.company);
            I.wait(1);
            I.pressKey('Enter');
            I.dontSeeElement(this.popupButton.saveInactive, 30);

        },
        /**
         * 
         * @param {*} user 
         */
        missingPassword(user) {
            I.say('Create new account manager missing password');
            I.refreshPage();
            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.managerPopup.username, timePage.time.normal);
            I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
            I.fillField(this.managerPopup.username, user.username);
            I.fillField(this.managerPopup.firstname, user.firstName);
            I.fillField(this.managerPopup.lastname, user.lastName);
            I.fillField(this.managerPopup.email, user.email);
            I.click(this.managerPopup.company);
            I.fillField(this.managerPopup.company, user.company);
            I.wait(1);
            I.pressKey('Enter');
            I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.waitForVisible(this.popupButton.saveInactive, timePage.time.normal);

            I.fillField(this.managerPopup.password, user.password);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        emailAlreadyExists(user) {
            I.say('Create new account manager Email already exists');
            I.refreshPage();
            I.waitForElement(this.accountManagerTab, timePage.time.normal);
            I.click(this.accountManagerTab);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.managerPopup.username, timePage.time.normal);
            I.fillField(this.managerPopup.username, user.username);
            I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
            I.fillField(this.managerPopup.firstname, user.firstName);
            I.fillField(this.managerPopup.lastname, user.lastName);
            I.fillField(this.managerPopup.email, user.email);
            I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
            I.click(this.managerPopup.company);
            I.fillField(this.managerPopup.company, user.company);
            I.wait(1);
            I.pressKey('Enter');
            I.fillField(this.managerPopup.password, user.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
            I.click(this.popupButton.save);
            I.waitForText(this.errorMessage.email, timePage.time.normal);
        },

        /**
         * 
         * @param {*} user 
         */
        nameAlreadyExists(user) {
            I.say('Create new account manager username already exists');
            I.refreshPage();
            I.waitForElement(this.accountManagerTab, timePage.time.normal);
            I.click(this.accountManagerTab);

            I.waitForVisible(this.createButton, timePage.time.normal);
            I.click(this.createButton);
            I.waitForVisible(this.managerPopup.username, timePage.time.normal);
            I.fillField(this.managerPopup.username, user.username);
            I.waitForVisible(this.managerPopup.firstname, timePage.time.normal);
            I.fillField(this.managerPopup.firstname, user.firstName);
            I.fillField(this.managerPopup.lastname, user.lastName);
            I.fillField(this.managerPopup.email, user.email);
            I.fillField(this.managerPopup.phone, user.mobilePhoneNumber);
            I.click(this.managerPopup.company);
            I.fillField(this.managerPopup.company, user.company);
            I.wait(1);
            I.pressKey('Enter');
            I.fillField(this.managerPopup.password, user.password);
            I.waitForVisible(this.popupButton.save, timePage.time.normal);
            I.dontSeeElement(this.popupButton.saveInactive, timePage.time.normal);
            I.click(this.popupButton.save);
            I.waitForText(this.errorMessage.name, timePage.time.normal);
        }
    };