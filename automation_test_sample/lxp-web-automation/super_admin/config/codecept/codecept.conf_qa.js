exports.config = {
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'https://site-admin.qa.leapxpert.app',
      show: false
    }
  },
  include: {
    I: './steps_file.js',
    loginPage: './super_admin/pages/login.js',
    companyPage: './super_admin/pages/company.js',
    menuPage: './super_admin/pages/menu.js',
    clientPage: './super_admin/pages/client.js',
    timePage: './tests/pages/time.js',
    managerPage: './super_admin/pages/manager.js',
    yobmailPage: './super_admin/pages/yobmail.js',
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './super_admin/features/*.feature',
    steps: [
      './super_admin/step_definitions/login.js',
      './super_admin/step_definitions/managers.js',
      './super_admin/step_definitions/companies.js',
      './super_admin/step_definitions/clients.js'
  ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    },
    retryFailedStep: {
      enabled: true
    }
  },
  tests: './*_test.js',
  name: 'lxp-web-automation'
}