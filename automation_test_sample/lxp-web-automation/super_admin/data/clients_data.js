let faker = require('faker');
const buildClient = (companyName, managerName) => {
    let client = {
        username: 'automation' + faker.random.number() + 'client' + faker.random.locale().toLowerCase(),
        firstName: 'automation' + faker.name.findName(),
        lastName: 'automation' + faker.name.findName(),
        email: faker.internet.email(),
        mobilePhoneNumber: faker.phone.phoneNumber(),
        company: companyName,
        manager: managerName,
        password: 'Testing@123'
    }
    return client;
}

module.exports = {
    buildClient
}