let faker = require('faker');
const buildManager = (companyValue) => {
    let manager = {
        username: 'automation' + faker.internet.userName().toLowerCase(),
        firstName:'automation' +  faker.name.findName(),
        lastName: 'automation' + faker.name.findName(),
        email: faker.internet.email().toLowerCase(),
        mobilePhoneNumber: faker.phone.phoneNumber(),
        company: companyValue,
        password: 'Testing@123'
    }
    return manager;
}

const buildManagerYopmail = (companyValue) => {
    let manager = {
        username: 'automation' + faker.internet.userName().toLowerCase(),
        firstName: 'automation' + faker.name.findName(),
        lastName: 'automation' + faker.name.findName(),
        email: faker.random.number() + 'manager' + faker.random.locale().toLowerCase() + '@yopmail.com'.toLowerCase(),
        mobilePhoneNumber: faker.phone.phoneNumber(),
        company: companyValue,
        password: 'Testing@123'
    }
    return manager;
}

module.exports = {
    buildManager,
    buildManagerYopmail
}