// enable I and another page object
const { I } = inject();

module.exports =
  {
    time: {
      normal: 30,
      wait60: 60,
      waitOpenForm: 3,
      waitForInputOnFiled: 1
    }
  };