exports.config = {
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'https://web.qa.leapxpert.app',
      windowSize: "1024x768",
      show: false
    }
  },
  include: {
    I: './steps_file.js',
    loginPage: './am/pages/login.js',
    menuPage: './am/pages/menu.js',
    clientPage: './am/pages/client.js',
    clientQuickAddPage: './am/pages/client_quick_add.js',
    clientDetailPage: './am/pages/client_detail.js',
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './am/features/*.feature',
    steps: [
      './am/step_definitions/login.js',
      './am/step_definitions/client.js',
      './am/step_definitions/client_quick_add.js',
      './am/step_definitions/client_edit.js'
  ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    },
    retryFailedStep: {
      enabled: true
    }
  },
  tests: './*_test.js',
  name: 'lxp-web-automation'
}