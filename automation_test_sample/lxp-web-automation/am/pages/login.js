// enable I and another page object
const { I } = inject();
const timePage = require('../../utils/time');
module.exports =
  {
    url: { login: '/login' },

    company: {
      name: '//span[3]/input'
    },

    nextButton: { xpath: '//div[2]/span[4]' },

    user: {
      name: '//span[3]/input',
      password: '//span[4]/input'
    },

    loginButton: { xpath: '//span[5]' },

    code: {
      code1: '//div[1]/input',
      code2: '//div[2]/input',
      code3: '//div[3]/input',
      code4: '//div[4]/input',
      code5: '//div[5]/input',
      code6: '//div[6]/input'
    },

    message: {
      companyNotExist: 'Company does not exist',
      incorrectOTP: 'Incorrect OTP entered. Please try again.'
    },

    confirmButton: { xpath: '//span[3]' },

    sendCompany(user) {
      I.refreshPage();
      I.say('input vaid value company');
      I.amOnPage(this.url.login);
      I.waitForElement(this.company.name, timePage.time.normal);
      I.fillField(this.company.name, user.company);
      I.pressKey('Enter');
      I.amOnPage(this.url.login + '/' + user.company);
    },

    /**
     * LXN-107
     * @param {*} user 
     */
    sendUser(user) {
      I.say(' Input valid username and password');
      I.waitForVisible(this.user.name, timePage.time.normal);
      I.fillField(this.user.name, user.username);
      I.fillField(this.user.password, user.password);
      I.click(this.loginButton);
      I.waitForVisible(this.code.code1, timePage.time.normal);

      I.fillField(this.code.code1, 1);
      I.fillField(this.code.code2, 1);
      I.fillField(this.code.code3, 1);
      I.fillField(this.code.code4, 1);
      I.fillField(this.code.code5, 1);
      I.fillField(this.code.code6, 1);
      I.waitForElement(this.confirmButton, timePage.time.normal);
      I.click(this.confirmButton);
    },

    /**
     * LXN-103
     * @param {*} user 
     */
    invalidCompany(user) {
      I.refreshPage();
      I.say('Check  invalid company name');
      I.amOnPage(this.url.login);
      I.waitForElement(this.company.name, timePage.time.normal);
      I.fillField(this.company.name, user.company + user.company);
      I.pressKey('Enter');
      I.waitForText(this.message.companyNotExist, timePage.time.normal);
    },

    /**
     * LXN-289
     * @param {*} user 
     */
    invalidUsername(user) {
      I.say('Check invalid username');
      I.waitForVisible(this.user.name, timePage.time.normal);
      I.fillField(this.user.name, user.username + user.username);
      I.fillField(this.user.password, user.password);
      I.click(this.loginButton);
      I.dontSeeElement(this.code.code1, timePage.time.normal);

      I.refreshPage();
      I.waitForVisible(this.user.name, timePage.time.normal);
      I.fillField(this.user.name, user.username);
      I.fillField(this.user.password, user.password);
      I.click(this.loginButton);
      I.waitForElement(this.code.code1, timePage.time.normal);
    },

    invalidPassword(user) {
      I.say('Check invalid password');
      I.waitForVisible(this.user.name, timePage.time.normal);
      I.fillField(this.user.name, user.username);
      I.fillField(this.user.password, user.password + user.password);
      I.click(this.loginButton);
      I.dontSeeElement(this.code.code1, timePage.time.normal);

      I.refreshPage();
      I.waitForVisible(this.user.name, timePage.time.normal);
      I.fillField(this.user.name, user.username);
      I.fillField(this.user.password, user.password);
      I.click(this.loginButton);
      I.waitForElement(this.code.code1, timePage.time.normal);
    },

    invalidCode(user) {
      I.say('Check invalid code');
      I.waitForVisible(this.user.name, timePage.time.normal);
      I.fillField(this.user.name, user.username);
      I.fillField(this.user.password, user.password);
      I.pressKey('Enter');
      I.waitForVisible(this.code.code1, timePage.time.normal);

      I.fillField(this.code.code1, 1);
      I.fillField(this.code.code2, 1);
      I.fillField(this.code.code3, 1);
      I.fillField(this.code.code4, 1);
      I.fillField(this.code.code5, 1);
      I.fillField(this.code.code6, 2);
      I.waitForElement(this.confirmButton, timePage.time.normal);
      I.click(this.confirmButton);
      I.waitForText(this.message.incorrectOTP, timePage.time.normal);
    }
  };