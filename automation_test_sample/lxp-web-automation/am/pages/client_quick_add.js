// enable I and another page object
const { I } = inject();
const menuPage = require('./menu');
const clientPage = require('./client');
const timePage = require('../../utils/time');
const clientDetailPage = require('./client_detail')

module.exports =
{
  inviteQuickAddForm: {
    addMoreButton: '//div[contains(@class, \'InviteClientModal__add-btn\')]',
    secondEmail: '(//input[@type=\'email\'])[2]',
    secondCodeName: '(//input[@name =\'name\'])[2]',
    secondPhoneNumber: '(//input[@type=\'tel\'])[2]',
    secondPassword: '(//input[@type=\'password\'])[2]'
  },

  /**
   * LXN-141
   * @param {*} client 
   */
  createClient(client) {
    I.say('Add new client with valid value by quick add');
    this.addValueSignleClient(client);
    I.waitForElement(clientPage.inviteButton.active, timePage.time.normal);
    I.waitForVisible(clientPage.inviteButton.active, timePage.time.normal);
    I.click(clientPage.inviteButton.active);
    I.waitForText(client.username, clientDetailPage.head.name);
  },

  /**
   * LXN-142
   * @param {*} client 
   * @param {*} client1 
   */
  clientMuiltClient(client, client1) {
    I.say('Add 2 cliens with valid value by quick add');
    this.addValueSignleClient(client);
    I.waitForVisible(this.inviteQuickAddForm.addMoreButton, timePage.time.normal);
    I.click(this.inviteQuickAddForm.addMoreButton);
    I.waitForVisible(this.inviteQuickAddForm.secondEmail, timePage.time.normal);
    I.fillField(this.inviteQuickAddForm.secondCodeName, client1.username);
    I.fillField(this.inviteQuickAddForm.secondEmail, client1.email);
    I.fillField(this.inviteQuickAddForm.secondPhoneNumber, client1.mobilePhoneNumber);
    I.fillField(this.inviteQuickAddForm.secondPassword, client1.password);
    I.waitForElement(clientPage.inviteButton.active, timePage.time.normal);
    I.waitForVisible(clientPage.inviteButton.active, timePage.time.normal);
    I.click(clientPage.inviteButton.active);
    I.waitForText(client1.username, clientDetailPage.head.name);
  },

  /**
   * 
   * @param {*} client 
   */
  missingCodeName(client) {
    I.say('Add new client with missing code name');
    this.openFormInviteQuickAdd();
    I.waitForVisible(clientPage.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(clientPage.inviteForm.phoneNumber, client.mobilePhoneNumber);

    I.waitForVisible(clientPage.inviteForm.email, timePage.time.normal);
    I.fillField(clientPage.inviteForm.email, client.email);

    I.waitForVisible(clientPage.inviteForm.password, timePage.time.normal);
    I.fillField(clientPage.inviteForm.password, client.password);

    I.dontSeeElement(clientPage.inviteButton.active);

    I.waitForVisible(clientPage.inviteForm.codeName, timePage.time.normal);
    I.fillField(clientPage.inviteForm.codeName, client.username);

    I.waitForVisible(clientPage.inviteButton.active);
  },

  /**
   * 
   * @param {*} client 
   */
  missingEmail(client) {
    I.say('Add missing email');
    this.openFormInviteQuickAdd();
    I.waitForVisible(clientPage.inviteForm.codeName, timePage.time.normal);
    I.fillField(clientPage.inviteForm.codeName, client.username);
    I.waitForVisible(clientPage.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(clientPage.inviteForm.phoneNumber, client.mobilePhoneNumber);
    I.waitForVisible(clientPage.inviteForm.password, timePage.time.normal);
    I.fillField(clientPage.inviteForm.password, client.password);
    I.dontSeeElement(clientPage.inviteButton.active);
    I.waitForVisible(clientPage.inviteForm.email, timePage.time.normal);
    I.fillField(clientPage.inviteForm.email, client.email);

    I.waitForVisible(clientPage.inviteButton.active);
  },

  /**
   * 
   * @param {*} client 
   */
  missingPassword(client) {
    I.say('Add missing password');
    this.openFormInviteQuickAdd();
    I.waitForVisible(clientPage.inviteForm.codeName, timePage.time.normal);
    I.fillField(clientPage.inviteForm.codeName, client.username);
    I.waitForVisible(clientPage.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(clientPage.inviteForm.phoneNumber, client.mobilePhoneNumber);
    I.waitForVisible(clientPage.inviteForm.email, timePage.time.normal);
    I.fillField(clientPage.inviteForm.email, client.email);
    I.dontSeeElement(clientPage.inviteButton.active);
    I.waitForVisible(clientPage.inviteForm.password, timePage.time.normal);
    I.fillField(clientPage.inviteForm.password, client.password);
    I.waitForVisible(clientPage.inviteButton.active);
  },

  /**
   * 
   * @param {*} client 
   */
  missingPhone(client) {
    I.say('Add missing phone number');
    this.openFormInviteQuickAdd();
    I.waitForVisible(clientPage.inviteForm.codeName, timePage.time.normal);
    I.fillField(clientPage.inviteForm.codeName, client.username);
    I.waitForVisible(clientPage.inviteForm.password, timePage.time.normal);
    I.fillField(clientPage.inviteForm.password, client.password);
    I.waitForVisible(clientPage.inviteForm.email, timePage.time.normal);
    I.fillField(clientPage.inviteForm.email, client.email);
    I.dontSeeElement(clientPage.inviteButton.active);
    I.waitForVisible(clientPage.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(clientPage.inviteForm.phoneNumber, client.mobilePhoneNumber);
    I.waitForVisible(clientPage.inviteButton.active);
  },

  /**
   * LXN-143
   * @param {*} client 
   */
  invalidPassword(client, value) {
    I.say('Add invalid  password');
    this.openFormInviteQuickAdd();
    I.waitForVisible(clientPage.inviteForm.codeName, 30);
    I.fillField(clientPage.inviteForm.codeName, client.username);

    I.waitForVisible(clientPage.inviteForm.phoneNumber, 30);
    I.fillField(clientPage.inviteForm.phoneNumber, client.mobilePhoneNumber);

    I.waitForVisible(clientPage.inviteForm.password, 30);
    I.fillField(clientPage.inviteForm.password, value);

    I.waitForVisible(clientPage.inviteForm.email, 30);
    I.fillField(clientPage.inviteForm.email, client.email);

    I.dontSeeElement(clientPage.inviteButton.active);

    I.click(clientPage.inviteForm.password);

    for (i = 0; i < value.length; i++) {
      I.pressKey('Backspace');
    }

    I.fillField(clientPage.inviteForm.password, client.password);

    I.waitForVisible(clientPage.inviteButton.active);
  },

  /**
   * LXN-144
   * @param {*} client 
   */
  invalidEmail(client) {
    I.say('Add invalid Email');
    this.openFormInviteQuickAdd();
    I.waitForVisible(clientPage.inviteForm.codeName, timePage.time.normal);
    I.fillField(clientPage.inviteForm.codeName, client.username);

    I.waitForVisible(clientPage.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(clientPage.inviteForm.phoneNumber, client.mobilePhoneNumber);

    I.waitForVisible(clientPage.inviteForm.password, timePage.time.normal);
    I.fillField(clientPage.inviteForm.password, client.password);

    I.waitForVisible(clientPage.inviteForm.email, timePage.time.normal);

    emailIncorrect = client.email + client.email;

    I.fillField(clientPage.inviteForm.email, emailIncorrect);

    I.dontSeeElement(clientPage.inviteButton.active);

    for (i = 0; i < emailIncorrect.length; i++) {
      I.pressKey('Backspace');
    }

    I.fillField(clientPage.inviteForm.email, client.email);
    I.fillField(clientPage.inviteForm.phoneNumber, client.mobilePhoneNumber);
    I.waitForVisible(clientPage.inviteButton.active);
  },

  /**
   * LXN-145
   * @param {*} client 
   */
  invalidPhone(client) {
    I.say('Add invalid phone number');
    this.openFormInviteQuickAdd();
    I.waitForVisible(clientPage.inviteForm.codeName, timePage.time.normal);
    I.fillField(clientPage.inviteForm.codeName, client.username);

    invalidPhone = client.mobilePhoneNumber + client.mobilePhoneNumber;
    I.waitForVisible(clientPage.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(clientPage.inviteForm.phoneNumber, invalidPhone);

    I.waitForVisible(clientPage.inviteForm.password, timePage.time.normal);
    I.fillField(clientPage.inviteForm.password, client.password);

    I.waitForVisible(clientPage.inviteForm.email, timePage.time.normal);
    I.fillField(clientPage.inviteForm.email, client.email);

    I.dontSeeElement(clientPage.inviteButton.active);

    I.click(clientPage.inviteForm.phoneNumber);
    for (i = 0; i < invalidPhone.length; i++) {
      I.pressKey('Backspace');
    }

    I.fillField(clientPage.inviteForm.phoneNumber, client.mobilePhoneNumber);

    I.waitForVisible(clientPage.inviteButton.active);
  },

  /**
   * LXN-146
   * @param {*} client 
   */
  codeNameExisted(client) {
    I.say('Add code name existed');
    this.openFormInviteQuickAdd();
    I.waitForVisible(clientPage.inviteForm.codeName, timePage.time.normal);
    I.fillField(clientPage.inviteForm.codeName, client.username);

    I.waitForVisible(clientPage.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(clientPage.inviteForm.phoneNumber, client.mobilePhoneNumber);

    I.waitForVisible(clientPage.inviteForm.password, timePage.time.normal);
    I.fillField(clientPage.inviteForm.password, client.password);

    I.waitForVisible(clientPage.inviteForm.email, timePage.time.normal);
    I.fillField(clientPage.inviteForm.email, client.email);

    I.wait(timePage.time.waitOpenForm);
    I.waitForElement(clientPage.inviteForm.error, timePage.time.normal);
  },

  /**
   * 
   * @param {*} client 
   */
  codeNameMoreThan32Char(client) {
    I.say('Add code name more than 32');
    this.openFormInviteQuickAdd();
    I.waitForVisible(clientPage.inviteForm.codeName, timePage.time.normal);
    usernameInvalid = client.username + client.username + client.username + client.username;
    I.fillField(clientPage.inviteForm.codeName, usernameInvalid);

    I.waitForVisible(clientPage.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(clientPage.inviteForm.phoneNumber, client.mobilePhoneNumber);

    I.waitForVisible(clientPage.inviteForm.password, timePage.time.normal);
    I.fillField(clientPage.inviteForm.password, client.password);

    I.waitForVisible(clientPage.inviteForm.email, timePage.time.normal);
    I.fillField(clientPage.inviteForm.email, client.email);

    I.dontSeeElement(clientPage.inviteButton.active);
  },

  openFormInviteQuickAdd() {
    clientPage.openFormInvite();
    I.waitForVisible(clientPage.inviteForm.quickAdd, timePage.time.normal);
    I.click(clientPage.inviteForm.quickAdd);
    I.waitForVisible(clientPage.inviteForm.codeName, timePage.time.normal);
  },

  /**
   * 
   * @param {*} client 
   */
  addValueSignleClient(client) {
    this.openFormInviteQuickAdd();
    I.say('Add value for client form');
    I.waitForVisible(clientPage.inviteForm.codeName, timePage.time.normal);
    I.fillField(clientPage.inviteForm.codeName, client.username);
    I.waitForVisible(clientPage.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(clientPage.inviteForm.phoneNumber, client.mobilePhoneNumber);
    I.waitForVisible(clientPage.inviteForm.email, timePage.time.normal30);
    I.fillField(clientPage.inviteForm.email, client.email);
    I.waitForVisible(clientPage.inviteForm.password, timePage.time.normal);
    I.fillField(clientPage.inviteForm.password, client.password);
  }
};