// enable I and another page object
const { I } = inject();
const menuPage = require('./menu');
const timePage = require('../../utils/time');
const clientDetailPage = require('./client_detail');
module.exports =
{
  head: {
    name: '//span[contains(@class, \'ContactHeader__username\')]',
    chat: '//div[contains(@class, \'ContactHeader__buttons\')]'
  },

  tab: {
    contact: '(//a[contains(@class, \'Contact__button\')])[1]',
    transactions: '(//a[contains(@class, \'Contact__button\')])[2]',
    notes: '(//a[contains(@class, \'Contact__button\')])[3]',
    integrations: '(//a[contains(@class, \'Contact__button\')])[4]'
  },

  contactTab: {
    editButton: '//div[contains(@class, \'Profile__btn-item\')]',
    cancelButton: '//div[contains(@class, \'Profile__button-section\')]/div[1]',
    saveButton: '//div[contains(@class, \'Profile__button-section\')]/div[2]',
    changeAvatar: '//input[contains(@class, \'Profile__upload-input\')]',
    avatarValue: '//div[contains(@class, \'Profile__avatar-wrapper\')]/img',
    avatarHeader: '//div[contains(@class, \'ContactHeader__avatar-wrapper\')]/img',
    firstName: '//input[@name=\'First Name\']',
    lastName: '//input[@name=\'Last Name\']',
    resetPassword: '//div[contains(@class, \'Profile__password\')]',
    bio: '//input[@name=\'Bio\']',
    email: '//input[@name=\'Email\']',
    phone: '//input[@name=\'Phone Number\']',
    linkedin: '//input[@name=\'Linkedin\']',
    linkImage: 'https://minio-public.qa.leapxpert.app/avatars'
  },

  message: {
    success: 'Profile updated successfully!'
  },

  editValidValue(client) {
    this.addValueForClient(client);
    I.waitForVisible(this.contactTab.saveButton, timePage.time.normal);
    I.click(this.contactTab.saveButton);

    I.waitForText(this.message.success, timePage.time.normal);
    I.waitForText(client.email, timePage.time.normal);
    I.waitForText(client.firstName, timePage.time.normal);
    I.waitForText(client.email, timePage.time.normal);
    I.waitForText(client.mobilePhoneNumber);
    I.waitForText(client.linkedin, timePage.time.normal);
  },

  /**
   * LXN-299
   * @param {*} client 
   */
  uploadAvatar(client, imageUrl) {
    this.startEditForm();
    I.waitForVisible(this.contactTab.saveButton, timePage.time.normal);
    I.wait(timePage.time.waitOpenForm);
    I.attachFile(this.contactTab.changeAvatar, imageUrl);
    I.wait(timePage.time.waitOpenForm);
    I.waitForVisible(this.contactTab.saveButton, timePage.time.normal);
    I.click(this.contactTab.saveButton);
    I.wait(timePage.time.waitOpenForm);
    I.wait(timePage.time.waitForInputOnFiled);
    I.waitForVisible(this.contactTab.editButton, timePage.time.normal);
    I.click(this.contactTab.editButton);
  },

  editMissingPhone(client) {
    this.startEditForm(client);
    I.click(this.contactTab.phone);

    for (i = 0; i < client.mobilePhoneNumber.length; i++) {
      I.pressKey('Backspace');
    }

    I.click(this.contactTab.bio);
    I.fillField(this.contactTab.bio, client.bio);
    I.waitForVisible(this.contactTab.saveButton, timePage.time.normal);
    I.click(this.contactTab.saveButton);
    I.dontSee(this.message.success);
  },

  addValueForClient(client) {
    this.startEditForm();
    I.fillField(this.contactTab.firstName, client.firstName);
    I.fillField(this.contactTab.lastName, client.lastName);
    I.fillField(this.contactTab.bio, client.bio);
    I.fillField(this.contactTab.email, client.email);
    I.fillField(this.contactTab.phone, client.mobilePhoneNumber);
    I.fillField(this.contactTab.linkedin, client.linkedin);
  },

  startEditForm() {
    I.wait(timePage.time.waitOpenForm);
    I.wait(timePage.time.waitOpenForm);
    I.wait(timePage.time.waitOpenForm);
    I.wait(timePage.time.waitOpenForm);
    I.waitForVisible(this.contactTab.editButton, timePage.time.normal);
    I.click(this.contactTab.editButton);
    I.waitForElement(this.contactTab.saveButton, timePage.time.normal);
    I.waitForElement(this.contactTab.firstName, timePage.time.normal);
  }
};