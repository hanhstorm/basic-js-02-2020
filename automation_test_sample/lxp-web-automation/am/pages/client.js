// enable I and another page object
const { I } = inject();
const menuPage = require('./menu');
const timePage = require('../../utils/time');
const clientDetailPage = require('./client_detail')
module.exports =
{
  addPopup: {
    invite: '//div[contains(@class, \'NavSideHeader__row-popup\')][1]',
    shareContact: '//div[contains(@class, \'NavSideHeader__header-popup\')]/div[2]'
  },

  user: {
    name: '//span[3]/input',
    password: '//span[4]/input'
  },

  inviteForm: {
    header: '//div[contains(@class, \'InviteClientModal__modal-header\')]',
    codeName: '//input[@name=\'name\']',
    phoneNumber: '//input[@type=\'tel\']',
    email: '//input[@type=\'email\']',
    password: '//input[@type=\'password\']',
    quickAdd: '//div[contains(@class, \'InviteClientModal__quick-add-button\')]',
    error: '//div[contains(@class, \'ErrorElement__container\')]'
  },

  quickAddHeader: {
    backButton: '//div[contains(@class, \'InviteClientModal__back-button\')]'
  },

  quickAddForm: {
    codeName: ' //input[@name=\'name\']',
    phoneNumber: '//input[@type=\'tel\']',
    email: '//input[@type=\'email\']',
    password: '//input[@type=\'password\']',
  },

  inviteButton:
  {
    active: '//div[contains(@class, \'InviteClientModal__active\')]',
    inactive: '//div[contains(@class, \'InviteClientModal__button\')]'
  },

  countryOption: {
    country: '//div[@id=\'select-country\']'
  },

  message: {
    success: 'Client has been invited successfully'
  },

  /**
   * 
   * @param {*} client 
   */
  createClient(client) {
    I.say('Create new client with valid value');
    this.addValueForClientForm(client);
    I.waitForElement(this.inviteButton.active, timePage.time.normal);
    I.waitForVisible(this.inviteButton.active, timePage.time.normal);
    I.click(this.inviteButton.active);
    I.waitForText(client.username, clientDetailPage.head.name);
  },

  /**
   * 
   * @param {*} client 
   */
  addValueForClientForm(client) {
    this.openFormInvite();
    I.say('Add value for client form');
    I.waitForVisible(this.inviteForm.codeName, timePage.time.normal);
    I.fillField(this.inviteForm.codeName, client.username);
    I.waitForVisible(this.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(this.inviteForm.phoneNumber, client.mobilePhoneNumber);
    I.waitForVisible(this.inviteForm.email, timePage.time.normal);
    I.fillField(this.inviteForm.email, client.email);
    I.waitForVisible(this.countryOption.country, timePage.time.normal);
    I.click(this.countryOption.country);
    I.waitForVisible(this.resultsOption(client.country), timePage.time.normal);
    I.click(this.resultsOption(client.country));
    I.waitForVisible(this.inviteForm.password, timePage.time.normal);
    I.fillField(this.inviteForm.password, client.password);
  },

  /**
   * 
   * @param {*} client 
   */
  invalidPassWord(client) {
    I.say('Add with invalid password');
    this.addValueForClientForm(client);
    I.waitForVisible(this.inviteForm.password, timePage.time.normal);
    I.clearField(this.inviteForm.password);
    I.fillField(this.inviteForm.password, '1231');
    I.waitForVisible(this.inviteButton.inactive, timePage.time.normal);
    I.scrollTo(this.inviteButton.inactive);
    I.clearField(this.inviteForm.password);
    I.fillField(this.inviteForm.password, client.password);
    I.waitForVisible(this.inviteButton.active, timePage.time.normal);
  },

  // LXN-138
  /**
   * 
   * @param {*} client 
   */
  invalidEmail(client) {
    I.say('Check invalid email');
    this.addValueForClientForm(client);
    I.clearField(this.inviteForm.email);
    I.waitForVisible(this.inviteForm.email, timePage.time.normal);
    I.fillField(this.inviteForm.email, 'email');

    I.waitForVisible(this.inviteForm.password, timePage.time.normal);
    I.fillField(this.inviteForm.password, client.password);

    I.waitForVisible(this.inviteButton.inactive, timePage.time.normal);
    I.clearField(this.inviteForm.email);
    I.fillField(this.inviteForm.email, client.email);

    I.waitForVisible(this.inviteButton.active, timePage.time.normal);
  },

  // LXN-137
  /**
   * 
   * @param {*} client 
   */
  invalidPhone(client) {
    this.openFormInvite();
    I.say('Check invalid phone');
    I.waitForVisible(this.inviteForm.codeName, timePage.time.normal);
    I.fillField(this.inviteForm.codeName, client.username);
    I.fillField(this.inviteForm.phoneNumber, '123');
    I.waitForVisible(this.inviteForm.email, timePage.time.normal);
    I.fillField(this.inviteForm.email, client.email);
    I.waitForVisible(this.countryOption.country, timePage.time.normal);
    I.click(this.countryOption.country);
    I.waitForVisible(this.resultsOption(client.country), timePage.time.normal);
    I.click(this.resultsOption(client.country));
    I.waitForVisible(this.inviteForm.password, timePage.time.normal);
    I.fillField(this.inviteForm.password, client.password);
    I.dontSeeElement(this.inviteButton.active, timePage.time.normal);
    I.clearField(this.inviteForm.phoneNumber);
    I.fillField(this.inviteForm.phoneNumber, client.mobilePhoneNumber);
    I.waitForVisible(this.inviteButton.active, timePage.time.normal);
  },

  /**
   * 
   * @param {*} client 
   */
  missingCodeName(client) {
    I.say('Create with missing code name');
    this.openFormInvite();
    I.waitForVisible(this.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(this.inviteForm.phoneNumber, client.mobilePhoneNumber);
    I.waitForVisible(this.inviteForm.email, timePage.time.normal);
    I.fillField(this.inviteForm.email, client.email);
    I.waitForVisible(this.countryOption.country, timePage.time.normal);
    I.click(this.countryOption.country);
    I.waitForVisible(this.resultsOption(client.country), timePage.time.normal);
    I.click(this.resultsOption(client.country));
    I.waitForVisible(this.inviteForm.password, timePage.time.normal);
    I.fillField(this.inviteForm.password, client.password);
    I.dontSeeElement(this.inviteButton.active, timePage.time.normal);

    I.fillField(this.inviteForm.codeName, client.username);
    I.waitForVisible(this.inviteButton.active, timePage.time.normal);
  },

  /**
   * 
   * @param {*} client 
   */
  addValueForClientForm(client) {
    this.openFormInvite();
    I.waitForVisible(this.inviteForm.codeName, timePage.time.normal);
    I.fillField(this.inviteForm.codeName, client.username);
    I.waitForVisible(this.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(this.inviteForm.phoneNumber, client.mobilePhoneNumber);
    I.waitForVisible(this.inviteForm.email, timePage.time.normal);
    I.fillField(this.inviteForm.email, client.email);
    I.waitForVisible(this.countryOption.country, timePage.time.normal);
    I.click(this.countryOption.country);
    I.waitForVisible(this.resultsOption(client.country), timePage.time.normal);
    I.click(this.resultsOption(client.country));
    I.waitForVisible(this.inviteForm.password, timePage.time.normal);
    I.fillField(this.inviteForm.password, client.password);
  },

  /**
   * 
   * @param {*} client 
   */
  missingPhone(client) {
    this.openFormInvite();
    I.say('Missing phone number');
    I.waitForVisible(this.inviteForm.codeName, timePage.time.normal);
    I.fillField(this.inviteForm.codeName, client.username);
    I.waitForVisible(this.inviteForm.email, timePage.time.normal);
    I.fillField(this.inviteForm.email, client.email);
    I.waitForVisible(this.countryOption.country, timePage.time.normal);
    I.click(this.countryOption.country);
    I.waitForVisible(this.resultsOption(client.country), timePage.time.normal);
    I.click(this.resultsOption(client.country));
    I.waitForVisible(this.inviteForm.password, timePage.time.normal);
    I.fillField(this.inviteForm.password, client.password);
    I.dontSeeElement(this.inviteButton.active, timePage.time.normal);
    I.fillField(this.inviteForm.phoneNumber, client.mobilePhoneNumber);
    I.waitForVisible(this.inviteButton.active, timePage.time.normal);
  },

  /**
   * 
   * @param {*} client 
   */
  missingEmail(client) {
    this.openFormInvite();
    I.say('Check missing email');
    I.waitForVisible(this.inviteForm.codeName, timePage.time.normal);
    I.fillField(this.inviteForm.codeName, client.username);
    I.waitForVisible(this.inviteForm.phoneNumber, timePage.time.normal);
    I.fillField(this.inviteForm.phoneNumber, client.mobilePhoneNumber);
    I.waitForVisible(this.countryOption.country, timePage.time.normal);
    I.click(this.countryOption.country);
    I.waitForVisible(this.resultsOption(client.country), timePage.time.normal);
    I.click(this.resultsOption(client.country));
    I.waitForVisible(this.inviteForm.password, timePage.time.normal);
    I.fillField(this.inviteForm.password, client.password);
    I.dontSeeElement(this.inviteButton.active, timePage.time.normal);
    I.waitForVisible(this.inviteForm.email, timePage.time.normal);
    I.fillField(this.inviteForm.email, client.email);
    I.waitForVisible(this.inviteButton.active, timePage.time.normal);
  },

  /**
   * LXN-135
   * 
   * @param {*} client 
   * @param {*} codeName 
   */
  codeMoreThan64(client, codeName) {
    I.say('Check with username more than 64 character');
    this.addValueForClientForm(client);
    I.dontSeeElement(this.inviteButton.active, timePage.time.normal);
    I.clearField(this.inviteForm.codeName);
    I.fillField(this.inviteForm.codeName, codeName);
    I.waitForVisible(this.inviteButton.active, timePage.time.normal);
  },

  /**
   * 
   * @param {*} client 
   * @param {*} codeName 
   * @param {*} email 
   */
  usernameEmailExisted(client, codeName, email) {
    I.say('check username and email exised');
    this.addValueForClientForm(client);
    I.dontSeeElement(this.inviteButton.active, timePage.time.normal);
    I.clearField(this.inviteForm.codeName);
    I.fillField(this.inviteForm.codeName, codeName);
    I.dontSeeElement(this.inviteButton.active, timePage.time.normal);
    I.clearField(this.inviteForm.email);
    I.fillField(this.inviteForm.email, email);
    I.waitForElement(this.inviteButton.active, timePage.time.normal);
  },

  /**
   * Client open invite form
   */
  openFormInvite() {
    I.say('Open Invite form');
    I.waitForElement(menuPage.menu.contact, timePage.time.normal);
    I.waitForVisible(menuPage.menu.contact, timePage.time.normal);
    I.click(menuPage.menu.contact);
    I.wait(timePage.time.waitOpenForm);
    I.waitForVisible(menuPage.header.addButton, timePage.time.normal);
    I.click(menuPage.header.addButton);
    I.wait(timePage.time.waitOpenForm);
    I.waitForElement(this.addPopup.invite, timePage.time.normal);
    I.waitForVisible(this.addPopup.invite, timePage.time.normal);
    I.click(this.addPopup.invite);
    I.waitForVisible(this.inviteForm.header, timePage.time.normal);
  },
  /**
   * 
   * @param {*} option 
   */
  resultsOption(option) {
    return `//li[contains(text(), \'${option}\')]`;
  }
};