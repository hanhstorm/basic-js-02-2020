const { I, clientDetailPage } = inject();
let faker = require('faker');
let client;
let imageUrl = 'am/data/leap.png';
const {
  buildClientEdit
} = require('../data/client_edit_data');

const {
  buildClient
} = require('../data/client_data');

Then('AM have check update avatar for client', async () => {
  clientDetailPage.uploadAvatar(client, imageUrl);
  I.waitForElement(clientDetailPage.contactTab.avatarHeader, 30);
  let linkImage = await I.grabAttributeFrom(clientDetailPage.contactTab.avatarHeader, 'src');
  I.amOnPage(linkImage);
  I.seeInCurrentUrl(clientDetailPage.contactTab.linkImage);
});
