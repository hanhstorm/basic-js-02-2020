const { I, clientPage } = inject();
let faker = require('faker');
let client, clientCodeMoreThan64, validCodeName, validEmail, clientCheckInvalid;
const {
  buildClient,
  buildClientCodeMoreThan64
} = require('../data/client_data');

const {
  users
} = require('../data/login_data');

Given('AM have create client data',() => {
  client = buildClient();
  clientCodeMoreThan64 = buildClientCodeMoreThan64();
  validCodeName = faker.random.number() + 'client' + faker.random.locale().toLowerCase();
  validEmail = faker.internet.email();
});

Then('AM have create client with valid value',() => {
  clientPage.createClient(client);
});

Given('AM have create client data to check invalid cases',() => {
  clientCheckInvalid = buildClient();
});


Then('AM have create client with invalid phone',() => {
  I.refreshPage();
  clientPage.invalidPhone(clientCheckInvalid);
});

Then('AM have create client with invalid password',() => {
  I.refreshPage();
  clientPage.invalidPassWord(clientCheckInvalid);
});

Then('AM have create client with invalid Email',() => {
  I.refreshPage();
  clientPage.invalidEmail(clientCheckInvalid);
});

Then('AM have create client with missing code name',() => {
  I.refreshPage();
  clientPage.missingCodeName(clientCheckInvalid);
});


Then('AM have create client with missing phone',() => {
  I.refreshPage();
  clientPage.missingPhone(clientCheckInvalid);
});

Then('AM have create client with missing Email',() => {
  I.refreshPage();
  clientPage.missingEmail(clientCheckInvalid);
});

Then('AM have create client with code name more than 64 character',() => {
  I.refreshPage();
  clientPage.codeMoreThan64(clientCodeMoreThan64, client.username);
});

Then('AM have create client with username and email existed',() => {
  I.refreshPage();
  clientPage.usernameEmailExisted(client, validCodeName, validEmail);
});
