const { I, clientQuickAddPage } = inject();
let faker = require('faker');
let client, firstClient, secondClient, clientCodeMoreThan64,validValue;
const {
  users
} = require('../data/client_data');

const {
  buildClient,
  buildFirstClient,
  buildSecondClient
} = require('../data/client_data');

Given('AM have create invalid client data',() => {
  client = buildClient();
  firstClient = buildFirstClient();
  secondClient = buildSecondClient();
  validValue = faker.random.number() + 'client' + faker.random.locale().toLowerCase();
});


Given('AM have create valid client data',() => {
  client = buildClient();
  firstClient = buildFirstClient();
  secondClient = buildSecondClient();
  validCodeName = faker.random.number() + 'client' + faker.random.locale().toLowerCase();
});

Then('AM have check with create quick client missing code name',() => {
  clientQuickAddPage.missingCodeName(client);
});

Then('AM have create quick client with missing phone',() => {
  I.refreshPage();
  clientQuickAddPage.missingPhone(client);
});

Then('AM have create quick client with missing Email',() => {
  I.refreshPage();
  clientQuickAddPage.missingEmail(client);
});

Then('AM have create quick client with missing password',() => {
  I.refreshPage();
  clientQuickAddPage.missingPassword(client);
});

Then('AM have create quick client with invalid Email',() => {
  I.refreshPage();
  clientQuickAddPage.invalidEmail(client);
});


Then('AM have create quick client with invalid password',() => {
  I.refreshPage();
  clientQuickAddPage.invalidPassword(client, validValue);
});


Then('AM have create quick client with invalid email',() => {
  I.refreshPage();
  clientQuickAddPage.invalidEmail(client);
});

Then('AM have create quick client with invalid phone',() => {
  I.refreshPage();
  clientQuickAddPage.invalidPhone(client);
});

Then('AM have create quick client with code name more than 32 character',() => {
  I.refreshPage();
  clientQuickAddPage.codeNameMoreThan32Char(client);
});

Given('AM update email of client',() => {
  client.email = faker.internet.email().toLowerCase();
});

Given('AM update username of client',() => {
  client.email = faker.internet.email().toLowerCase();
  firstClient.username = faker.random.number() + 'client' + faker.random.locale().toLowerCase();
});


Then('AM have create quick client with username existed',() => {
  I.refreshPage();
  clientQuickAddPage.codeNameExisted(client);
});


Then('AM have create quick muilt clients',() => {
  I.refreshPage();
  clientQuickAddPage.clientMuiltClient(firstClient, secondClient);
});

Then('AM have create quick single client',() => {
  I.refreshPage();
  clientQuickAddPage.createClient(client);
});

Then('AM have create quick client with email existed',() => {
  I.refreshPage();
  clientQuickAddPage.codeNameExisted(client);
});
