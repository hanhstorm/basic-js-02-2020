const { I, loginPage } = inject();

const {
  users
} = require('../data/login_data');

Then('AM have login with correct usename and password',() => {
    loginPage.sendCompany(users[0]);
    loginPage.sendUser(users[0]);
});

Then('AM have check invalid input company',() => {
    loginPage.invalidCompany(users[0]);
   
});

Then('AM have check invalid input useranme',() => {
  loginPage.sendCompany(users[0]);
  loginPage.invalidUsername(users[0]);
});

Then('AM have check invalid input password',() => {
  loginPage.sendCompany(users[0]);
  loginPage.invalidPassword(users[0]);
});

Then('AM have check invalid input OTP code',() => {
  loginPage.sendCompany(users[0]);
  loginPage.invalidCode(users[0]);
});
