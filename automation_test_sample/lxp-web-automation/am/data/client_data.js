let faker = require('faker');

const buildClient = () => {
  let client = {
    username: 'automation' + faker.random.number() + 'client' + faker.random.locale().toLowerCase(),
    mobilePhoneNumber: '2015550125',
    email: faker.internet.email().toLowerCase(),
    country: 'Albania',
    currency: 'USD',
    password: 'Test@123'
  }
  return client;
}

const buildFirstClient = () => {
  let client = {
    username:'automation' + faker.random.number() + 'client' + faker.random.locale().toLowerCase(),
    mobilePhoneNumber: '2015550125',
    email: faker.internet.email().toLowerCase(),
    country: 'Albania',
    currency: 'USD',
    password: 'Test@123'
  }
  return client;
}


const buildSecondClient = () => {
  let client = {
    username: 'automation' + faker.random.number() + 'client' + faker.random.locale().toLowerCase(),
    mobilePhoneNumber: '2015550125',
    email: faker.internet.email().toLowerCase(),
    country: 'Albania',
    currency: 'USD',
    password: 'Test@123'
  }
  return client;
}

const buildClientCodeMoreThan64 = () => {
  let client = {
    username: 'automation' + faker.random.number() + 'clLoremipsumissimplydummytextoftheprintingandtypesettingindustryloremipsuhasbeentheindustry' + faker.random.locale().toLowerCase(),
    mobilePhoneNumber: '2015550125',
    email: faker.internet.email(),
    country: 'Albania',
    currency: 'USD',
    password: 'Test@123'
  }
  return client;
}

module.exports = {
  buildClient,
  buildFirstClient,
  buildClientCodeMoreThan64,
  buildSecondClient
}
