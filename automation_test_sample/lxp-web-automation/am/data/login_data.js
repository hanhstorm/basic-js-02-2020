let faker = require('faker');

const users = [{
  // company: process.env.AUTOMATION_COMPANY,
  // username: process.env.AUTOMATION_USERNAME,
  // password: process.env.AUTOMATION_PASSWORD

  // company: 'codelink',
  // username: 'anh.am01',
  // password: 'Sandworm1$'

  company: 'leapxpertqa',
  username: 'qa1',
  password: 'Testing@123'
},
{
  company: 'codelink',
  username: 'anh.am01',
  password: 'Sandworm1$'
}];

const fakeUsers = () => {
  users[1].name = faker.name.findName();
  users[1].password = faker.date.between();
  return users;
};

module.exports = {
  users,
  fakeUsers
}