let faker = require('faker');

const buildClientEdit = () => {
  let client = {
    username: 'automation' + faker.random.number() + 'client' + faker.random.locale().toLowerCase(),
    mobilePhoneNumber: '2015550125',
    firstName: 'automation' + faker.name.findName(),
    lastName:'automation' + faker.name.findName(),
    bio: faker.name.findName(),
    email: faker.internet.email().toLowerCase(),
    linkedin: faker.internet.url(),
    country: 'Albania',
    currency: 'USD',
    password: 'Test@123'
  }
  return client;
}

module.exports = {
  buildClientEdit
}
