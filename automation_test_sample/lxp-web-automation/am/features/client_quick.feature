Feature: Business rules
  In order to achieve my goals
  As a persona
  I want to be able to check all cases on quick add clients
  Scenario: AM check all cases for quick add clients
    Given AM have login with correct usename and password
    Given AM have create invalid client data
    Then AM have check with create quick client missing code name
    Then AM have create quick client with missing phone
    Then AM have create quick client with missing Email
    Then AM have create quick client with missing password
    Then AM have create quick client with invalid Email
    Then AM have create quick client with invalid password
    Then AM have create quick client with invalid email
    Then AM have create quick client with invalid phone
    Then AM have create quick client with code name more than 32 character
    Given AM have create valid client data
    Then AM have create quick single client
    Then AM have create quick muilt clients
    Given AM update email of client
    Then AM have create quick client with username existed
    Given AM update username of client
    Then AM have create quick client with username existed