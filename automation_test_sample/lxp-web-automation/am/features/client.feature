Feature: Business rules
  In order to achieve my goals
  As a persona
  I want to be able to check all cases on client
  Scenario: AM check all cases for client 
    Given AM have login with correct usename and password
    Given AM have create client data
    Then AM have create client with valid value
    Then AM have create client with username and email existed
    Given AM have create client data to check invalid cases
    Then AM have create client with invalid phone
    Then AM have create client with invalid password
    Then AM have create client with invalid Email
    Then AM have create client with missing code name
    Then AM have create client with missing phone
    Then AM have create client with missing Email
    Then AM have create client with code name more than 64 character
