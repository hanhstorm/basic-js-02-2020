const faker = require('faker');

const COMPANY = {
    "amCount": 501,
    "creationTimestamp": "2019-09-26T03:29:29.903Z",
    "description": "",
    "domainName": "",
    "fullName": "codelink",
    "id": "5d635219600dcf142bed93b5",
    "location": "hcm",
    "logoUrl": "",
    "name": "codelink",
    "resellerId": "",
    "shareClients": false,
    "sso": false,
    "status": "NEW"
}


const buildEditCompany = (name, id) => {
    const company = {
        ...COMPANY,
        name: name,
        location: 'location' + faker.random.locale().toLowerCase() + faker.random.number(),
        sso: true,
        fullName: 'company' + faker.random.locale().toLowerCase() + faker.random.number(),
        id: id
    };

    const buildUser = {
        company: company
    };

    return buildUser;
}


module.exports = {
    buildEditCompany
};

