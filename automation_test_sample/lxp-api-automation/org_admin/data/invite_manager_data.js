const faker = require('faker');

const PERSONA = {
    "email": "qa@leap.expert",
    "mobilePhoneNumber": "84 97 654 27 18",
    "officePhoneNumber": "84 96 328 29 27",
    "firstName": "expert",
    "lastName": "expert",
    "middleName": "checking",
    "country": 'AX',
    "jobTitle": "Account Manager"
}

const USER = {
    "companyId": "5d635219600dcf142bed93b5",
    "name": "checking",
    "persona": PERSONA,
    "roles": [
        "EXPERT"
    ]
}

const buildManagerData = (companyId) => {
    const persona = {
        ...PERSONA,
        email: faker.internet.email().toLowerCase(),

    };

    const user = {
        ...USER,
        companyId,
        name: 111 + 'manager' + faker.random.locale().toLowerCase() + faker.random.number(),
        persona: persona
    };

    const manager = {
        user: user,
        "password": "Testing1@123",
        "sendEmail": true,
        "emailContent": "checkingnguyen"
    }

    return manager;
}


const buildIncorrectEmailManagerData = (companyId) => {
    const persona = {
        ...PERSONA,
        email: 'checkingemail',

    };

    const user = {
        ...USER,
        companyId,
        name: 111 + 'manager' + faker.random.locale().toLowerCase() + faker.random.number(),
        persona: persona
    };

    const manager = {
        user: user,
        "password": "Testing1@123",
        "sendEmail": true,
        "emailContent": "checkingnguyen"
    }

    return manager;
}

const buildIncorrectUsernameManagerData = (companyId) => {
    const persona = {
        ...PERSONA,
        email: faker.internet.email().toLowerCase(),

    };

    const user = {
        ...USER,
        companyId,
        name: 'manager11Z11111111111111111111111111111111111111111111111111111111111dnhdsduddng' + faker.random.locale().toLowerCase() + faker.random.number(),
        persona: persona
    };

    const manager = {
        user: user,
        "password": "Testing1@123",
        "sendEmail": true,
        "emailContent": "checkingnguyen"
    }

    return manager;
}


const buildIncorrectCompanyId = (companyId) => {
    companyId = companyId + companyId;
    const persona = {
        ...PERSONA,
        email: faker.internet.email().toLowerCase(),

    };

    const user = {
        ...USER,
        companyId,
        name: 111 + 'manager' + faker.random.locale().toLowerCase() + faker.random.number(),
        persona: persona
    };

    const manager = {
        user: user,
        "password": "Testing1@123",
        "sendEmail": true,
        "emailContent": "checkingnguyen"
    }

    return manager;
}

module.exports = {
    buildManagerData,
    buildIncorrectEmailManagerData,
    buildIncorrectUsernameManagerData,
    buildIncorrectCompanyId
};

