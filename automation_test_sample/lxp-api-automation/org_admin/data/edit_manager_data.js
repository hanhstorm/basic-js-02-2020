const faker = require('faker');

const PERSONA = {
    "email": "qa@leap.expert",
    "mobilePhoneNumber": "84 97 654 27 18",
    "officePhoneNumber": "84 96 328 29 27",
    "firstName": "expert",
    "middleName": "checking",
    "lastName": "expert",
    "language": "",
    "profileImageUrl": "",
    "country": "AL",
    "communicationIdentifier": "",
    "jobTitle": "fgfgfg",
    "timezone": "",
    "linkedIn": ""
}

const USER = {
    "companyId": "5d635219600dcf142bed93b5",
    "departmentId": "",
    "name": "chekcing",
    "status": "ACTIVE",
    "riskRating": 0,
    "clientCount": 0,
    "biography": "",
    "persona": PERSONA,
    "creationTimestamp": "2019-08-26T12:00:02.647Z",
    "lastUpdateTimestamp": "2019-10-24T06:24:38.022Z",
    "roles": [
        "EXPERT"
    ]
}

const editManagerData = (companyId, id) => {
    const persona = {
        ...PERSONA,
        email: faker.internet.email().toLowerCase(),
    };

    const user = {
        id: id,
        ...USER,
        persona: persona,
        companyId,
        "isEmailChange": false,
        "success": true,
    };

    const manager = {
        user: user,
        "sendEmail": true,
        "emailContent": "checkingnguyen"
    }

    return manager;
}


module.exports = {
    editManagerData
};

