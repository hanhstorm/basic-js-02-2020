Feature: Business rules
  In order to achieve my goals
  As a org admin
  I want to be able to check all cases for account manager 
  Scenario: Login with valid value
    Given I create new account manager
    Then  I verify create new AM have username already existed
    Then  I verify create new AM have email already existed
    Then  I verify create new AM missing username
    Then  I verify create new AM missing password
    Then  I verify create new AM incorrect username
    Then  I verify create new AM incorrect password