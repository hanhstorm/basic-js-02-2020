Feature: Business rules
  In order to achieve my goals
  As a org admin
  I want to be able to check all cases for update account manager 
  Scenario: Login with valid value
    Given I create new account manager for update
    Then  I verify update missing name for account manager
    Then  I verify update incorrect email for account manager
