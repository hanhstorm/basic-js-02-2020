const expect = require('chai').expect;
const faker = require('faker');
const { I } = inject();
let emailIncorrect;
const managerPage = require('../pages/manager_page');
const codeResults = require('../../code_results');
let managerData, res, headers;
const {
  buildManagerData
} = require('../data/invite_manager_data');

const {
  editManagerData
} = require('../data/edit_manager_data');

Given('I create new account manager for update', async () => {
  emailIncorrect = 'manageremail' + faker.random.locale().toLowerCase() + faker.random.number();
  headers = {
    authorization: `Bearer ${process.env.tokenADMIN_COMPANY}`
  }
  const companyId = process.env.ID_COMPANY;
  const manager = buildManagerData(companyId)
  I.say('Create new AM with valid value');
  res = await I.sendPostRequest(managerPage.url.manager, manager, headers);
  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res.data.user.companyId).to.eql(companyId);
  expect(res.data.user.name).to.eql(manager.user.name);
  expect(res.data.user.persona.email).to.eql(manager.user.persona.email);
  expect(res.data.user.persona.mobilePhoneNumber).to.eql(manager.user.persona.mobilePhoneNumber);
  expect(res.data.user.persona.officePhoneNumber).to.eql(manager.user.persona.officePhoneNumber);
  expect(res.data.user.persona.firstName).to.eql(manager.user.persona.firstName);
  expect(res.data.user.persona.middleName).to.eql(manager.user.persona.middleName);
  expect(res.data.user.persona.lastName).to.eql(manager.user.persona.lastName);
  expect(res.data.user.persona.country).to.eql(manager.user.persona.country);
  expect(res.data.user.persona.jobTitle).to.eql(manager.user.persona.jobTitle);
  expect(res.data.user.persona.email).to.eql(manager.user.persona.email);
  userUpdate = res.data;
  managerData = editManagerData(companyId, res.data.user.id);
  managerData.user.name = res.data.user.name;
  const resEdit = await I.sendPostRequest(managerPage.url.manager, managerData, headers);
  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);

  expect(res.data.user.name).to.eql(resEdit.data.user.name);
  expect(resEdit.data.user.persona.email).to.eql(managerData.user.persona.email);
  expect(resEdit.data.user.persona.mobilePhoneNumber).to.eql(managerData.user.persona.mobilePhoneNumber);
  expect(resEdit.data.user.persona.officePhoneNumber).to.eql(managerData.user.persona.officePhoneNumber);
  expect(resEdit.data.user.persona.firstName).to.eql(managerData.user.persona.firstName);
  expect(resEdit.data.user.persona.middleName).to.eql(managerData.user.persona.middleName);
  expect(resEdit.data.user.persona.lastName).to.eql(managerData.user.persona.lastName);
  expect(resEdit.data.user.persona.country).to.eql(managerData.user.persona.country);
  expect(resEdit.data.user.persona.jobTitle).to.eql(managerData.user.persona.jobTitle);
  expect(resEdit.data.user.persona.email).to.eql(managerData.user.persona.email);
});

Then('I verify update missing name for account manager', async () => {
  managerData.user.name = '';
  const missingNameEdit = await I.sendPostRequest(managerPage.url.manager, managerData, headers);
  expect(missingNameEdit.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(missingNameEdit.data.error.errorMessage).to.eql(managerPage.message.invalidRequest);
});

Then('I verify update incorrect email for account manager', async () => {
  managerData.user.persona.email = emailIncorrect;
  managerData.user.name = res.data.user.name;
  const emailIncorrectEmailEdit = await I.sendPostRequest(managerPage.url.manager, managerData, headers);
  expect(emailIncorrectEmailEdit.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(emailIncorrectEmailEdit.data.error.errorMessage).to.eql(managerPage.message.saveUserFail);
});

