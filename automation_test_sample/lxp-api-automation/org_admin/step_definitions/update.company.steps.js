const expect = require('chai').expect;
const faker = require('faker');
const { I } = inject();
const companyPage = require('../pages/company_page');
const codeResults = require('../../code_results');
let headers, companyId, editCompany;

const {
  adminAccount
} = require('../../org_admin/data/admin_company_data');

const {
  buildEditCompany
} = require('../data/edit_company_data');


Then('I verify update company valid value', async () => {
  companyId = process.env.ID_COMPANY;
  headers = {
    authorization: `Bearer ${process.env.tokenADMIN_COMPANY}`
  }
  I.say('Edit vaid value for company');
  editCompany = buildEditCompany(adminAccount.company, companyId);
  const restEditResults = await I.sendPostRequest(companyPage.url.update, editCompany, headers);
  expect(restEditResults.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(restEditResults.data.company.location).to.eql(editCompany.company.location);
  expect(restEditResults.data.company.fullName).to.eql(editCompany.company.fullName);
  expect(restEditResults.data.company.sso).to.eql(editCompany.company.sso);
});

Then('I back to default SSO is no', async () => {
  editCompany.company.sso = false;
  const revertCompany = await I.sendPostRequest(companyPage.url.update, editCompany, headers);
  expect(revertCompany.data.company.sso).to.eql(editCompany.company.sso);
});


Then('I verify update emty username for company', async () => {
  editCompany.company.name = "";
  const missingName = await I.sendPostRequest(companyPage.url.update, editCompany, headers);
  expect(missingName.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(missingName.data.error.errorCode).to.eql(codeResults.code.REQUEST_VALIDATION_FAILED);
  expect(missingName.data.error.errorDetails[0]).to.eql(companyPage.message.invalidName);
});
