const { I } = inject();
const expect = require('chai').expect;
const loginPage = require('../pages/login_page');
const companyPage = require('../pages/company_page');
const codeResults = require('../../code_results');
const {
  adminAccount
} = require('../data/admin_company_data');

Given('I have verify a invalid call for company', async () => {
  const res = await I.sendGetRequest(loginPage.url.company + adminAccount.company + adminAccount.company + loginPage.url.public);
  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res.data.error.errorMessage).to.eql(loginPage.message.companyFail);
});

Given('I have verify a successful call for company', async () => {
  const res = await I.sendGetRequest(loginPage.url.company + adminAccount.company + loginPage.url.public);
  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
});

Then('I login admin company with invalid value', async () => {
  I.say('Check invalid username');
  const resInvalidUsername = await I.sendPostRequest(loginPage.url.login + adminAccount.company + loginPage.url.token, loginPage.url.username + adminAccount.username + adminAccount.password + loginPage.url.password + adminAccount.password + loginPage.url.client, '');
  process.env.tokenADMIN_COMPANY = resInvalidUsername.data.access_token;
  expect(resInvalidUsername.data.error_description).to.eql(loginPage.message.error_description);

  I.say('Check invalid password');
  const resInvalidPassword = await I.sendPostRequest(loginPage.url.login + adminAccount.company + loginPage.url.token, loginPage.url.username + adminAccount.password + adminAccount.password + loginPage.url.password + adminAccount.password + loginPage.url.client, '');
  expect(resInvalidPassword.data.error_description).to.eql(loginPage.message.error_description);

  I.say('Check login success');
  const res = await I.sendPostRequest(loginPage.url.login + adminAccount.company + loginPage.url.token, loginPage.url.username + adminAccount.username + loginPage.url.password + adminAccount.password + loginPage.url.client, '');

  process.env.tokenADMIN_COMPANY = res.data.access_token;
});

Then('I get infor of company detail', async () => {
  I.say('Get infor of company detail');
  const headers = {
    authorization: `Bearer ${process.env.tokenADMIN_COMPANY}`
  }
  const companyDetail = await I.sendGetRequest(companyPage.url.profile, headers);
  process.env.ID_COMPANY = companyDetail.data.user.companyId;
});
