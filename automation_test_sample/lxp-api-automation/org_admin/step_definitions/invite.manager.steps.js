const expect = require('chai').expect;
const faker = require('faker');
const { I } = inject();
const managerPage = require('../pages/manager_page');
const codeResults = require('../../code_results');
const {
  buildManagerData,
  buildIncorrectUsernameManagerData
} = require('../data/invite_manager_data');

let companyId, manager, headers;

Given('I create new account manager', async () => {
  headers = {
    authorization: `Bearer ${process.env.tokenADMIN_COMPANY}`
  }
  companyId = process.env.ID_COMPANY;
  manager = buildManagerData(companyId)

  I.say('Create new AM with valid value');
  const res = await I.sendPostRequest(managerPage.url.manager, manager, headers);
  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res.data.user.companyId).to.eql(companyId);
  expect(res.data.user.name).to.eql(manager.user.name);
  expect(res.data.user.persona.email).to.eql(manager.user.persona.email);
});

Then('I verify create new AM have username already existed', async () => {
  const resNameAlreadyExisted = await I.sendPostRequest(managerPage.url.manager, manager, headers);
  expect(resNameAlreadyExisted.data.error.errorMessage).to.eql(managerPage.message.name);
});

Then('I verify create new AM have email already existed', async () => {
  I.say('Create new AM when email already existed');
  manager.user.name = 111 + 'client' + faker.random.locale().toLowerCase() + faker.random.number();
  const resEmailAlreadyExisted = await I.sendPostRequest(managerPage.url.manager, manager, headers);
  expect(resEmailAlreadyExisted.data.error.errorMessage).to.eql(managerPage.message.email);
});

Then('I verify create new AM missing username', async () => {
  manager.user.name = '';
  const resMissingUsername = await I.sendPostRequest(managerPage.url.manager, manager, headers);
  expect(resMissingUsername.data.error.errorMessage).to.eql(managerPage.message.invalidRequest);
});

Then('I verify create new AM missing password', async () => {
  manager.user.name = 111 + 'client' + faker.random.locale().toLowerCase() + faker.random.number();
  manager.password = '';
  const resMissPassword = await I.sendPostRequest(managerPage.url.manager, manager, headers);
  expect(resMissPassword.data.error.errorDetails[0]).to.eql(managerPage.message.invalidPassword);
});

Then('I verify create new AM incorrect username', async () => {
  let incorrectUsernameManagerData = buildIncorrectUsernameManagerData(companyId);
  const resIncorrectUsernameManagerData = await I.sendPostRequest(managerPage.url.manager, incorrectUsernameManagerData, headers);
  expect(resIncorrectUsernameManagerData.data.error.errorDetails[0]).to.eql(managerPage.message.usernameIncorrect);
});

Then('I verify create new AM incorrect password', async () => {
  manager.password = 'nhu';
  const resIncorrectPassword = await I.sendPostRequest(managerPage.url.manager, manager, headers);
  expect(resIncorrectPassword.data.error.errorDetails[0]).to.eql(managerPage.message.invalidPassword);
});