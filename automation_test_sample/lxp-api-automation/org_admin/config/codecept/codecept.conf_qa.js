exports.config = {
  output: './tests/output',
  helpers: {
    REST: {
      endpoint: 'https://public.qa.leapxpert.app',
      onRequest: (request) => {
      }
    }
  },
  include: {
  },
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './org_admin/features/*.feature',
    steps: [
      './org_admin/step_definitions/login.steps.js',
      './org_admin/step_definitions/invite.manager.steps.js',
      './org_admin/step_definitions/update.company.steps.js',
      './org_admin/step_definitions/update.manager.steps.js'

    ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    }
  },
  name: 'lxp-api-automation'
}