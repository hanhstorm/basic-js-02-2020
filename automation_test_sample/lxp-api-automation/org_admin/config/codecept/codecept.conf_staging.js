exports.config = {
  tests: 'tests/am/*_test.js',
  output: './tests/output',
  helpers: {
    REST: {
      endpoint: 'https://api-staging-2.leap.expert',
      onRequest: (request) => {
      }
    }
  },
  bootstrap: null,
  mocha: {},
  name: 'lxp-api-server'
}