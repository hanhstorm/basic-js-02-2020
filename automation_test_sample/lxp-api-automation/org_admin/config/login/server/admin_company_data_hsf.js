const adminAccount = {
    company: process.env.HSF_ADMIN_COMPANY,
    username: process.env.HSF_ADMIN_COMPANY_USERNAME,
    password: process.env.HSF_ADMIN_COMPANY_PASSWORD
}

module.exports = {
    adminAccount
};

