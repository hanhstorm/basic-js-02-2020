const adminAccount = {
    company: process.env.STAGING_ADMIN_COMPANY,
    username: process.env.STAGING_ADMIN_COMPANY_USERNAME,
    password: process.env.STAGING_ADMIN_COMPANY_PASSWORD
}

module.exports = {
    adminAccount
};

