const url = {
  manager: '/v1/users',
  public: '/public',
  login: 'https://web.qa.leapxpert.app/auth/realms/',
  authentication: '/v,authentication/admin/login',
  token: '/protocol/openid-connect/token',
  username: 'username=',
  password: '&password=',
  client: '&client_id=admin-app&grant_type=password',
}
const message = {
  name: 'name',
  email: 'email',
  invalidRequest: 'Failed Validating Request',
  saveUserFail: 'Save User Failed',
  invalidPassword: 'Password must be between 6 - 16 characters and contain alphabet, numeric and special characters.',
  usernameIncorrect: 'User login name is not matches [a-zA-Z0-9\\._]{3,32}'
}

module.exports = {
  url,
  message
}