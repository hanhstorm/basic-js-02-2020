const url = {
  company: '/v1/companies/name/',
  public: '/public',
  login: 'https://web.qa.leapxpert.app/auth/realms/',
  authentication: '/v,authentication/admin/login',
  token: '/protocol/openid-connect/token',
  username: 'username=',
  password: '&password=',
  client: '&client_id=admin-app&grant_type=password',
}
const message = {
  companyFail: 'Get Company Public Info Failed',
  error: 'invalid_grant',
  error_description: 'Invalid user credentials'
}

module.exports = {
  url,
  message
}