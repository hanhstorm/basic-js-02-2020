const url = {
  profile: '/v1/users/my-profile',
  update: '/v1/companies'
}
const message = {
  companyFail: 'Get Company Public Info Failed',
  error: 'invalid_grant',
  error_description: 'Invalid user credentials',
  invalidName: 'Company login name is not matches [a-zA-Z0-9\\\\._]{3,32}'
}

module.exports = {
  url,
  message
}