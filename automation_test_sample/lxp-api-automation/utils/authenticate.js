
const forge = require('node-forge');
const atob = require('atob');
const md5 = require('blueimp-md5');
const { padStart } = require('lodash');
const uuid = require('uuid/v4');
const { I } = inject();
const isNotRequireEncrypt = false;

const uri = '/v1/authentication/login';
const realm = 'LeapXpert';
const deviceUniqueIdentifier = '65fdfdeb-952a-49e8-ad92-25e41eb8f80c';
const algorithm = 'md5';
const method = 'POST';
const VAULT_PREFIX = 'vault:v1';


const keypair = forge.rsa.generateKeyPair({ bits: 2048, workers: -1 })
const privateKeyPem = forge.pki.privateKeyToPem(keypair.privateKey)
const publicKeyPem = forge.pki.publicKeyToPem(keypair.publicKey)

const buildDigestHeader = (params) => {
    let result = 'DigestLeapXpert '
    const keys = Object.keys(params)
    keys.map((key, index) => {
        if (params[key]) {
            if (index < keys.length - 1) {
                result += `${key}="${params[key]}",`
            } else {
                result += `${key}="${params[key]}"`
            }
        }
        return null
    })
    return result;
}

const parseAuthenticateHeader = (header) => {
    const result = {}
    if (!header) return result
    const headerString = header.replace('DigestLeapXpert ', '')
    headerString.split(',').map(param => {
        const key = param.split('=')[0]
        const value = param.split('=')[1].replace(/"/g, '')
        result[key] = value
        return null
    })
    return result
}


const getAuthorizationSt1 = (params) => {
    return buildDigestHeader({
        ...params,
        realm,
        deviceUniqueIdentifier
    });
}

const getAuthorizationSt2 = ({ username, company, password, nonce, realm, qop }) => {
    const nc = '00000001';
    let nextNc = parseInt(nc, 10) + 1
    if (nextNc > 999) {
        nextNc = 1
    }
    const nextNcString = padStart(nextNc.toString(), 8, '0');
    const cnonce = uuid().slice(0, 8)

    const HA1 = md5(
        `${username}:${realm}:${password}`
    )
    const HA2 = md5(`${method}:${uri}:${md5(JSON.stringify({}))}`)
    const response = md5(
        `${HA1}:${nonce}:${nextNcString}:${cnonce}:${qop}:${HA2}`
    )

    return buildDigestHeader({
        username,
        company,
        realm,
        nonce,
        qop,
        deviceUniqueIdentifier,
        nc: nextNcString,
        cnonce,
        uri,
        algorithm,
        response
    });
}

const decryptAES = (textEncrypted) => {
    const symmetricKeyText = forge.util.decode64(process.env.symmetricKey);
    const symetricKey = keypair.privateKey.decrypt(symmetricKeyText)

    const resRemovedPrefix = textEncrypted.replace(`${VAULT_PREFIX}:`, '')
    const decodedData = atob(resRemovedPrefix)
    const iv = decodedData.slice(0, 12)
    const text = decodedData.slice(12, decodedData.length - 16)
    const tag = decodedData.slice(decodedData.length - 16)

    const decipher = forge.cipher.createDecipher(
        'AES-GCM',
        atob(symetricKey)
    )
    decipher.start({
        iv: iv,
        tagLength: 128,
        tag: tag
    })
    decipher.update(forge.util.createBuffer(text))
    const pass = decipher.finish()
    if (pass) {
        return JSON.parse(forge.util.decodeUtf8(decipher.output.data))
    } else {
        throw Error('cannot decrypt data')
    }
}

const encryptAES = (body) => {
    body = JSON.stringify(body);
    const iv = forge.random.getBytesSync(12)
    const symmetricKeyText = forge.util.decode64(process.env.symmetricKey);
    const symetricKey = keypair.privateKey.decrypt(symmetricKeyText)

    const cipher = forge.cipher.createCipher('AES-GCM', atob(symetricKey))

    cipher.start({
        iv: iv
    })
    cipher.update(forge.util.createBuffer(forge.util.encodeUtf8(body)))
    cipher.finish()
    const encrypted = cipher.output
    const tag = cipher.mode.tag
    const buffer = forge.util.createBuffer()
    buffer.putBytes(iv)
    buffer.putBytes(encrypted.data)
    buffer.putBytes(tag.data)

    const encryptedWithTagAndIV = forge.util.encode64(buffer.data)
    return `${VAULT_PREFIX}:${encryptedWithTagAndIV}`
}


async function sendPostRequest(link, body, header) {

    var isTrueSet = (process.env.ENCRYPT_DATA.toLowerCase() === 'true');
    if (isTrueSet == isNotRequireEncrypt) {
        results = await I.sendPostRequest(link, body, header);
        return results;
    } else {
        body = encryptAES(body);
        results = await I.sendPostRequest(link, body, header);
        return results;
    }
}

async function sendGetRequest(link, header) {

    var isTrueSet = (process.env.ENCRYPT_DATA.toLowerCase() === 'true');
    if (isTrueSet == isNotRequireEncrypt) {
        results = await I.sendGetRequest(link, header);
        return results;
    } else {
        body = encryptAES(body);
        results = await I.sendGetRequest(link, header);
        return results;
    }
}

async function responseRequest(body) {
    var isTrueSet = (process.env.ENCRYPT_DATA.toLowerCase() === 'true');
    let results;
    if (isTrueSet == isNotRequireEncrypt) {
        results = body.data;
        return results;
    } else {
        results = decryptAES(body.data);
        return results;
    }
}
module.exports = {
    getAuthorizationSt1,
    getAuthorizationSt2,
    parseAuthenticateHeader,
    publicKeyPem,
    decryptAES,
    encryptAES,
    sendPostRequest,
    responseRequest,
    sendGetRequest
}