exports.config = {
  output: './tests/output',
  helpers: {
    REST: {
      endpoint: 'https://api-staging-2.leap.expert',
      onRequest: (request) => {
      }
    }
  },
  include: {
  },
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './am/features/*.feature',
    steps: [
      './am/step_definitions/steps.js',
      './am/step_definitions/client.steps.js',
      './am/step_definitions/update.client.steps.js',
      './am/step_definitions/room.steps.js',
      './am/step_definitions/update.manager.steps.js',

    ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    }
  },
  name: 'lxp-api-automation'
}