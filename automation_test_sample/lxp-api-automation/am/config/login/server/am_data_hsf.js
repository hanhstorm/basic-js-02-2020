const amAccount = {
    company: process.env.HSF_AUTOMATION_COMPANY,
    username: process.env.HSF_AUTOMATION_AM_USERNAME,
    password: process.env.HSF_AUTOMATION_AM_PASSWORD
}

const config = {
    ENCRYPT_DATA: false
}
module.exports = {
    amAccount,
    config
};
