const url = {
    rooms: '/v1/integration/rooms'
}

const message = {
    fail: 'Create integrations room failed'
}

module.exports = {
    url,
    message
}