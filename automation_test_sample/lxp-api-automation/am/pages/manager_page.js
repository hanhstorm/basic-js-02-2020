const url = {
    manager_update: '/v1/user_management/users',
    update_password: '/v1/authentication/update_password'
}

const message = {
    saveFail: 'Save User Failed'
}

module.exports = {
    url,
    message
}
