const expect = require('chai').expect;
const { I } = inject();
const managerPage = require('../pages/manager_page');
const codeResults = require('../../code_results');
const { sendPostRequest, responseRequest } = require('../../utils/authenticate');

const {
  buildManagerData,
  incorrectEmailManagerData
} = require('../data/manager_data');
const amAccout = require('../data/am_data');
let username, headers, userId, companyId;

Then('I update account manager info', async () => {
  headers = {
    authorization: `Bearer ${process.env.tokenAM}`
  }
  userId = process.env.userId;
  companyId = process.env.companyId;
  const manager = buildManagerData(companyId, userId, process.env.username);

  I.say('Update with correct data');
  const resResponse = await sendPostRequest(managerPage.url.manager_update, manager, headers);

  expect(resResponse.status).to.eql(codeResults.code.SERVER_SUCCESS);

  const res = await responseRequest(resResponse);
  expect(res.code).to.eql(codeResults.code.SUCCESS);

  expect(res.record.companyId).to.eql(manager.user.companyId);
  expect(res.record.name).to.eql(manager.user.name);
  expect(res.record.persona.firstName).to.eql(manager.user.persona.firstName);
  expect(res.record.persona.email).to.eql(manager.user.persona.email);
});

Then('I update incorrect email', async () => {
  incorrectEmailManager = incorrectEmailManagerData(companyId, userId, process.env.username);

  const resIncorrectEmailManagerResponse = await sendPostRequest(managerPage.url.manager_update, incorrectEmailManager, headers);
  expect(resIncorrectEmailManagerResponse.status).to.eql(codeResults.code.SERVER_SUCCESS);

  const resIncorrectEmailManager = await responseRequest(resIncorrectEmailManagerResponse);
  console.log(resIncorrectEmailManager);
  expect(resIncorrectEmailManager.code).to.eql(codeResults.code.REQUEST_SAVE_FAIL);
  expect(resIncorrectEmailManager.message).to.eql(managerPage.message.saveFail);
});

Then('I update incorrect userID', async () => {
  const incorrectUserIdManager = buildManagerData(companyId, userId + userId, process.env.AUTOMATION_AM_USERNAME);
  const resIncorrectUserIdManagerResponse = await sendPostRequest(managerPage.url.manager_update, incorrectUserIdManager, headers);
  expect(resIncorrectUserIdManagerResponse.status).to.eql(codeResults.code.SERVER_SUCCESS);

  const resIncorrectUserIdManager = await responseRequest(resIncorrectUserIdManagerResponse);
  expect(resIncorrectUserIdManager.code).to.eql(codeResults.code.REQUEST_VALIDATION_FAILED);
  expect(resIncorrectUserIdManager.message).to.eql(codeResults.message.INVALID_REQUEST);
});

Then('I update incorrect companyID', async () => {
  const incorrecCompanyIdManager = buildManagerData(companyId + userId, userId, process.env.AUTOMATION_AM_USERNAME);
  const resIncorrectCompanyIdManagerResponse = await sendPostRequest(managerPage.url.manager_update, incorrecCompanyIdManager, headers);
  expect(resIncorrectCompanyIdManagerResponse.status).to.eql(codeResults.code.SERVER_SUCCESS);

  const resIncorrectCompanyIdManager = await responseRequest(resIncorrectCompanyIdManagerResponse);
  expect(resIncorrectCompanyIdManager.code).to.eql(codeResults.code.REQUEST_VALIDATION_FAILED);
  expect(resIncorrectCompanyIdManager.message).to.eql(codeResults.message.INVALID_REQUEST);
});