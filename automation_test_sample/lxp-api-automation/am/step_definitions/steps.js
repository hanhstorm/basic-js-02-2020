const { I } = inject();
const expect = require('chai').expect;
const loginPage = require('../pages/login_page');
const amAccout = require('../data/am_data');

const {
  getAuthorizationSt1,
  getAuthorizationSt2,
  parseAuthenticateHeader,
  publicKeyPem,
} = require('../../utils/authenticate');

let wwwAuthenticate;
let authenResData;
let accessToken;

Given('I have input company', async () => {
  const res = await I.sendGetRequest(loginPage.url.company + amAccout.amAccount.company);
  expect(res.status).to.eql(200);
  expect(res.data.record.name).to.eql(amAccout.amAccount.company);
});

Then('I have input username and password', async () => {
  const headers = {
    authorization: getAuthorizationSt1(amAccout.amAccount)
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);
  expect(res.status).to.eql(401);
  wwwAuthenticate = res.headers['www-authenticate'];
});

Then('I request Authenticate step 2', async () => {
  const AuthenticateData = parseAuthenticateHeader(wwwAuthenticate);
  const headers = {
    authorization: getAuthorizationSt2({ ...amAccout.amAccount, ...AuthenticateData })
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);

  expect(res.status).to.eql(200);
  expect(res.data.success).to.eql(true);

  authenResData = res.data;

  process.env.companyId = res.data.companyId;

  process.env.userId = res.data.userId;
});

When('I have input OTP', async () => {
  const payload = {
    companyId: authenResData.companyId,
    passcode: "111111",
    role: authenResData.role,
    ticket: authenResData.ticket,
    userId: authenResData.userId,
  }

  const res = await I.sendPostRequest(loginPage.url.verify, payload, {});
  expect(res.status).to.eql(200);
  accessToken = res.data.accessToken;
});

Then('I have  save devices', async () => {
  const headers = {
    authorization: `Bearer ${accessToken}`
  }

  const payload = {
    device: {
      applications: [],
      formFactor: "DESKTOP",
      hardwareIdentifier: "5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
      osIdentifier: "Win32",
      platform: "WEB",
      publicEncryptionKey: publicKeyPem,
      resolution: "1536x864",
      uniqueIdentifier: "65fdfdeb-952a-49e8-ad92-25e41eb8f80c",
    }
  }

  const res = await I.sendPostRequest(loginPage.url.device, payload, headers);
  expect(res.status).to.eql(200);
  process.env.tokenAM = res.data.record.bondAccessToken;
  process.env.username = amAccout.amAccount.username;
  process.env.symmetricKey = res.data.record.symmetricKey;
  process.env.ENCRYPT_DATA = amAccout.config.ENCRYPT_DATA;
});