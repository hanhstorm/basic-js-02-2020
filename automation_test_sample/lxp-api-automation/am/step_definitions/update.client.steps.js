const expect = require('chai').expect;
const { I } = inject();
const clientPage = require('../pages/client_page');
const codeResults = require('../../code_results');
const { sendPostRequest, responseRequest } = require('../../utils/authenticate');
let username, updateShare, headers, id, userId, companyId;

const {
  buildSingleClient
} = require('../data/invite_client_data');

const {
  updateSingleClient,
  invalidEmailClient,
  updateShareClient
} = require('../data/update_client_data');

Given('I create single client', async () => {
  headers = {
    authorization: `Bearer ${process.env.tokenAM}`
  }
  userId = [process.env.userId];
  companyId = process.env.companyId;
  const payload = buildSingleClient(companyId, userId);
  const resResponse = await sendPostRequest(clientPage.url.client, payload, headers);

  I.setRequestTimeout(20000);
  expect(resResponse.status).to.eql(codeResults.code.SERVER_SUCCESS);
  const res = await responseRequest(resResponse);
  expect(res.code).to.eql(codeResults.code.SUCCESS);

  username = res.record.name;
  email = res.record.persona.email;
  id = res.record.id;
});

Then('I update valid profile for client', async () => {
  updateClient = updateSingleClient(companyId, userId, id, username);
  const resReponse = await sendPostRequest(clientPage.url.client, updateClient, headers);

  expect(resReponse.status).to.eql(codeResults.code.SERVER_SUCCESS);

  const res = await responseRequest(resReponse);

  expect(res.code).to.eql(codeResults.code.SUCCESS);
  expect(res.record.persona.lastName).to.equal(updateClient.client.persona.lastName);
});

Then('I update share client to true', async () => {
  updateShare = updateShareClient(companyId, userId, id, username);
  const resUpdateShareResponse = await sendPostRequest(clientPage.url.client, updateShare, headers);
  expect(resUpdateShareResponse.status).to.eql(codeResults.code.SERVER_SUCCESS);

  const resUpdateShare = await responseRequest(resUpdateShareResponse);

  expect(resUpdateShare.code).to.eql(codeResults.code.SUCCESS);
  expect(resUpdateShare.record.shared).to.equal(true);
});

Then('I update incorrect id of client', async () => {
  updateIdClient = updateSingleClient(companyId, userId, id + id, username);
  I.setRequestTimeout(20000);
  const resUpdateIdClientResponse = await sendPostRequest(clientPage.url.client, updateIdClient, headers);

  const resUpdateIdClient = await responseRequest(resUpdateIdClientResponse);
  expect(resUpdateIdClient.code).to.equal(codeResults.code.REQUEST_INVALID_FAILED);
});

Then('I update incorrect id of client', async () => {
  updateIdManager = updateSingleClient(companyId, userId + userId, id, username);
  const resUpdateIdManagerResponse = await sendPostRequest(clientPage.url.client, updateIdManager, headers);
  expect(resUpdateIdManagerResponse.data).to.equal(codeResults.message.ERROR_SERVER);
});

Then('I update incorrect Email', async () => {
  updateInvalidEmail = invalidEmailClient(companyId, userId, id, username);
  const resUpdateInvalidEmailResponse = await sendPostRequest(clientPage.url.client, updateInvalidEmail, headers);
  const resUpdateInvalidEmail = await responseRequest(resUpdateInvalidEmailResponse);
  expect(resUpdateInvalidEmail.code).to.equal(codeResults.code.REQUEST_INVALID_FAILED);
});
