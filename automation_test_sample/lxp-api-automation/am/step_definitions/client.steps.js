const expect = require('chai').expect;
const { I } = inject();
const clientPage = require('../pages/client_page');
const codeResults = require('../../code_results');
const { sendPostRequest, responseRequest } = require('../../utils/authenticate');
const {
  buildMultiClients,
  buildInvalidEmailClient,
  buildMissingNameClient,
  buildMissingMobilePhoneClient,
  buildInvalidPasswordClient
} = require('../data/invite_client_data');

let headers, userId, companyId;

Then('I create multi clients', async () => {
  headers = {
    authorization: `Bearer ${process.env.tokenAM}`
  }
  userId = [process.env.userId];
  companyId = process.env.companyId;

  const clients = buildMultiClients(companyId, userId);
  const res = await sendPostRequest(clientPage.url.client, clients[0], headers);

  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);

  const resResponse = await responseRequest(res);

  // check email and name results is the same post 

  expect(clients[0].client.name).to.equal(resResponse.record.name);
  expect(clients[0].client.persona.email).to.equal(resResponse.record.persona.email);
});

Then('I create single client', async () => {
  const clients = buildMultiClients(companyId, userId);
  const res1Response = await sendPostRequest(clientPage.url.client, clients[1], headers);

  const res1 = await responseRequest(res1Response);

  expect(res1Response.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res1.code).to.eql(codeResults.code.SUCCESS);

  // check email and name results is the same post 
  expect(clients[1].client.name).to.equal(res1.record.name);
  expect(clients[1].client.persona.email).to.equal(res1.record.persona.email);
});

Then('I create invalid email', async () => {
  const invalidEmailClient = buildInvalidEmailClient(companyId, userId);

  const resInvalidEmailResponse = await sendPostRequest(clientPage.url.client, invalidEmailClient, headers);

  const resInvalidEmail = await responseRequest(resInvalidEmailResponse);
  expect(resInvalidEmail.code).to.equal(codeResults.code.REQUEST_INVALID_FAILED);
});

Then('I create missing name', async () => {
  const missingNameClient = buildMissingNameClient(companyId, userId);

  const resMissingName = await sendPostRequest(clientPage.url.client, missingNameClient, headers);

  const resMissingNameClient = await responseRequest(resMissingName);
  expect(resMissingNameClient.code).to.equal(codeResults.code.REQUEST_VALIDATION_FAILED);
});

Then('I create invalid password', async () => {
  const invalidPasswordClient = buildInvalidPasswordClient(companyId, userId);
  const resInvalidPassword = await sendPostRequest(clientPage.url.client, invalidPasswordClient, headers);

  const resInvalidPasswordClient = await responseRequest(resInvalidPassword);
  expect(resInvalidPasswordClient.code).to.equal(codeResults.code.REQUEST_VALIDATION_FAILED);
});

Then('I create missing mobile phone', async () => {
  const missingMissingMobilePhoneClient = buildMissingMobilePhoneClient(companyId, userId);
  const resMissingMobilePhone = await sendPostRequest(clientPage.url.client, missingMissingMobilePhoneClient, headers);

  const resMissingMobilePhoneClient = await responseRequest(resMissingMobilePhone);
  expect(resMissingMobilePhoneClient.code).to.equal(codeResults.code.REQUEST_VALIDATION_FAILED);
});
