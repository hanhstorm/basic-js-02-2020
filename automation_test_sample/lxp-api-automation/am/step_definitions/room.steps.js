const expect = require('chai').expect;
const { I } = inject();
const roomPage = require('../pages/room_page');
const clientPage = require('../pages/client_page');
const codeResults = require('../../code_results');
const { sendPostRequest, responseRequest } = require('../../utils/authenticate');
let clientsId = [];
let otherUser = [];
let headers, userId, companyId;
const faker = require('faker');
let room, res;
const {
  buildRooms
} = require('../data/room_data');
const {
  buildMultiClients
} = require('../data/invite_client_data');

Given('I created 2 clients', async () => {
  roomName = faker.random.locale().toLowerCase();

  headers = {
    authorization: `Bearer ${process.env.tokenAM}`
  }
  userId = [process.env.userId];
  companyId = process.env.companyId;

  const clients = buildMultiClients(companyId, userId)

  const resResponse = await sendPostRequest(clientPage.url.client, clients[0], headers);

  expect(resResponse.status).to.eql(codeResults.code.SERVER_SUCCESS);


  res = await responseRequest(resResponse);

  expect(res.code).to.eql(codeResults.code.SUCCESS);

  // check email and name results is the same post 
  expect(clients[0].client.name).to.equal(res.record.name);
  expect(clients[0].client.persona.email).to.equal(res.record.persona.email);

  I.setRequestTimeout(20000);
  const res1Response = await sendPostRequest(clientPage.url.client, clients[1], headers);

  const res1 = await responseRequest(res1Response);
  expect(res1Response.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res1.code).to.eql(codeResults.code.SUCCESS);

  // check email and name results is the same post 
  expect(clients[1].client.name).to.equal(res1.record.name);
  expect(clients[1].client.persona.email).to.equal(res1.record.persona.email);

  clientsId.push(res.record.id);
  clientsId.push(res1.record.id);
});

Then('I add new 2 clients on the room', async () => {

  room = buildRooms(clientsId, otherUser, 'checking');
  I.say('Create new room for 2 clients above');
  const roomResultsResponse = await sendPostRequest(roomPage.url.rooms, room, headers);
  expect(roomResultsResponse.status).to.eql(codeResults.code.SERVER_SUCCESS);

  const roomResults = await responseRequest(roomResultsResponse);

  expect(roomResults.code).to.eql(codeResults.code.SUCCESS);
});

Then('I create new room includeMe is false', async () => {

  room = buildRooms(clientsId, otherUser, 'checking');
  I.say('Create new room for 2 clients above');
  const roomResultsResponse = await sendPostRequest(roomPage.url.rooms, room, headers);
  expect(roomResultsResponse.status).to.eql(codeResults.code.SERVER_SUCCESS);

  const roomResults = await responseRequest(roomResultsResponse);

  expect(roomResults.code).to.eql(codeResults.code.SUCCESS);
});

Then('I create new room don not have any client', async () => {
  let clientsMissingId = [];
  clientsMissingId.push('');

  let missingClientId = buildRooms(clientsMissingId, otherUser, 'checking');
  const missingClientIdResultsResponse = await sendPostRequest(roomPage.url.rooms, missingClientId, headers);
  expect(missingClientIdResultsResponse.status).to.equal(codeResults.code.SERVER_SUCCESS);

  const missingClientIdResults = await responseRequest(missingClientIdResultsResponse);
  expect(missingClientIdResults.code).to.equal(codeResults.code.CREATE_INTEGRATIONS_ROOM_FAILED);
  expect(missingClientIdResults.message).to.equal(roomPage.message.fail);
});

Then('I create room with incorrect client id', async () => {

  let incorrectClientId = [];
  incorrectClientId.push(res.record.id + res.record.id);
  let incorrectClientIdRoom = buildRooms(incorrectClientId, otherUser, 'checking');
  const incorrectClientIdResultsResponse = await sendPostRequest(roomPage.url.rooms, incorrectClientIdRoom, headers);
  expect(incorrectClientIdResultsResponse.status).to.eql(codeResults.code.SERVER_SUCCESS);

  const incorrectClientIdResults = await responseRequest(incorrectClientIdResultsResponse);
  expect(incorrectClientIdResults.code).to.eql(codeResults.code.CREATE_INTEGRATIONS_ROOM_FAILED);
  expect(incorrectClientIdResults.message).to.eql(roomPage.message.fail);
});