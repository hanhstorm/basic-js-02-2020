Feature: Business rules for update client
  In order to achieve my goals
  As a account manger 
  I want to be update my info after login successfully
  Scenario: Update with happy cases and bad cases
    Then I update account manager info
    Then I update incorrect email
    Then I update incorrect userID
    Then I update incorrect companyID
