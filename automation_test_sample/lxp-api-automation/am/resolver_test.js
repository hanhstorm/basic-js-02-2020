const expect = require('chai').expect;
const { I } = inject();
const resolverPage = require('../page/resolver_page');
const codeResults = require('../code_results');
Scenario('Create New Resolver', async () => {

    const headers = {
        authorization: `Bearer ${process.env.tokenAM}`
    }

    const userId = [process.env.userId];

    const res = await I.sendPostRequest(resolverPage.resolver.AM, '', headers);

    console.log(res.data);
    expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
    expect(res.data.code).to.eql(codeResults.code.SUCCESS);

    let body = {
        "shortCode": res.data.shortCode
    };

    let bodyInvalid = {
        "shortCode": res.data.shortCode + '1'
    };

    // I.say('Get environment via short code invalid value');
    // const resCheckShortcodeInvalid = await I.sendPostRequest(resolverPage.resolver.ENV, bodyInvalid, '');

    // expect(resCheckShortcodeInvalid.data.message).to.eql(resolverPage.message.invalid);
    // expect(resCheckShortcodeInvalid.status).to.eql(codeResults.code.SERVER_SUCCESS);
    // expect(resCheckShortcodeInvalid.data.code).to.eql(codeResults.code.RESOLVER_ERROR);

    I.say('Check Invitation Token');
    const resCheckInvitation = await I.sendGetRequest(resolverPage.resolver.CHECK_INVITATION, headers);
    const invitationTokenLength = resCheckInvitation.data.invitationTokens.length;
    const lastShortCode = resCheckInvitation.data.invitationTokens[invitationTokenLength - 1];
    expect(lastShortCode.status).to.eql(resolverPage.status.ACTIVE);

    I.say('Get environment via short code');
    const resCheckShortcode = await I.sendPostRequest(resolverPage.resolver.ENV, body, '');
    expect(resCheckShortcode.status).to.eql(codeResults.code.SERVER_SUCCESS);
    expect(resCheckShortcode.data.code).to.eql(codeResults.code.SUCCESS);
    expect(resCheckShortcode.data.record.attributes.API_ENDPOINT).to.eql(resolverPage.resolver.API_ENDPOINT_DEV);

    // I.say('Get environment via short code again');
    // const resInvitationShortcode = await I.sendPostRequest(resolverPage.resolver.ENV, body, '');
    // expect(resInvitationShortcode.data.message).to.eql(resolverPage.message.invalid);
    // const resInvitationInvitation = await I.sendGetRequest(resolverPage.resolver.CHECK_INVITATION, headers);

    // const invitationTokenLengthSuspended = resInvitationInvitation.data.invitationTokens.length;

    // const lastShortCodeSuspended = resInvitationInvitation.data.invitationTokens[invitationTokenLengthSuspended - 1];
    // expect(lastShortCodeSuspended.status).to.eql(resolverPage.status.SUSPENDED);
});

Scenario('Check Resolver with case sensitive and insensitive', async () => {

    const headers = {
        authorization: `Bearer ${process.env.tokenAM}`
    }

    const res = await I.sendPostRequest(resolverPage.resolver.AM, '', headers);

    expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
    expect(res.data.code).to.eql(codeResults.code.SUCCESS);

    let bodyUpper = {
        "shortCode": res.data.shortCode.toUpperCase(),
    };

    I.say('Get environment via short code with upper ');
    const resCheckShortcode = await I.sendPostRequest(resolverPage.resolver.ENV, bodyUpper, '');
    expect(resCheckShortcode.status).to.eql(codeResults.code.SERVER_SUCCESS);
    expect(resCheckShortcode.data.code).to.eql(codeResults.code.SUCCESS);
    expect(resCheckShortcode.data.record.attributes.API_ENDPOINT).to.eql(resolverPage.resolver.API_ENDPOINT_DEV);
    I.say('Get environment via short code again');
    const resReCheckShortcode = await I.sendPostRequest(resolverPage.resolver.ENV, bodyUpper, '');
    // expect(resReCheckShortcode.data.message).to.eql(resolverPage.message.invalid);
});