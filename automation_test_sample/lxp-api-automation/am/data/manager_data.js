const faker = require('faker');

const PERSONA = {
    "communicationIdentifier": "",
    "country": "VN",
    "countryName": 'Vietnam',
    "email": "qa@leap.expert",
    "firstName": "expert",
    "jobTitle": "Account manager",
    "language": "",
    "lastName": "leap",
    "linkedIn": "",
    "middleName": "",
    "mobilePhoneNumber": "84963282927",
    "officePhoneNumber": "84 96 328 29 27",
    "profileImageUrl": "",
    "timezone": ""
}

const USER = {
    "biography": "",
    "clientCount": 0,
    "companyId": "5d635219600dcf142bed93b5",
    "creationTimestamp": "2019-08-27T06:06:49.702Z",
    "departmentId": "",
    "id": "5d64c879bc0c116ff3c7bc19",
    "lastUpdateTimestamp": "2019-09-12T08:03:40.452Z",
    "name": "qa_checking",
    "persona": PERSONA,
    "riskRating": 0,
    "roles": [
        "EXPERT"
    ],
    "status": "ACTIVE"
}

const buildManagerData = (companyId, id, name) => {
    const persona = {
        ...PERSONA,
        country: 'VN',
        email: faker.internet.email().toLowerCase()
    };

    const user = {
        ...USER,
        companyId,
        id,
        name:name,
        persona: persona,
        "roles": ["EXPERT"],
    };

    const manager = {
        user: user,
    }

    return manager;
}


const incorrectEmailManagerData = (companyId, id, name) => {
    const persona = {
        ...PERSONA,
        country: 'VN',
        "email": "testing.leap"
        };

    const user = {
        ...USER,
        companyId,
        id,
        persona: persona,
        "roles": ["EXPERT"],
    };

    const manager = {
        user: user,
    }

    return manager;
}

const incorrectUsernameManagerData = (companyId, id, name) => {
    const persona = {
        ...PERSONA,
        country: 'VN',
        "email": "testing.leap",
        name: name
    };

    const user = {
        ...USER,
        companyId,
        id,
        persona: persona,
        "roles": ["EXPERT"],
    };

    const manager = {
        user: user,
    }

    return manager;
}

const buildPassword = (oldPassword, newPassword) => {
    const passwrord = {
        oldPassword: oldPassword,
        passwrord: newPassword
    }
    return passwrord;
}

module.exports = {
    buildManagerData,
    incorrectEmailManagerData,
    incorrectUsernameManagerData,
    buildPassword
};

