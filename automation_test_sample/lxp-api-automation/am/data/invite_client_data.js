const faker = require('faker');

const PERSONA = {
    "country": "DZ",
    "email": "",
    "mobilePhoneNumber": "2015550122",
    "officePhoneNumber": ""
}

const CLIENT = {
    "companyId": "",
    "name": "",
    "currency": "USD",
    "shared": false,
    "persona": PERSONA,
    "userId": [process.env.userId]
}

const _CLIENT = {
    "client": CLIENT,
    "password": "Nhung@123",
    "skipEmail": false
}

const buildSingleClient = (companyId, userId) => {
    const persona = {
        ...PERSONA,
        country: 'VN',
        "email": faker.internet.email().toLowerCase(),
    };
    const client = {
        ...CLIENT,
        companyId,
        userId,
        name: 'automation_client' + faker.random.locale().toLowerCase() + faker.random.number(),
        persona: persona
    };
    return {
        ..._CLIENT,
        client
    };
}

const buildMultiClients = (companyId, userId) => {
    const datas = [];
    //case 1
    let persona = {
        ...PERSONA,
        country: 'VN',
        "email": faker.internet.email().toLowerCase(),
    };
    let client = {
        ...CLIENT,
        companyId,
        userId,
        name: 'automation_client' + faker.random.locale().toLowerCase() + faker.random.number(),
        persona
    };
    let _client = {
        ..._CLIENT,
        client
    };
    datas.push(_client);

    //case 2:
    persona = {
        ...PERSONA,
        country: 'VN',
        "email": faker.internet.email().toLowerCase(),
    };
    client = {
        ...CLIENT,
        companyId,
        userId,
        name: 'automation_client' + faker.random.locale().toLowerCase() + faker.random.number(),
        persona
    };
    _client = {
        ..._CLIENT,
        client,
        password: "Nhung@123",
        skipEmail: false
    };
    datas.push(_client);

    return datas;
}

const buildInvalidEmailClient = (companyId, userId) => {
    const persona = {
        ...PERSONA,
        country: 'VN',
        "email": 'nhungnguyen',
    };
    const client = {
        ...CLIENT,
        companyId,
        userId,
        name: 'automation_client' + faker.random.locale().toLowerCase() + faker.random.number(),
        persona
    };
    return {
        ..._CLIENT,
        client,
        password: "Nhung@123",
        skipEmail: false
    };
}

const buildMissingNameClient = (companyId, userId) => {
    const persona = {
        ...PERSONA,
        country: 'VN',
        "email": faker.internet.email().toLowerCase()
    };
    const client = {
        ...CLIENT,
        companyId,
        userId,
        name: "",
        persona
    };
    return {
        ..._CLIENT,
        client,
        password: "Nhung@123",
        skipEmail: false
    }
}

const buildMissingMobilePhoneClient = (companyId, userId) => {
    const persona = {
        ...PERSONA,
        country: 'VN',
        "email": faker.internet.email().toLowerCase(),
        mobilePhoneNumber: ""
    };
    const client = {
        ...CLIENT,
        companyId,
        userId,
        name: 'automation_client' + faker.random.locale().toLowerCase() + faker.random.number(),
        persona
    };
    return {
        ..._CLIENT,
        client,
        password: "Nhung@123",
        skipEmail: false
    };
}

const buildInvalidPasswordClient = (companyId, userId) => {
    const persona = {
        ...PERSONA,
        country: 'VN',
        "email": faker.internet.email().toLowerCase(),
    };
    const client = {
        ...CLIENT,
        companyId,
        userId,
        name: 'automation_client' + faker.random.locale().toLowerCase() + faker.random.number(),
        persona
    };
    return {
        ..._CLIENT,
        client,
        password: "Nhung",
        skipEmail: false
    }
}


module.exports = {
    buildMultiClients,
    buildSingleClient,
    buildInvalidEmailClient,
    buildMissingNameClient,
    buildMissingMobilePhoneClient,
    buildInvalidPasswordClient
};

