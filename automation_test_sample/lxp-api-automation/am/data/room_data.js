const faker = require('faker');

const CLIENT = {
    "id": "",
    "channel": ""
}

const ROOM = {
    "type": "GROUP",
    "includeMe": true,
    "groupName": "automation_testing_room"
}

const buildClient = (id) => {
    const client = {
        ...CLIENT,
        "id": id
    };

    return {
        client
    }

}

const buildMuiltClientsArray = (cientsId) => {
    const clients = [];

    cientsId.forEach(id => {
        let client = {
            ...CLIENT,
            id: id
        }

        clients.push(client)
    });

    return clients;
}

const buildRooms = (clientIds, otherUsers, name) => {
    const clients = {
        "clients": buildMuiltClientsArray(clientIds),
        "otherUsers": buildMuiltClientsArray(otherUsers),
        "type": "GROUP",
        "includeMe": true,
        "groupName": name
    }

    return clients;
}

const roomIncludeMeFalse = (clientIds, otherUsers, name) => {
    const includeMeFalse = {
        "clients": buildMuiltClientsArray(clientIds),
        "otherUsers": buildMuiltClientsArray(otherUsers),
        "type": "GROUP",
        "includeMe": false,
        "groupName": name
    }

    return includeMeFalse;
}
module.exports = {
    buildRooms,
    buildClient,
    roomIncludeMeFalse
};

