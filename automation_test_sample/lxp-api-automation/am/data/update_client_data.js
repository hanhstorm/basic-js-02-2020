const faker = require('faker');

const PERSONA = {
    "communicationIdentifier": "",
    "country": "",
    "countryName": "",
    "email": "",
    "firstName": "updatefirstNameTesting",
    "jobTitle": "Checking",
    "language": "Checking_1",
    "lastName": "updateListNameTesting",
    "linkedIn": "",
    "middleName": "",
    "mobilePhoneNumber": "2015550122",
    "officePhoneNumber": "",
    "profileImageUrl": "",
    'timezone': ""
}

const CLIENT = {
    "biography": "",
    "companyId": "",
    "name": "",
    "currency": "USD",
    "id": "",
    "notes": "",
    "lastUpdateTimestamp": "",
    "creationTimestamp": "",
    "shared": false,
    "persona": PERSONA,
    "riskRating": "",
    "status": "ACTIVE",
    "userAtDomain": "",
    "riskVisible": false,
    "userId": [process.env.userId]
}

const updateSingleClient = (companyId, userId, id, name) => {
    const persona = {
        ...PERSONA,
        country: 'VN',
        "email": faker.internet.email(),
    };

    const client = {
        ...CLIENT,
        companyId,
        userId,
        id,
        name,
        persona: persona
    };

    return {
        client: client
    };
}

const invalidEmailClient = (companyId, userId, id, name) => {
    const persona = {
        ...PERSONA,
        country: 'VN',
        "email": "",
    };
    const client = {
        ...CLIENT,
        companyId,
        userId,
        id,
        name,
        persona: persona
    };
    return {
        client: client
    };
}


const updateShareClient = (companyId, userId, id, name) => {
    const persona = {
        ...PERSONA,
        country: 'VN',
        "email": "",
    };
    const client = {
        ...CLIENT,
        companyId,
        userId,
        id,
        name,
        persona,
        shared: true
    };
    return {
        client: client
    };
}


module.exports = {
    updateSingleClient,
    invalidEmailClient,
    updateShareClient
};

