
const forge = require('node-forge');
const expect = require('chai').expect;
const { I } = inject();
const loginPage = require('../super_admin/pages/login_page');
const codeResults = require('../code_results');
let accessToken;
const {
  getAuthorizationSt1,
  getAuthorizationSt2,
  parseAuthenticateHeader
} = require('../utils/authenticate');

const {
  superAccount,
  superInvalidUser,
  superInvalidPassword,
  superMissingUsername,
  superMissingPassword
} = require('../super_admin/data/super_admin_data');


let wwwAuthenticate;
Feature('Authentication');
Feature('Check invalid username');
Scenario('Authenticate step 1', async () => {
  const headers = {
    authorization: getAuthorizationSt1(superInvalidUser)
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);
  expect(res.status).to.eql(codeResults.code.UNAUTHORIZED);
  wwwAuthenticate = res.headers['www-authenticate'];
});

Scenario('Authenticate step 2', async () => {
  const AuthenticateData = parseAuthenticateHeader(wwwAuthenticate);
  const headers = {
    authorization: getAuthorizationSt2({ ...superInvalidUser, ...AuthenticateData })
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);

  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res.data.code).to.eql(codeResults.code.LOGIN_FAILED);
  expect(res.data.accessToken).to.eql(null);
  expect(res.data.companyId).to.eql(null);
  expect(res.data.matrixAccessToken).to.eql(null);
  expect(res.data.matrixId).to.eql(null);
  expect(res.data.message).to.eql(loginPage.message.error);
});

Feature('Check invalid password');
Scenario('Authenticate step 1', async () => {
  const headers = {
    authorization: getAuthorizationSt1(superInvalidPassword)
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);
  expect(res.status).to.eql(401);
  wwwAuthenticate = res.headers['www-authenticate'];
});

Scenario('Authenticate step 2', async () => {
  const AuthenticateData = parseAuthenticateHeader(wwwAuthenticate);
  const headers = {
    authorization: getAuthorizationSt2({ ...superInvalidPassword, ...AuthenticateData })
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);

  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res.data.code).to.eql(codeResults.code.LOGIN_FAILED);
  expect(res.data.accessToken).to.eql(null);
  expect(res.data.companyId).to.eql(null);
  expect(res.data.matrixAccessToken).to.eql(null);
  expect(res.data.matrixId).to.eql(null);
  expect(res.data.message).to.eql(loginPage.message.error);
});

Feature('Check missing username');
Scenario('Authenticate step 1', async () => {
  const headers = {
    authorization: getAuthorizationSt1(superMissingUsername)
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);
  expect(res.status).to.eql(401);
  wwwAuthenticate = res.headers['www-authenticate'];
});

Scenario('Authenticate step 2', async () => {
  const AuthenticateData = parseAuthenticateHeader(wwwAuthenticate);
  const headers = {
    authorization: getAuthorizationSt2({ ...superMissingUsername, ...AuthenticateData })
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);

  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res.data.code).to.eql(codeResults.code.LOGIN_FAILED);
  expect(res.data.accessToken).to.eql(null);
  expect(res.data.companyId).to.eql(null);
  expect(res.data.matrixAccessToken).to.eql(null);
  expect(res.data.matrixId).to.eql(null);
  expect(res.data.message).to.eql(loginPage.message.error);
});

Feature('Check missing password');
Scenario('Authenticate step 1', async () => {
  const headers = {
    authorization: getAuthorizationSt1(superMissingPassword)
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);
  expect(res.status).to.eql(401);
  wwwAuthenticate = res.headers['www-authenticate'];
});

Scenario('Authenticate step 2', async () => {
  const AuthenticateData = parseAuthenticateHeader(wwwAuthenticate);
  const headers = {
    authorization: getAuthorizationSt2({ ...superMissingPassword, ...AuthenticateData })
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);

  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res.data.code).to.eql(codeResults.code.LOGIN_FAILED);
  expect(res.data.accessToken).to.eql(null);
  expect(res.data.companyId).to.eql(null);
  expect(res.data.matrixAccessToken).to.eql(null);
  expect(res.data.matrixId).to.eql(null);
  expect(res.data.message).to.eql(loginPage.message.error);
});

Feature('Check login valid value');
Scenario('Authenticate step 1', async () => {
  const headers = {
    authorization: getAuthorizationSt1(superAccount)
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);
  expect(res.status).to.eql(codeResults.code.UNAUTHORIZED);
  wwwAuthenticate = res.headers['www-authenticate'];
});

Scenario('Authenticate step 2', async () => {
  const AuthenticateData = parseAuthenticateHeader(wwwAuthenticate);
  const headers = {
    authorization: getAuthorizationSt2({ ...superAccount, ...AuthenticateData })
  };

  const res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);

  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res.data.success).to.eql(true);

  authenResData = res.data;

  process.env.id = res.data.id;

  accessToken = res.data.accessToken;
});


Feature('Device Authentication');

Scenario('Verify Device', async () => {
  const headers = {
    authorization: `Bearer ${accessToken}`
  }

  const keypair = await forge.rsa.generateKeyPair({ bits: 2048, workers: -1 })
  const privateKeyPem = forge.pki.privateKeyToPem(keypair.privateKey)
  const publicKeyPem = forge.pki.publicKeyToPem(keypair.publicKey)

  const payload = {
    device: {
      applications: [],
      formFactor: "DESKTOP",
      hardwareIdentifier: "5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
      osIdentifier: "Win32",
      platform: "WEB",
      publicEncryptionKey: publicKeyPem,
      resolution: "1536x864",
      uniqueIdentifier: "65fdfdeb-952a-49e8-ad92-25e41eb8f80c",
    }
  }
    ;

  const res = await I.sendPostRequest(loginPage.url.device, payload, headers);
  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  process.env.tokenSUPERADMIN = res.data.record.bondAccessToken;
  console.log(process.env.tokenSUPERADMIN);
});