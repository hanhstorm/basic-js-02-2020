const url = {
  company: '/v1/sso/',
  authentication: '/v1/authentication/admin/login',
  verify: '/v1/authentication/login/mfa/verify',
  device: '/v1/device_management/devices',
  client: '/v1/user_management/clients'
}
const message = {
  error: 'Login Failed',
}

module.exports = {
  url,
  message
}