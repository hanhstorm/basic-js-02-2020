exports.config = {
  output: './tests/output',
  helpers: {
    REST: {
      endpoint: 'https://api.qa.leapxpert.app',
      onRequest: (request) => {
      }
    }
  },
  include: {
  },
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './super_admin/features/*.feature',
    steps: [
      './super_admin/step_definitions/login.steps.js',
      './super_admin/step_definitions/company.steps.js'

    ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    }
  },
  name: 'lxp-api-automation'
}