const faker = require('faker');

const params = {
    username: 'admin',
    password: 'admin@123'
}

const paramsInvalidUser = {
    username: 'admindd123',
    password: 'secret@LeapXpert2018'
}

const paramsInvalidPassword = {
    username: 'admin',
    password: 'secret'
}

const paramsMissingUsername = {
    username: '',
    password: 'secret'
}

const paramsMissingPassword = {
    username: 'admin',
    password: ''
}

module.exports = {
    params,
    paramsInvalidUser,
    paramsInvalidPassword,
    paramsMissingUsername,
    paramsMissingPassword
};

