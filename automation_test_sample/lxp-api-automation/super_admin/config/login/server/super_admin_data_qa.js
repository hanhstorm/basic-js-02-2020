const superAccount = {
    // username: process.env.SUPER_ADMIN_USERNAME,
    // password: process.env.SUPER_ADMIN_PASSWORD

    username: 'admin',
    password: 'secret@LeapXpert2018'
}

const superInvalidUser = {
    username: 'admindd123',
    password: 'secret@LeapXpert2018'
}

const superInvalidPassword = {
    username: 'admin',
    password: 'secret'
}

const superMissingUsername = {
    username: '',
    password: 'secret'
}

const superMissingPassword = {
    username: 'admin',
    password: ''
}

module.exports = {
    superAccount,
    superInvalidUser,
    superInvalidPassword,
    superMissingUsername,
    superMissingPassword
};

