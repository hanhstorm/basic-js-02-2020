const superAccount = {
    username: process.env.STAGING_SUPER_ADMIN_USERNAME,
    password: process.env.STAGING_SUPER_ADMIN_PASSWORD
}

const superInvalidUser = {
    username: 'admindd123',
    password: 'secret@LeapXpert2018'
}

const superInvalidPassword = {
    username: 'admin',
    password: 'secret'
}

const superMissingUsername = {
    username: '',
    password: 'secret'
}

const superMissingPassword = {
    username: 'admin',
    password: ''
}

module.exports = {
    superAccount,
    superInvalidUser,
    superInvalidPassword,
    superMissingUsername,
    superMissingPassword
};

