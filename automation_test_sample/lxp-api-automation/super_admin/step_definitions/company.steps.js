// const { I } = inject();
// const forge = require('node-forge');
// const expect = require('chai').expect;
// const companyPage = require('../pages/company_page');
// const codeResults = require('../../code_results');
// let headers, res;
// const {
//   buildCompany,
//   buildCompanyMissingName,
//   buildCompanyMissingFullNameLocation
// } = require('../data/company_data');


// Then('I have verify create new company missing name', async () => {
//   headers = {
//     authorization: `Bearer ${process.env.tokenSUPERADMIN}`
//   }

//   const companyMissingName = buildCompanyMissingName();

//   I.say('Create new company missing name');
//   console.log(companyMissingName);
//   res = await I.sendPostRequest(companyPage.url.company, companyMissingName, headers);
//   console.log(res.data);
//   expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
//   expect(res.data.code).to.eql(codeResults.code.REQUEST_VALIDATION_FAILaED);
//   expect(res.data.message).to.eql(codeResults.message.INVALID_REQUEST);
// });

// Then('I have verify create new company with valid value', async () => {
//   const company = buildCompany();
//   const resCompany = await I.sendPostRequest(companyPage.url.company, company, headers);
//   expect(resCompany.status).to.eql(codeResults.code.SERVER_SUCCESS);

//   expect(resCompany.data.code).to.eql(codeResults.code.SUCCESS);
//   expect(resCompany.data.message).to.eql(codeResults.message.SUCCESS);
//   expect(resCompany.data.record.name).to.eql(company.company.name);
//   expect(resCompany.data.record.fullName).to.eql(company.company.fullName);
//   expect(resCompany.data.record.location).to.eql(company.company.location);
// });

// Then('I have verify create new company with missing fullName and localtion', async () => {
//   const companyMissingFullNameLocation = buildCompanyMissingFullNameLocation();
//   const resCompanyMissingFullNameLocationy = await I.sendPostRequest(companyPage.url.company, companyMissingFullNameLocation, headers);
//   expect(resCompanyMissingFullNameLocationy.status).to.eql(codeResults.code.SERVER_SUCCESS);

//   expect(resCompanyMissingFullNameLocationy.data.code).to.eql(codeResults.code.SUCCESS);
//   expect(resCompanyMissingFullNameLocationy.data.message).to.eql(codeResults.message.SUCCESS);
//   expect(resCompanyMissingFullNameLocationy.data.record.name).to.eql(company.company.name);
// });

