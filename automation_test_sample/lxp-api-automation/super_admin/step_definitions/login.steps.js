const { I } = inject();
const forge = require('node-forge');
const expect = require('chai').expect;
const loginPage = require('../pages/login_page');
const codeResults = require('../../code_results');
let accessToken, headers, wwwAuthenticate, res, AuthenticateData;
const {
  getAuthorizationSt1,
  getAuthorizationSt2,
  parseAuthenticateHeader
} = require('../../utils/authenticate');

const {
  superAccount,
  superInvalidUser,
  superInvalidPassword,
  superMissingUsername,
  superMissingPassword
} = require('../data/super_admin_data');

Then('I have verify login missing username', async () => {
  headers = {
    authorization: getAuthorizationSt1(superInvalidUser)
  };

  res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);
  expect(res.status).to.eql(codeResults.code.UNAUTHORIZED);
  wwwAuthenticate = res.headers['www-authenticate'];
  const AuthenticateData = parseAuthenticateHeader(wwwAuthenticate);
  headers = {
    authorization: getAuthorizationSt2({ ...superMissingUsername, ...AuthenticateData })
  };

  res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);

  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res.data.code).to.eql(codeResults.code.LOGIN_FAILED);
  expect(res.data.accessToken).to.eql(null);
  expect(res.data.companyId).to.eql(null);
  expect(res.data.matrixAccessToken).to.eql(null);
  expect(res.data.matrixId).to.eql(null);
  expect(res.data.message).to.eql(loginPage.message.error);
});

Then('I have verify login missing password', async () => {
  headers = {
    authorization: getAuthorizationSt1(superMissingPassword)
  };

  res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);
  expect(res.status).to.eql(401);
  wwwAuthenticate = res.headers['www-authenticate'];

  AuthenticateData = parseAuthenticateHeader(wwwAuthenticate);
  headers = {
    authorization: getAuthorizationSt2({ ...superMissingPassword, ...AuthenticateData })
  };

  res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);
  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res.data.code).to.eql(codeResults.code.LOGIN_FAILED);
  expect(res.data.accessToken).to.eql(null);
  expect(res.data.companyId).to.eql(null);
  expect(res.data.matrixAccessToken).to.eql(null);
  expect(res.data.matrixId).to.eql(null);
  expect(res.data.message).to.eql(loginPage.message.error);
});

Then('I have verify login with valid value', async () => {
  headers = {
    authorization: getAuthorizationSt1(superAccount)
  };

  res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);
  expect(res.status).to.eql(codeResults.code.UNAUTHORIZED);
  wwwAuthenticate = res.headers['www-authenticate'];

  AuthenticateData = parseAuthenticateHeader(wwwAuthenticate);
  headers = {
    authorization: getAuthorizationSt2({ ...superAccount, ...AuthenticateData })
  };

  res = await I.sendPostRequest(loginPage.url.authentication, {}, headers);

  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  expect(res.data.success).to.eql(true);

  authenResData = res.data;

  process.env.id = res.data.id;

  accessToken = res.data.accessToken;
});

Then('I have verify device value', async () => {
  headers = {
    authorization: `Bearer ${accessToken}`
  }

  const keypair = await forge.rsa.generateKeyPair({ bits: 2048, workers: -1 })
  const privateKeyPem = forge.pki.privateKeyToPem(keypair.privateKey)
  const publicKeyPem = forge.pki.publicKeyToPem(keypair.publicKey)

  const payload = {
    device: {
      applications: [],
      formFactor: "DESKTOP",
      hardwareIdentifier: "5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
      osIdentifier: "Win32",
      platform: "WEB",
      publicEncryptionKey: publicKeyPem,
      resolution: "1536x864",
      uniqueIdentifier: "65fdfdeb-952a-49e8-ad92-25e41eb8f80c",
    }
  }
    ;

  const res = await I.sendPostRequest(loginPage.url.device, payload, headers);
  expect(res.status).to.eql(codeResults.code.SERVER_SUCCESS);
  process.env.tokenSUPERADMIN = res.data.record.bondAccessToken;
});
