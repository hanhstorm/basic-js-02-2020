const faker = require('faker');

const COMPANY = {
    "name": 'country' + faker.random.locale().toLowerCase() + faker.random.number(),
    "fullName": "CheckingVN",
    "location": 'Vietdnam',
    "status": "NEW",
    "shareClients": false

}

const buildCompany = () => {
    const company = {
        company: COMPANY
    };

    return company;
}

const buildCompanyMissingName = () => {
    const company = {
        ...COMPANY,
        name: ""
    };

    const companyValue = {
        company: company
    }
    return companyValue;
}

const buildCompanyMissingFullNameLocation = () => {
    const company = {
        ...COMPANY,
        fullName: "",
        location: ""
    };

    const companyValue = {
        company: company
    }
    return companyValue;
}

module.exports = {
    buildCompany,
    buildCompanyMissingName,
    buildCompanyMissingFullNameLocation
};

