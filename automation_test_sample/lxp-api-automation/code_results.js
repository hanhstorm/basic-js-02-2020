const code = {
    SERVER_SUCCESS: 200,

    UNAUTHORIZED: 401,

    REQUEST_INVALID_FAILED: 70112,

    REQUEST_SAVE_FAIL: 70117,

    REQUEST_VALIDATION_FAILED: 10001, //"Failed Validating Request");

    INTERNAL_SERVICE_ERROR: 10002, // "Internal Service Error");

    // Database related errors
    DATABASE_ENTITY_NOT_FOUND: 10101, // "Entity Not Found");

    DATABASE_OPERATION_FAILED: 10102, //"Database Operation Failed");

    SUCCESS: 0,// Error(0, "Success");

    RESOLVER_ERROR: 1, // Resolver with code invalid

    LOGIN_FAILED: 10000, //  Error(10000, "Login Failed");

    GET_INFOCARD_FAILED: 10001, // new Error(10001, "Get Infocard Failed");

    PDATE_INFOCARD_FAILED: 10002, //new Error(10002, "Update Infocard Failed");

    CREATE_INFOCARD_FAILED: 10003, //new Error(10003, "Create Infocard Failed");

    GET_TRANSACTION_FAILED: 10001, //new Error(10001, "Get Transaction Failed");

    UPDATE_TRANSACTION_FAILED: 10005,// new Error(10005, "Update Transaction Failed");

    CREATE_TRANSACTION_FAILED: 10006, // new Error(10006, "Create Transaction Failed");

    GET_AUDITLOGS_FAILED: 10008, // Error(10008, "Get AuditLogs Failed");

    CREATE_COMMENT_FAILED: 10009, // Error(10009, "Create Comment Failed");

    GET_POSITIONS_FAILED: 10010, // Error(10010, "Get Positions Failed");

    GET_TAGS_AS_YOU_TYPE_FAILED: 10011, //new Error(10011, "GetTagsAsYouType Failed");

    GET_SECURITIES_FAILED: 100012, // Error(100012, "Get Securities Failed");

    GET_SECURITY_PRICES_FAILED: 100013, // Error(10013, "Get Security Prices Failed");

    GET_SECURITY_INTRADAY_PRICES_FAILED: 100014, // Error(10014, "Get Security Intraday Prices Failed");

    GET_PRICES_CHANGE_FAILED: 10015, // Error(10015, "Get Prices Change Failed");

    GET_CURRENT_PRICE_FAILED: 10016, // Error(10016, "Get Current Price Failed");

    WATCH_PRODUCT_FAILED: 10017, // Error(10017, "Watch Product Failed");

    UNWATCH_PRODUCT_FAILED: 10018, //Error(10018, "Unwatch Product Failed");

    IS_WATCHED_PRODUCT_FAILED: 10018, //Error(10018, "Is Watched Product Failed");

    GET_WATCH_PRODUCTS_FAILED: 10019, // Error(10019, "Get Watch Products Failed");

    GET_AGGREGATED_POSITIONS_FAILED: 10020, // Error(10020, "Get Aggregated Positions Failed");

    GET_INDEX_PRICE_FAILED: 10021, // Error(10021, "Get Index Price Failed");

    RENAME_TAG_FAILED: 10022, // Error(10022, "RenameTag Failed");

    GET_USER_FAILED: 10023,  // Error(10023, "Get UserEntity Failed");

    SAVE_USER_FAILED: 10024, // Error(10024, "Save UserEntity Failed");

    DELETE_USER_FAILED: 10025, // Error(10025, "Delete UserEntity Failed");

    GET_RESELLER_FAILED: 10026, // Error(10026, "Get Reseller Failed");

    SAVE_RESELLER_FAILED: 10027, // Error(10027, "Save Reseller Failed");

    DELETE_RESELLER_FAILED: 10028, // Error(10028, "Delete Reseller Failed");

    GET_COMPANY_FAILED: 10029, // Error(10029, "Get Company Failed");

    SAVE_COMPANY_FAILED: 10030, //"Save Company Failed");

    DELETE_COMPANY_FAILED: 10031,// "Delete Company Failed");

    GET_DEPARTMENT_FAILED: 10032,//"Get Department Failed");

    SAVE_DEPARTMENT_FAILED: 10033,//"Save Department Failed");

    DELETE_DEPARTMENT_FAILED: 10034, //"Delete Department Failed");

    GET_CLIENT_FAILED: 10035,// "Get ClientEntity Failed");

    SAVE_CLIENT_FAILED: 10036, //"Save ClientEntity Failed");

    DELETE_CLIENT_FAILED: 10037, //"Delete ClientEntity Failed");

    GET_CLIENTS_FAILED: 10038, //"Get Clients Failed");

    UPDATE_CLIENT_PASSWORD_FAILED: 10037, //"Update clientEntity password failed");

    UPDATE_USER_PASSWORD_FAILED: 10038, //"Update userEntity password failed");

    CHECK_CLIENT_NAME_FAILED: 10039, //"Check clientEntity name failed");

    TRIGGER_MFA_FAILED: 10040, //"Trigger MFA Failed");

    VERIFY_MFA_FAILED: 10090, //"Verify MFA Failed");

    CALCULATE_TWR_FAILED: 10041, //"Calculate TWR Failed");

    GET_AGGREGATED_POSITION_HISTORIES_FAILED: 10041, //"Get Aggregated Position Histories Failed");

    GET_SECURITY_HOLDING_STATUS_FAILED: 10042, //"Get Security Holding Status Failed");

    COUNT_TRANSACTION_FAILED: 10043, // "Count Transactions Failed");

    CHECK_LINK_FAILED: 10044, //"Check Link Failed");

    PRE_SIGNED_URL_FAILED: 10045, //"PreSigned URL Failed");

    GET_DEVICE_FAILED: 10050, //"Get device failed");

    SAVE_DEVICE_FAILED: 10051,// "Save device failed");

    REFRESH_TOKEN_FAILED: 10052, // Error(10052, "Refresh token failed");

    GET_TRANSACTION_METRICS_FAILED: 10053,  // Error(10053, "Get Transaction Metrics Failed");

    GET_HOLDING_METRICS_FAILED: 10054, // Error(10054, "Get Holding Metrics Failed");

    GET_VALUE_CHANGE_METRICS_FAILED: 10055,  // Error(10055, "Get Value Change Metrics Failed");

    CREATE_AUDIT_LOG_FAILED: 10056, // Error(10056, "Create Audit Log Failed");

    EXPORT_AUDITLOGS_FAILED: 10057, // Error(10057, "Export AuditLogs Failed");

    ENABLE_WHATSAPP_INTEGRATION_FAILED: 10060, // Error(10060, "Enable whatsapp integration failed");

    DISABLE_WHATSAPP_INTEGRATION_FAILED: 10061, // Error(10061, "Disable whatsapp integration failed");

    GET_STATUS_WHATSAPP_INTEGRATION_FAILED: 10062, // Error(10062, "Get status whatsapp integration failed");

    GET_TEMPLATES_WHATSAPP_INTEGRATION_FAILED: 10063,  // Error(10063, "Get templates for whatsapp integration failed");

    ENABLE_WECHAT_INTEGRATION_FAILED: 10064,// Error(10064, "Enable wechat integration failed");

    DISABLE_WECHAT_INTEGRATION_FAILED: 10065, // Error(10065, "Disable wechat integration failed");

    GET_STATUS_WECHAT_INTEGRATION_FAILED: 10066, // Error(10066, "Get status wechat integration failed");

    ENABLE_LINE_INTEGRATION_FAILED: 10067, // Error(10067, "Enable line integration failed");

    DISABLE_LINE_INTEGRATION_FAILED: 10068, // Error(10068, "Disable line integration failed");

    GET_STATUS_LINE_INTEGRATION_FAILED: 10069, // Error(10069, "Get status line integration failed");

    SAVE_WECHAT_BINDING_DESTINATION_FAILED: 10070, // Error(10070, "Save wechat binding destination failed");

    GET_TEMPLATES_WECHAT_INTEGRATION_FAILED: 10071, // Error(10071, "Get templates for wechat integration failed");

    TRIGGER_TEMPLATE_WECHAT_INTEGRATION_FAILED: 10072, // Error(10072, "Trigger template for wechat integration failed");

    TRIGGER_TEMPLATE_WHATSAPP_INTEGRATION_FAILED: 10073, // Error(10073, "Trigger template for whatsapp integration failed");

    GET_INTEGRATION_STATUS_LIST_FAILED: 10074, // Error(10074, "Get integration status list failed");

    CREATE_INTEGRATIONS_ROOM_FAILED: 10075, // Error(10075, "Create integrations room failed");

    UPDATE_INTEGRATIONS_ROOM_FAILED: 10076, // Error(10076, "Update integrations room failed");

    RENDER_TEMPLATE_WECHAT_INTEGRATION_FAILED: 10077,// Error(10077, "Render template for wechat integration failed");

    RENDER_TEMPLATE_WHATSAPP_INTEGRATION_FAILED: 10078, // Error(10078, "Render template for whatsapp integration failed");

    GET_COMPANY_PUBLIC_INFO: 10079,// Error(10079, "Get Company Public Info Failed");

    GET_COMPANY_PROFILES: 10080,  // Error(10080, "Get Company Profiles Failed");

    ERROR_SERVER: 500

}

const message = {
    ERROR_SERVER: 'There was an internal server error.',
    INVALID_REQUEST: 'Failed Validating Request',
    SUCCESS: 'Success'
}
module.exports = {
    code,
    message
}